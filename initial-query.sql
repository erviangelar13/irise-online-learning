INSERT INTO gb_oauth_client (id,name,client_id,client_secret,grants,"scope",access_token_lifetime,refresh_token_lifetime,"privateKey","publicKey",cert,cert_expires_at,created_at,deleted_at) VALUES
	 ('6d1dbd71-fac9-4001-b102-abec374e3554'::uuid,'beefsteak-server','c8535817-1f01-4ffb-8c44-ab710d5621f2-beefsteak-server','Ihk9qccM7EKv7C5Fa+qAnye0nm64YWtOPTos2MbDpQU=','client_credentials,refresh_token,password','["all"]',3600,7200,'privatekey','publickey','cert','2099-12-31 00:00:00.000','2021-07-31 23:21:33.945',NULL);

INSERT INTO roles ("name",description) VALUES
	 ('admin','Administrator'),
	 ('operator','Operator'),
	 ('user','User'),
	 ('instructor','Instructor');
