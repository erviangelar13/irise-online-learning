import { Controller, Inject, Get, Res, Param } from '@nestjs/common';
import { response, responseError } from '../../helpers/response.helper';
// import { Oauth2Guard } from '../../guards/guards';
import { ContentService } from '../graphql/content/content.service';
import { ContentMediaService } from '../graphql/content-media/content-media.service';
import { PaymentMethodService } from '../graphql/payment-method/payment-method.service';
import { PaymentProviderService } from '../graphql/payment-provider/payment-provider.service';
import { CourseResourceService } from '../graphql/course-resource/course-resource.service';
import { CourseOrganizerService } from '../graphql/course-organizer/course-organizer.service';
import { CourseService } from '../graphql/course/course.service';

@Controller('view')
export class ViewController {

    constructor(
        @Inject(CourseService) private courseService: CourseService,
        @Inject(CourseOrganizerService) private courseOrganizerService: CourseOrganizerService,
        @Inject(CourseResourceService) private courseResourceService: CourseResourceService,
        @Inject(ContentService) private contentService: ContentService,
        @Inject(ContentMediaService) private contentMediaService: ContentMediaService,
        @Inject(PaymentMethodService) private paymentMethodService: PaymentMethodService,
        @Inject(PaymentProviderService) private paymentProviderService: PaymentProviderService,
    ) { }

    @Get('course/cover/:id')
    // @UseGuards(Oauth2Guard)
    async getCourseCover(@Res() res: any, @Param('id') id: number) {
        try {
            const getBuffer = await this.courseService.viewCover(id);
            res.set('Content-Type', getBuffer.mime);
            res.status(200);
            res.send(Buffer.from(getBuffer.file));
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('course-organizer/photo/:id')
    // @UseGuards(Oauth2Guard)
    async getCourseOrganizerPhoto(@Res() res: any, @Param('id') id: number) {
        try {
            const getBuffer = await this.courseOrganizerService.viewPhoto(id);
            res.set('Content-Type', getBuffer.mime);
            res.status(200);
            res.send(Buffer.from(getBuffer.file));
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('course-resource/file/:id')
    // @UseGuards(Oauth2Guard)
    async getCourseResourceFile(@Res() res: any, @Param('id') id: number) {
        try {
            const getBuffer = await this.courseResourceService.viewFile(id);
            res.set('Content-Type', getBuffer.mime);
            res.status(200);
            res.send(Buffer.from(getBuffer.file));
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('content/banner/:id')
    async getContentBanner(@Res() res: any, @Param('id') id: number) {
        try {
            const getBuffer = await this.contentService.viewBanner(id);
            res.set('Content-Type', getBuffer.mime);
            res.status(200);
            res.send(Buffer.from(getBuffer.file));
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('content/media/:id')
    async getContentMedia(@Res() res: any, @Param('id') id: number) {
        try {
            const getBuffer = await this.contentMediaService.viewMedia(id);
            res.set('Content-Type', getBuffer.mime);
            res.status(200);
            res.send(Buffer.from(getBuffer.file));
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('payment-method/logo/:id')
    async getPaymentMethodLogo(@Res() res: any, @Param('id') id: number) {
        try {
            const getBuffer = await this.paymentMethodService.viewLogo(id);
            res.set('Content-Type', getBuffer.mime);
            res.status(200);
            res.send(Buffer.from(getBuffer.file));
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('payment-provider/logo/:id')
    async getPaymentProviderLogo(@Res() res: any, @Param('id') id: number) {
        try {
            const getBuffer = await this.paymentProviderService.viewLogo(id);
            res.set('Content-Type', getBuffer.mime);
            res.status(200);
            res.send(Buffer.from(getBuffer.file));
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }
}
