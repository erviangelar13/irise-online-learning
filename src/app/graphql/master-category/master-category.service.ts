import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MasterCategory } from '../../../entities/master-category.entity';
import { MasterCategoryInput } from './master-category.dto';
import { PaginationOptions } from '../../../helpers/pagination.helper';

@Injectable()
export class MasterCategoryService {

    constructor(
        @InjectRepository(MasterCategory) private masterCategoryRepo: Repository<MasterCategory>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.masterCategoryRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList() {
        try {
            return await this.masterCategoryRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.masterCategoryRepo.findOne(id);
            if (!data) throw new Error(`Master category with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: MasterCategoryInput) {
        try {
            const data = new MasterCategory();
            data.name = dto.name;
            data.description = dto.description;
            await this.masterCategoryRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: MasterCategoryInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.description = dto.description;
            await this.masterCategoryRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            if (!data) throw new Error(`Master category with id = ${id} is not found`);
            await this.masterCategoryRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
