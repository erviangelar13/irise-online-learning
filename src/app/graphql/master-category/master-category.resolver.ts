import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { MasterCategory } from '../../../entities/master-category.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { MasterCategoryService } from './master-category.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { MasterCategoryPagination, MasterCategoryInput } from './master-category.dto';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => MasterCategory)
export class MasterCategoryResolver {

    constructor(
        @Inject(MasterCategoryService) private masterCategoryService: MasterCategoryService,
    ) { }

    @Scopes('read')
    @Query(returns => MasterCategoryPagination)
    async masterCategoryList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.masterCategoryService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    // @UseGuards(Oauth2Guard)
    @Query(returns => [MasterCategory])
    async masterCategories() {
        try {
            return await this.masterCategoryService.findList();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => MasterCategory)
    async masterCategory(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.masterCategoryService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createMasterCategory(
        @Args({ name: 'masterCategoryInput', type: () => MasterCategoryInput }) masterCategoryInput: MasterCategoryInput,
    ) {
        try {
            const data = await this.masterCategoryService.create(masterCategoryInput);
            return { id: data.id, message: 'Success create master category' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateMasterCategory(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'masterCategoryInput', type: () => MasterCategoryInput }) masterCategoryInput: MasterCategoryInput,
    ) {
        try {
            const data = await this.masterCategoryService.update(id, masterCategoryInput);
            return { id: data.id, message: 'Success update master category' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteMasterCategory(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.masterCategoryService.delete(id);
            return { id: data.id, message: 'Success delete master category' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
