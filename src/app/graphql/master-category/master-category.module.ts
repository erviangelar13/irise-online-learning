import { Module } from '@nestjs/common';
import { MasterCategoryResolver } from './master-category.resolver';
import { MasterCategoryService } from './master-category.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MasterCategory } from '../../../entities/master-category.entity';

@Module({
    providers: [MasterCategoryResolver, MasterCategoryService],
    imports: [
        TypeOrmModule.forFeature([MasterCategory]),
    ],
})
export class MasterCategoryModule { }
