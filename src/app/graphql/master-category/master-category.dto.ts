import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { MasterCategory } from "../../../entities/master-category.entity";


@InputType()
export class MasterCategoryInput {

    @Field()
    name: string;

    @Field()
    description: string;
}

@ObjectType()
export class MasterCategoryPagination extends PaginationPage {
    @Field(type => [MasterCategory])
    data: MasterCategory[];
}
