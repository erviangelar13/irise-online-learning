import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { CourseCategory } from "../../../entities/course-category.entity";

@InputType()
export class CourseOrganizerInput {

    @Field()
    courseId: number;

    @Field()
    categoryId: number;
}

@ObjectType()
export class CourseCategoryPagination extends PaginationPage {
    @Field(type => [CourseCategory])
    data: CourseCategory[];
}
