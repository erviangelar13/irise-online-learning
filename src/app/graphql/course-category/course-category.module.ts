import { Module } from '@nestjs/common';
import { CourseCategoryResolver } from './course-category.resolver';
import { CourseCategoryService } from './course-category.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseCategory } from '../../../entities/course-category.entity';

@Module({
    providers: [CourseCategoryResolver, CourseCategoryService],
    imports: [
        TypeOrmModule.forFeature([CourseCategory]),
    ],
})
export class CourseCategoryModule { }
