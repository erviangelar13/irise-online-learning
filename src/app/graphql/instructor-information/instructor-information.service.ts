import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { InstructorInformationInput } from './instructor-information.dto';
import { Users } from '../../../entities/users.entity';

@Injectable()
export class InstructorInformationService {

    constructor(
        @InjectRepository(InstructorInformation) private instructorRepo: Repository<InstructorInformation>,
        @InjectRepository(Users) private usersRepo: Repository<Users>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.instructorRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.instructorRepo.findOne(id);
            if (!data) throw new Error(`Instructor with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: InstructorInformationInput) {
        try {
            const data = new InstructorInformation();
            data.name = dto.name;
            data.title = dto.title;
            data.personalInfo = dto.personalInfo;
            data.profesionalExperience = dto.profesionalExperience;
            await this.instructorRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: InstructorInformationInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.title = dto.title;
            data.personalInfo = dto.personalInfo;
            data.profesionalExperience = dto.profesionalExperience;
            await this.instructorRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const exist = await this.usersRepo.createQueryBuilder()
                .where('instructor_id = :id', { id })
                .getOne();

            if (exist) throw new Error(`Cannot delete instructor information with id = ${id}, because in use in user ${exist.name}`);
            const data = await this.findById(id);
            await this.instructorRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
