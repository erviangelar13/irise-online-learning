import { Module } from '@nestjs/common';
import { InstructorInformationService } from './instructor-information.service';
import { InstructorInformationResolver } from './instructor-information.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { Users } from '../../../entities/users.entity';

@Module({
    providers: [InstructorInformationService, InstructorInformationResolver],
    imports: [
        TypeOrmModule.forFeature([InstructorInformation, Users]),
    ],
})
export class InstructorInformationModule { }
