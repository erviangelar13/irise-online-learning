import { InputType, Field, ObjectType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { InstructorInformation } from "../../../entities/instructor-information.entity";

@InputType()
export class InstructorInformationInput {

    @Field()
    name: string;

    @Field()
    title: string;

    @Field()
    personalInfo: string;

    @Field()
    profesionalExperience: string;
}

@ObjectType()
export class InstructorInformationPagination extends PaginationPage {
    @Field(type => [InstructorInformation])
    data: InstructorInformation[];
}
