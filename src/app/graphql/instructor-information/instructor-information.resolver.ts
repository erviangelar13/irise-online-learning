import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { InstructorInformationService } from './instructor-information.service';
import { Oauth2Guard } from '../../../guards/guards';
import { Scopes } from '../../../decorators/scopes.decorator';
import { InstructorInformationPagination, InstructorInformationInput } from './instructor-information.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => InstructorInformation)
export class InstructorInformationResolver {

    constructor(
        @Inject(InstructorInformationService) private instructorInformationService: InstructorInformationService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => InstructorInformationPagination)
    async instructorInformationList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.instructorInformationService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => InstructorInformation)
    async instructorInformation(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.instructorInformationService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createInstructorInformation(
        @Args({ name: 'instructorInformationInput', type: () => InstructorInformationInput }) instructorInformationInput: InstructorInformationInput,
    ) {
        try {
            const data = await this.instructorInformationService.create(instructorInformationInput);
            return { id: data.id, message: 'Success create instructor information' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateInstructorInformation(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'instructorInformationInput', type: () => InstructorInformationInput }) instructorInformationInput: InstructorInformationInput,
    ) {
        try {
            const data = await this.instructorInformationService.update(id, instructorInformationInput);
            return { id: data.id, message: 'Success update instructor information' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteInstructorInformation(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.instructorInformationService.delete(id);
            return { id: data.id, message: 'Success delete instructor information' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
