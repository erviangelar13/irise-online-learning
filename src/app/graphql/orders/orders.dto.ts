import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Orders } from "../../../entities/orders.entity";

@InputType()
export class OrdersFilter {

    @Field({ nullable: true })
    courseType: number;

    @Field({ nullable: true })
    categoryId: number;

    @Field({ nullable: true })
    organizerId: number;

    @Field({ nullable: true })
    statusId: number;
}

@InputType()
export class MonitorOrdersFilter {

    @Field({ nullable: true })
    courseType: number;

    @Field({ nullable: true })
    categoryId: number;

    @Field({ nullable: true })
    organizerId: number;

    @Field({ nullable: true })
    statusId: number;
}

@InputType()
export class OrdersInput {

    @Field()
    orderTypeId: number;

    @Field({ nullable: true })
    institutionId?: number;

    @Field()
    name: string;

    @Field()
    email: string;

    @Field()
    phone: string;

    @Field()
    courseId: number;

    @Field()
    qty: number;

    @Field({ nullable: true })
    promoCode?: string;

    @Field()
    withParticipant: boolean;

    @Field(type => [Participant], { nullable: true })
    participants: Participant[];

}

@InputType()
export class Participant {
    @Field()
    name: string;

    @Field()
    email: string;

    @Field()
    phone: string;
}

@ObjectType()
export class OrdersPagination extends PaginationPage {
    @Field(type => [Orders])
    data: Orders[];
}
