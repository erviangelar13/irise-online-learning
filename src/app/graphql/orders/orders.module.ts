import { Module } from '@nestjs/common';
import { OrdersResolver } from './orders.resolver';
import { OrdersService } from './orders.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Orders } from '../../../entities/orders.entity';
import { MasterReference } from '../../../entities/master-reference.entity';
import { Institution } from '../../../entities/institution.entity';
import { Course } from '../../../entities/course.entity';
import { PromoCode } from '../../../entities/promo-code.entity';
import { Users } from '../../../entities/users.entity';
import { OrderPIC } from '../../../entities/order-pic.entity';
import { OrderInstitution } from '../../../entities/order-institution.entity';
import { OrderDetail } from '../../../entities/order-detail.entity';
import { Transactions } from '../../../entities/transaction.entity';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { PaymentProvider } from '../../../entities/payment-provider.entity';

@Module({
    providers: [OrdersResolver, OrdersService],
    imports: [
        TypeOrmModule.forFeature([
            Orders,
            MasterReference,
            Institution,
            Course,
            PromoCode,
            Users,
            OrderPIC,
            OrderInstitution,
            OrderDetail,
            Transactions,
            TransactionHistory,
            PaymentProvider
        ]),
    ],
})
export class OrdersModule { }
