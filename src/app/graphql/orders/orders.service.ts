import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { MailerService } from '@nestjs-modules/mailer';
import { Orders } from '../../../entities/orders.entity';
import { OrdersInput, OrdersFilter } from './orders.dto';
import { MasterReference } from '../../../entities/master-reference.entity';
import { Institution } from '../../../entities/institution.entity';
import { Course } from '../../../entities/course.entity';
import { PromoCode } from '../../../entities/promo-code.entity';
import { Auth } from '../../../helpers/auth.helper';
import { dateMoment } from '../../../helpers/date.helper';
import { generateCode, generateInvoice } from '../../../helpers/generate.helper';
import { Users } from '../../../entities/users.entity';
import { OrderPIC } from '../../../entities/order-pic.entity';
import { OrderInstitution } from '../../../entities/order-institution.entity';
import { OrderDetail } from '../../../entities/order-detail.entity';
import { Transactions } from '../../../entities/transaction.entity';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { MailService } from '../../mail/mail.service';

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(Orders) private ordersRepo: Repository<Orders>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
        @InjectRepository(Institution) private institutionRepo: Repository<Institution>,
        @InjectRepository(Course) private courseRepo: Repository<Course>,
        @InjectRepository(PromoCode) private promoCodeRepo: Repository<PromoCode>,
        @InjectRepository(Users) private usersRepo: Repository<Users>,
        private readonly mailerService: MailerService,
    ) { }

    async find(auth: Auth, filter: OrdersFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.ordersRepo.createQueryBuilder('orders')
                .leftJoin(
                    'orders.course',
                    'course'
                )
                .leftJoin(
                    'course.courseCategory',
                    'courseCategory'
                )
                .leftJoin(
                    'orders.orderPIC',
                    'orderPIC'
                )
                .leftJoin(
                    'orders.transactionsHistory',
                    'TransactionHistory'
                )
                .where(
                    `(LOWER(orders.order_no) LIKE :search
                    OR LOWER(course.name) LIKE :search
                    OR LOWER(orderPIC.name) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            if (filter.courseType) list.andWhere('course.course_type = :courseType', { courseType: filter.courseType });
            if (filter.organizerId) list.andWhere('course.organizer_id = :organizerId', { organizerId: filter.organizerId });
            if (filter.categoryId) list.andWhere('courseCategory.id = :categoryId', { categoryId: filter.categoryId });
            if (filter.statusId) list.andWhere('orders.statusId = :statusId', { statusId: filter.statusId });

            if (!auth.isAdmin) list.andWhere('orders.user_id = :userId', { userId: auth.id });
            // console.log(list)
            return await list.orderBy("orders.orderDate","ASC").pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id);
            if (!data) throw new Error(`Order with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findByOrderNo(orderNo: string) {
        try {
            const where = {
                orderNo : orderNo
            }
            const data = await this.ordersRepo.findOne(where, { relations: ['vouchers','transactions']});
            if (!data) throw new Error(`Order with order no = ${orderNo} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findCourseById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id, { relations: ['course'] });
            return data.course;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findStatusById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'order_status', idx: idx });
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderTypeById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'order_type', idx: idx });
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderPICById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id, { relations: ['orderPIC'] });
            return data.orderPIC;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderDetailById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id, { relations: ['orderDetail'] });
            return data.orderDetail;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderInstitutionById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id, { relations: ['orderInstitution'] });
            return data.orderInstitution;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findUserOrderById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id, { relations: ['user'] });
            return data.user;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findTransactionHistoryById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id, { relations: ['transactionsHistory'] });
            return data.transactionsHistory;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findVoucherById(id: number) {
        try {
            const data = await this.ordersRepo.findOne(id, { relations: ['vouchers'] });
            // console.log(data.vouchers)
            return data.vouchers;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(auth: Auth, dto: OrdersInput) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const ordersValidationData = await this.ordersValidation(dto);
            const course = ordersValidationData.course;
            const promoCode = ordersValidationData.promoCode;

            const priceDefault = course.price;
            let price: number = priceDefault;
            let discount: number = 0;
            if (promoCode) {
                if (promoCode.valueTypeId === 1) {
                    // Percentage
                    const percentageValue = (priceDefault * promoCode.value) / 100;
                    price = priceDefault - percentageValue;
                    discount = percentageValue;
                } else if (promoCode.valueTypeId === 2) {
                    // Value
                    price = priceDefault - promoCode.value;
                    discount = promoCode.value;
                }
            }
            // console.log(promoCode)

            const orderNo = await generateCode(Orders, `ORD_${dateMoment().format('DD')}`, 16, queryRunner);
            const users = await this.usersRepo.findOne(auth.id);

            const ordersData: Orders = new Orders();
            ordersData.user = users;
            ordersData.course = course;
            ordersData.orderDate = dateMoment().toDate();
            ordersData.orderNo = orderNo;
            ordersData.statusId = 1;
            ordersData.orderTypeId = dto.orderTypeId;
            ordersData.priceDefault = priceDefault;
            ordersData.discount = discount;
            ordersData.price = price;
            ordersData.qty = dto.qty;
            ordersData.promoCode = promoCode ? promoCode.code : '';

            await queryRunner.manager.save(ordersData);

            const orderPIC: OrderPIC = new OrderPIC();
            orderPIC.orders = ordersData;
            orderPIC.name = dto.name;
            orderPIC.email = dto.email;
            orderPIC.phone = dto.phone;

            await queryRunner.manager.save(orderPIC);

            // const transactionCode = await generateCode(Transactions, `TRANS_${dateMoment().format('ss')}`, 16, queryRunner);
            // const invoiceNo = await generateInvoice(Transactions, 'INV', 12, queryRunner);
            
            // const paymentProvider = await this.paymentProviderRepo.findOne(4);
            // if (!paymentProvider) throw new Error(`PaymentProvider with id = 4 is not found`);

            // const data = new Transactions();
            // data.order = ordersData;
            // data.date = dateMoment().toDate();
            // data.amount = price * dto.qty;
            // data.notes = '';
            // data.paymentProvider = paymentProvider;
            // data.transactionCode = transactionCode;
            // data.invoiceNo = invoiceNo;
            // data.paymentStatusId = 1;
            // data.paymentTypeId = 2;

            // await queryRunner.manager.save(data);

            // const transactionHistory: TransactionHistory = new TransactionHistory();
            // transactionHistory.transaction = data;
            // transactionHistory.date = dateMoment().toDate();
            // transactionHistory.info = '';
            // transactionHistory.order = ordersData;
            // transactionHistory.paymentStatusId = 1;

            // await queryRunner.manager.save(transactionHistory);

            if (dto.orderTypeId === 2) {
                const orderInstitution: OrderInstitution = new OrderInstitution();
                orderInstitution.orders = ordersData;
                orderInstitution.institution = ordersValidationData.institution;
                orderInstitution.name = dto.name;
                orderInstitution.email = dto.email;
                orderInstitution.phone = dto.phone;

                await queryRunner.manager.save(orderInstitution);
            }

            if (dto.withParticipant) {
                const orderDetails: OrderDetail[] = [];
                for (const participant of dto.participants) {
                    const orderDetail: OrderDetail = new OrderDetail();
                    orderDetail.orders = ordersData;
                    orderDetail.name = participant.name;
                    orderDetail.email = participant.email;
                    orderDetail.phone = participant.phone;
                    orderDetails.push(orderDetail);
                }
                await queryRunner.manager.save(orderDetails);
            }

            // const mailService = new MailService(this.mailerService);
            // await mailService.sendEmail(
            //     orderPIC.email,
            //     'Order Information',
            //     'order',
            //     {
            //         date: ordersData.orderDate,
            //         orderNo: ordersData.orderNo,
            //         courseName: ordersData.course.name,
            //         qty: ordersData.qty,
            //         name: orderPIC.name,
            //         phone: orderPIC.phone
            //     },
            // );

            await queryRunner.commitTransaction();

            return ordersData;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async cancelledOrder(id: number) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const ordersData = await this.ordersRepo.findOne(id, { relations: ['course'] });
            if (!ordersData) throw new Error(`Order with id = ${id} is not found`);

            ordersData.statusId = 4;
            await queryRunner.manager.save(ordersData);

            // const course = ordersData.course;
            // course.currentQuota = course.currentQuota + ordersData.qty;
            // await queryRunner.manager.save(course);

            await queryRunner.commitTransaction();

            return ordersData;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    private async ordersValidation(dto: OrdersInput) {
        try {
            const orderType = await this.masterReferenceRepo.findOne({ group: 'order_type', idx: dto.orderTypeId });
            if (!orderType) throw new Error(`Order type with id = ${dto.orderTypeId} is not found`);

            let institution: Institution = new Institution();
            if (dto.orderTypeId === 2) {
                institution = await this.institutionRepo.findOne(dto.institutionId);
                if (!institution) throw new Error(`Institution with id = ${dto.institutionId} is not found`);
            }

            const course = await this.courseRepo.findOne(dto.courseId);
            if (!course) throw new Error(`Course with id = ${dto.courseId} is not found`);
            // if (course.currentQuota < dto.qty) throw new Error(`Course current quota is ${course.currentQuota}`);

            let promoCode: PromoCode = new PromoCode();
            if (dto.promoCode) {
                promoCode = await this.promoCodeRepo.findOne({ code: dto.promoCode });
                if (!promoCode) throw new Error(`Promo with code = ${dto.promoCode} is not found`);
            }

            if (dto.withParticipant) {
                if (dto.participants.length !== dto.qty) {
                    throw new Error('Total participant is not match with qty');
                }
            }

            return {
                orderType,
                institution,
                course,
                promoCode,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
