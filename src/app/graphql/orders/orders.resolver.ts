import { Resolver, Query, Args, Mutation, ResolveField, Parent } from '@nestjs/graphql';
import { Orders } from '../../../entities/orders.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersPagination, OrdersFilter, OrdersInput } from './orders.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { Oauth2Guard } from '../../../guards/guards';
import { AuthUser } from '../../../decorators/auth.decorator';
import { Auth } from '../../../helpers/auth.helper';
import { Scopes } from '../../../decorators/scopes.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { OrderPIC } from '../../../entities/order-pic.entity';
import { OrderDetail } from '../../../entities/order-detail.entity';
import { OrderInstitution } from '../../../entities/order-institution.entity';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { Users } from '../../../entities/users.entity';
import { Course } from '../../../entities/course.entity';
import { Voucher } from '../../../entities/voucher.entity';

@Resolver(of => Orders)
export class OrdersResolver {

    constructor(
        @Inject(OrdersService) private ordersService: OrdersService,
    ) { }

    @UseGuards(Oauth2Guard)
    @Query(returns => OrdersPagination)
    async orderList(
        @AuthUser() auth: Auth,
        @Args({ name: 'ordersFilter', type: () => OrdersFilter, nullable: true })
        ordersFilter: OrdersFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            // console.log(paginationOptions)
            return await this.ordersService.find(auth, ordersFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @UseGuards(Oauth2Guard)
    @Query(returns => OrdersPagination)
    async orderVoucherList(
        @AuthUser() auth: Auth,
        @Args({ name: 'ordersFilter', type: () => OrdersFilter, nullable: true })
        ordersFilter: OrdersFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            // console.log(paginationOptions)
            return await this.ordersService.find(auth, ordersFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Query(returns => Orders)
    async orders(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.ordersService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Query(returns => Orders)
    async ordersVoucher(
        @Args({ name: 'orderNo', type: () => String }) orderNo: string,
    ) {
        try {
            return await this.ordersService.findByOrderNo(orderNo);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async status(@Parent() orders: Orders) {
        const { statusId } = orders;
        return this.ordersService.findStatusById(statusId);
    }

    @ResolveField(returns => String)
    async orderType(@Parent() orders: Orders) {
        const { orderTypeId } = orders;
        return this.ordersService.findOrderTypeById(orderTypeId);
    }

    @ResolveField(returns => OrderPIC)
    async orderPIC(@Parent() orders: Orders) {
        const { id } = orders;
        return this.ordersService.findOrderPICById(id);
    }

    @ResolveField(returns => [OrderDetail])
    async orderDetail(@Parent() orders: Orders) {
        const { id } = orders;
        return this.ordersService.findOrderDetailById(id);
    }

    @ResolveField(returns => OrderInstitution)
    async orderInstitution(@Parent() orders: Orders) {
        const { id } = orders;
        return this.ordersService.findOrderInstitutionById(id);
    }

    @ResolveField(returns => Users)
    async user(@Parent() orders: Orders) {
        const { id } = orders;
        return this.ordersService.findUserOrderById(id);
    }

    @ResolveField(returns => Course)
    async course(@Parent() orders: Orders) {
        const { id } = orders;
        return this.ordersService.findCourseById(id);
    }

    @ResolveField(returns => TransactionHistory)
    async transactionsHistory(@Parent() orders: Orders) {
        const { id } = orders;
        return this.ordersService.findTransactionHistoryById(id);
    }

    @ResolveField(returns => [Voucher])
    async vouchers(@Parent() orders: Orders) {
        const { id } = orders;
        return this.ordersService.findVoucherById(id);
    }

    @Scopes('create')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createOrders(
        @AuthUser() auth: Auth,
        @Args({ name: 'ordersInput', type: () => OrdersInput }) ordersInput: OrdersInput,
    ) {
        try {
            const data = await this.ordersService.create(auth, ordersInput);
            return { id: data.id, message: 'Success create orders' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @Mutation(returns => ResponseMutation)
    async cancelledOrder(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.ordersService.cancelledOrder(id);
            return { id: id, data: data, message: 'Success cancelled order' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
