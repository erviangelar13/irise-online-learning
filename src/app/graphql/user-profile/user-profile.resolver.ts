import { Resolver, ResolveField, Parent, Mutation, Args } from '@nestjs/graphql';
import { UserProfile } from '../../../entities/user-profile.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { UserProfileService } from './user-profile.service';
import { Users } from '../../../entities/users.entity';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { ResponseMutation } from '../../../helpers/response.helper';
import { UserProfileInput } from './user-profile.dto';

@Resolver(of => UserProfile)
export class UserProfileResolver {

    constructor(
        @Inject(UserProfileService) private userProfileService: UserProfileService,
    ) { }

    @ResolveField(returns => Users)
    async user(@Parent() userProfile: UserProfile) {
        const { id } = userProfile;
        return this.userProfileService.findUserById(id);
    }

    @ResolveField(returns => String)
    async gender(@Parent() userProfile: UserProfile) {
        const { genderId } = userProfile;
        const data = await this.userProfileService.gender(genderId);
        return data.name;
    }

    @Scopes('create', 'update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async setUserProfile(
        @Args({ name: 'userProfileInput', type: () => UserProfileInput }) userProfileInput: UserProfileInput,
    ) {
        try {
            const data = await this.userProfileService.setUserProfile(userProfileInput);
            return { id: data.id, message: 'Success set user profile' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
