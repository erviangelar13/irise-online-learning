import { Module } from '@nestjs/common';
import { UserProfileResolver } from './user-profile.resolver';
import { UserProfileService } from './user-profile.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserProfile } from '../../../entities/user-profile.entity';
import { Users } from '../../../entities/users.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [UserProfileResolver, UserProfileService],
    imports: [TypeOrmModule.forFeature([UserProfile, Users, MasterReference])],
})
export class UserProfileModule { }
