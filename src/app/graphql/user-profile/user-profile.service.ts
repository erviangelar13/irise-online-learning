import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserProfile } from '../../../entities/user-profile.entity';
import { UserProfileInput } from './user-profile.dto';
import { Users } from '../../../entities/users.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Injectable()
export class UserProfileService {

    constructor(
        @InjectRepository(UserProfile) private userProfileRepo: Repository<UserProfile>,
        @InjectRepository(Users) private usersRepo: Repository<Users>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
    ) { }

    async findUserById(id: number) {
        try {
            const data = await this.userProfileRepo.findOne(id, { relations: ['user'] });
            return data.user;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async gender(genderId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'gender', idx: genderId });
            if (!data) throw new Error(`Gender with idx = ${genderId} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async setUserProfile(dto: UserProfileInput) {
        try {
            const userData = await this.usersRepo.findOne(dto.userId);
            if (!userData) throw new Error(`User with id = ${dto.userId} is not found`);
            const data = await this.userProfileRepo.createQueryBuilder()
                .where('user_id = :userId', { userId: dto.userId })
                .getOne();

            data.genderId = dto.genderId;
            data.birthDate = dto.birthDate;
            data.birthPlace = dto.birthPlace;
            data.user = userData;
            await this.userProfileRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
