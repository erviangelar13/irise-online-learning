import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class UserProfileInput {

    @Field()
    userId: number;

    @Field()
    genderId: number;

    @Field()
    birthDate: string;

    @Field()
    birthPlace: string;
}
