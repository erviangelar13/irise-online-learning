import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { Roles } from '../../../entities/roles.entity';
import { Scopes } from '../../../decorators/scopes.decorator';
import { UseGuards, Inject } from '@nestjs/common';
import { Oauth2Guard } from '../../../guards/guards';
import { RolesService } from './roles.service';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesPagination, RolesInput } from './roles.dto';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => Roles)
export class RolesResolver {

    constructor(
        @Inject(RolesService) private rolesService: RolesService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => RolesPagination)
    async rolesList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.rolesService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [Roles])
    async roleList() {
        try {
            return await this.rolesService.findList();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => Roles)
    async roles(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.rolesService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createRoles(
        @Args({ name: 'rolesInput', type: () => RolesInput }) rolesInput: RolesInput,
    ) {
        try {
            const data = await this.rolesService.create(rolesInput);
            return { id: data.id, message: 'Success create roles' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateRoles(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'rolesInput', type: () => RolesInput }) rolesInput: RolesInput,
    ) {
        try {
            const data = await this.rolesService.update(id, rolesInput);
            return { id: data.id, message: 'Success update roles' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteRoles(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.rolesService.delete(id);
            return { id: data.id, message: 'Success delete roles' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
