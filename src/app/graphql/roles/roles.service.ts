import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Roles } from '../../../entities/roles.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { RolesInput } from './roles.dto';

@Injectable()
export class RolesService {

    constructor(
        @InjectRepository(Roles) private rolesRepo: Repository<Roles>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.rolesRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList() {
        try {
            return await this.rolesRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.rolesRepo.findOne(id);
            if (!data) throw new Error(`Roles with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: RolesInput) {
        try {
            const data = new Roles();
            data.name = dto.name;
            data.description = dto.description;
            await this.rolesRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: RolesInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.description = dto.description;
            await this.rolesRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.rolesRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

}
