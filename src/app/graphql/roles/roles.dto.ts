import { InputType, Field, ObjectType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Roles } from "../../../entities/roles.entity";

@InputType()
export class RolesInput {

    @Field()
    name: string;

    @Field()
    description: string;
}

@ObjectType()
export class RolesPagination extends PaginationPage {
    @Field(type => [Roles])
    data: Roles[];
}
