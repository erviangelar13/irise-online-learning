import { Module } from '@nestjs/common';
import { RolesResolver } from './roles.resolver';
import { RolesService } from './roles.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Roles } from '../../../entities/roles.entity';
import { UserHasRoles } from '../../../entities/user-has-roles.entity';

@Module({
    providers: [RolesResolver, RolesService],
    imports: [
        TypeOrmModule.forFeature([Roles, UserHasRoles]),
    ],
})
export class RolesModule { }
