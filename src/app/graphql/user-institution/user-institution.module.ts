import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Institution } from '../../../entities/institution.entity';
import { UsersInstitution } from '../../../entities/users-institution.entity';
import { Users } from "../../../entities/users.entity";
import { UsersInstituionResolver } from "./user-institution.resolver";
import { UsersInstitutionService } from "./user-institution.service";

@Module({
    providers: [UsersInstituionResolver, UsersInstitutionService],
    imports: [
        TypeOrmModule.forFeature([
            UsersInstitution,
            Users,
            Institution
        ]),
    ],
})
export class UserInstitutionModule { }