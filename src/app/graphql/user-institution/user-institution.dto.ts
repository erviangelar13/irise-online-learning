import { Field, InputType, ObjectType } from "@nestjs/graphql";
import { UsersInstitution } from "../../../entities/users-institution.entity";
import { PaginationPage } from "../../../helpers/pagination.helper";


@InputType()
export class JoinInstitution {

    @Field()
    code: string;
}

@ObjectType()
export class UserInstitutionPagination extends PaginationPage {
    @Field(type => [UsersInstitution])
    data: UsersInstitution[];
}
