import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Institution } from "../../../entities/institution.entity";
import { UsersInstitution } from "../../../entities/users-institution.entity";
import { Users } from "../../../entities/users.entity";
import { PaginationOptions } from "../../../helpers/pagination.helper";
import { Repository } from "typeorm";
import { JoinInstitution } from "./user-institution.dto";


@Injectable()
export class UsersInstitutionService {

    constructor(
        @InjectRepository(Users) private usersRepo: Repository<Users>,
        @InjectRepository(Institution) private institutionRepo: Repository<Institution>,
        @InjectRepository(UsersInstitution) private usersInstitutionRepo: Repository<UsersInstitution>
    ) { }

    async find(userId: number, paginationOptions: PaginationOptions) {
        try {
            const list = this.usersInstitutionRepo.createQueryBuilder('usersInstitution')
                .leftJoin(
                    'usersInstitution.user',
                    'users',
                )
                .leftJoin(
                    'usersInstitution.institution',
                    'institution',
                )
                .where(
                    `(LOWER(users.name) LIKE :search
                    OR LOWER(institution.name) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                )
                .andWhere('users.id = :userId', { userId : userId});
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.usersInstitutionRepo.findOne(id);
            if (!data) throw new Error(`User Institution with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async joinInstitution(id: number, dto: JoinInstitution) {
        try {
            // console.log(dto)
            const user = await this.usersRepo.findOne(id)
            const where = {
                invitationCode: dto.code
            }
            const institution = await this.institutionRepo.findOne(where)
            if (!institution) throw new Error(`Institution with code = ${dto.code} is not found`);

            const userData = new UsersInstitution()
            userData.user = user;
            userData.institution = institution
            
            await this.usersInstitutionRepo.save(userData);
            return userData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async user(id: number) {
        try {
            const data = await this.usersInstitutionRepo.findOne(id, { relations: ['user'] });
            if (!data) throw new Error(`User Institution with idx = ${id} is not found`);
            return data.user;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async institution(id: number) {
        try {
            const data = await this.usersInstitutionRepo.findOne(id, { relations: ['institution'] });
            if (!data) throw new Error(`User Institution with idx = ${id} is not found`);
            return data.institution;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
