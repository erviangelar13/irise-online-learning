import { Inject, UseGuards } from "@nestjs/common";
import { Args, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { AuthUser } from "../../../decorators/auth.decorator";
import { Scopes } from "../../../decorators/scopes.decorator";
import { Institution } from "../../../entities/institution.entity";
import { UsersInstitution } from "../../../entities/users-institution.entity";
import { Users } from "../../../entities/users.entity";
import { Oauth2Guard } from "../../../guards/guards";
import { Auth } from "../../../helpers/auth.helper";
import { createPaginationOptions, PaginationInputType } from "../../../helpers/pagination.helper";
import { ResponseMutation } from "../../../helpers/response.helper";
import { JoinInstitution, UserInstitutionPagination } from "./user-institution.dto";
import { UsersInstitutionService } from "./user-institution.service";

@Resolver(of => UsersInstitution)
export class UsersInstituionResolver {

    constructor(
        @Inject(UsersInstitutionService) private usersInstitutionService: UsersInstitutionService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UserInstitutionPagination)
    async userInstitutionList(
        @AuthUser() auth: Auth,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.usersInstitutionService.find(auth.id, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersInstitution)
    async userInstitution(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.usersInstitutionService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async joinInstitution(
        @AuthUser() auth: Auth,
        @Args({ name: 'form', type: () => JoinInstitution }) form: JoinInstitution,
    ) {
        try {
            // console.log(form)
            const data = await this.usersInstitutionService.joinInstitution(auth.id, form);
            return { id: data.id, message: 'Success join institution' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => Users)
    async user(@Parent() usersInstitution: UsersInstitution) {
        const { id } = usersInstitution;
        return this.usersInstitutionService.user(id);
    }

    @ResolveField(returns => Institution)
    async institution(@Parent() usersInstitution: UsersInstitution) {
        const { id } = usersInstitution;
        return this.usersInstitutionService.institution(id);
    }

    @ResolveField(returns => Institution)
    async course(@Parent() usersInstitution: UsersInstitution) {
        const { id } = usersInstitution;
        return this.usersInstitutionService.institution(id);
    }

}
