import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ContentMedia } from '../../../entities/content-media.entity';
import { Content } from '../../../entities/content.entity';
import { MasterReference } from '../../../entities/master-reference.entity';
import { join } from 'path';
import * as fs from 'fs';
import { getMime } from '../../../helpers/mime.helper';
import { Config } from '../../../helpers/config.helper';
import { ContentMediaInput } from './content-media.dto';
import { IUploadOptions } from '../../../helpers/file.helper';

@Injectable()
export class ContentMediaService {

    constructor(
        @InjectRepository(ContentMedia) private contentMediaRepo: Repository<ContentMedia>,
        @InjectRepository(Content) private contentRepo: Repository<Content>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
    ) { }

    async findById(id: number) {
        try {
            const data = await this.contentMediaRepo.findOne(id);
            if (!data) throw new Error(`Content media with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async mediaType(mediaTypeId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'file_type', idx: mediaTypeId });
            if (!data) throw new Error(`Media type with idx = ${mediaTypeId} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async viewMedia(id: number) {
        try {
            const data = await this.findById(id);
            const path = join(`${Config.get('BASE_PATH_FILE')}content/media`, data.mediaName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: ContentMediaInput, attachment: IUploadOptions) {
        try {
            const contentData = await this.contentRepo.findOne(dto.contentId);
            if (!contentData) throw new Error(`Content with id = ${dto.contentId} is not found`);

            const data = new ContentMedia();
            data.mediaName = attachment.filename;
            data.mediaOriginalName = attachment.originalname;
            data.mediaPath = `${Config.get('BASE_PATH_FILE')}content/media/`;
            data.mediaExtension = attachment.filename.substring(attachment.filename.lastIndexOf('.'), attachment.filename.length);
            data.mediaTypeId = dto.mediaTypeId;
            data.content = contentData;

            await this.contentMediaRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.contentMediaRepo.delete(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
