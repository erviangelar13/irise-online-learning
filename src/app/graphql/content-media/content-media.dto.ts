import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class ContentMediaInput {

    @Field()
    contentId: number;

    @Field()
    mediaTypeId: number;
}
