import { Module } from '@nestjs/common';
import { ContentMediaResolver } from './content-media.resolver';
import { ContentMediaService } from './content-media.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContentMedia } from '../../../entities/content-media.entity';
import { Content } from '../../../entities/content.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [ContentMediaResolver, ContentMediaService],
    imports: [
        TypeOrmModule.forFeature([ContentMedia, Content, MasterReference]),
    ],
})
export class ContentMediaModule { }
