import { Resolver, ResolveField, Parent, Mutation, Args } from '@nestjs/graphql';
import { ContentMedia } from '../../../entities/content-media.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { ContentMediaService } from './content-media.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { ResponseMutation } from '../../../helpers/response.helper';
import { removeFile } from '../../../helpers/file.helper';
import { Config } from '../../../helpers/config.helper';

@Resolver(of => ContentMedia)
export class ContentMediaResolver {

    constructor(
        @Inject(ContentMediaService) private contentMediaService: ContentMediaService,
    ) { }

    @ResolveField(returns => String)
    async mediaURL(@Parent() contentMedia: ContentMedia) {
        const { id, mediaName } = contentMedia;
        return mediaName ? `${Config.get('BASE_URL')}view/content/media/${id}` : null;
    }

    @ResolveField(returns => String)
    async fileType(@Parent() contentMedia: ContentMedia) {
        const { mediaTypeId } = contentMedia;
        const data = await this.contentMediaService.mediaType(mediaTypeId);
        return data.name;
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteContentMedia(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.contentMediaService.delete(id);
            if (data.mediaName) await removeFile(data.mediaPath, data.mediaName);
            return { id: data.id, message: 'Success delete content media' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
