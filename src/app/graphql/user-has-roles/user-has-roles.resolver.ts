import { Resolver, ResolveField, Parent, Mutation, Args } from '@nestjs/graphql';
import { Inject, UseGuards } from '@nestjs/common';
import { UserHasRolesService } from './user-has-roles.service';
import { UserHasRoles } from '../../../entities/user-has-roles.entity';
import { Roles } from '../../../entities/roles.entity';
import { Users } from '../../../entities/users.entity';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => UserHasRoles)
export class UserHasRolesResolver {

    constructor(
        @Inject(UserHasRolesService) private userHasRolesService: UserHasRolesService,
    ) { }

    @ResolveField(returns => Users)
    async user(@Parent() userHasRole: UserHasRoles) {
        const { id } = userHasRole;
        return this.userHasRolesService.findUserById(id);
    }

    @ResolveField(returns => Roles)
    async role(@Parent() userHasRole: UserHasRoles) {
        const { id } = userHasRole;
        return this.userHasRolesService.findRoleById(id);
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateUserHasRoles(
        @Args({ name: 'userId', type: () => Number }) userId: number,
        @Args({ name: 'roles', type: () => [String] }) roles: string[],
    ) {
        try {
            await this.userHasRolesService.update(userId, roles);
            return { id: null, message: 'Success update user has roles' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
