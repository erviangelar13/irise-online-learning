import { Module } from '@nestjs/common';
import { UserHasRolesResolver } from './user-has-roles.resolver';
import { UserHasRolesService } from './user-has-roles.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserHasRoles } from '../../../entities/user-has-roles.entity';
import { Roles } from '../../../entities/roles.entity';
import { Users } from '../../../entities/users.entity';

@Module({
    providers: [UserHasRolesResolver, UserHasRolesService],
    imports: [TypeOrmModule.forFeature([UserHasRoles, Roles, Users])],
})
export class UserHasRolesModule { }
