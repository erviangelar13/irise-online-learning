import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { UserHasRoles } from '../../../entities/user-has-roles.entity';
import { Roles } from '../../../entities/roles.entity';
import { Users } from '../../../entities/users.entity';

@Injectable()
export class UserHasRolesService {

    constructor(
        @InjectRepository(UserHasRoles) private userHasRolesRepo: Repository<UserHasRoles>,
    ) { }

    async findUserById(id: number) {
        try {
            const data = await this.userHasRolesRepo.findOne(id, { relations: ['user'] });
            return data.user;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findRoleById(id: number) {
        try {
            const data = await this.userHasRolesRepo.findOne(id, { relations: ['role'] });
            return data.role;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(userId: number, newRoles: string[]) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            const usersRepo = queryRunner.manager.getRepository(Users);
            const userData = await usersRepo.findOne(userId);
            if (!userData) throw new Error(`Users with id = ${userId} is not found`);

            const userHasRolesRepo = queryRunner.manager.getRepository(UserHasRoles);
            await userHasRolesRepo.createQueryBuilder()
                .delete()
                .where('user_id = :userId', { userId: userId })
                .execute();

            const rolesRepo = queryRunner.manager.getRepository(Roles);
            const roles = await rolesRepo.find();
            const rolesData: UserHasRoles[] = [];

            for (const role of newRoles) {
                const filterRole = roles.filter(filterItem => {
                    return filterItem.name === role;
                });
                if (filterRole.length) {
                    const roleUser = new UserHasRoles();
                    roleUser.user = userData;
                    roleUser.role = filterRole[0];
                    rolesData.push(roleUser);
                }
            }

            await userHasRolesRepo.save(rolesData);

            await queryRunner.commitTransaction();

            return rolesData;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }
}
