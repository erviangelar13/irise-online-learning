import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { CourseTags } from '../../../entities/course-tags.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { CourseTagsService } from './course-tags.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { CourseTagsPagination, CourseTagsInput } from './course-tags.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => CourseTags)
export class CourseTagsResolver {

    constructor(
        @Inject(CourseTagsService) private courseTagsService: CourseTagsService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseTagsPagination)
    async courseTagsList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.courseTagsService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseTags)
    async courseTags(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.courseTagsService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseTags(
        @Args({ name: 'courseTagsInput', type: () => CourseTagsInput }) courseTagsInput: CourseTagsInput,
    ) {
        try {
            const data = await this.courseTagsService.create(courseTagsInput);
            return { id: data.id, message: 'Success create course tags' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourseTags(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseTagsInput', type: () => CourseTagsInput }) courseTagsInput: CourseTagsInput,
    ) {
        try {
            const data = await this.courseTagsService.update(id, courseTagsInput);
            return { id: data.id, message: 'Success update course tags' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteCourseTags(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseTagsService.delete(id);
            return { id: data.id, message: 'Success delete course tags' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
