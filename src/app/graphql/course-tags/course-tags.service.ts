import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CourseTags } from '../../../entities/course-tags.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { CourseTagsInput } from './course-tags.dto';

@Injectable()
export class CourseTagsService {

    constructor(
        @InjectRepository(CourseTags) private courseTagsRepo: Repository<CourseTags>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.courseTagsRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.courseTagsRepo.findOne(id);
            if (!data) throw new Error(`Course tags with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: CourseTagsInput) {
        try {
            const data = new CourseTags();
            data.name = dto.name;
            data.description = dto.description;
            await this.courseTagsRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: CourseTagsInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.description = dto.description;
            await this.courseTagsRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.courseTagsRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
