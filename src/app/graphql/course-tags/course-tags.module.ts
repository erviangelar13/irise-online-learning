import { Module } from '@nestjs/common';
import { CourseTagsResolver } from './course-tags.resolver';
import { CourseTagsService } from './course-tags.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseTags } from '../../../entities/course-tags.entity';

@Module({
    providers: [CourseTagsResolver, CourseTagsService],
    imports: [
        TypeOrmModule.forFeature([CourseTags]),
    ],
})
export class CourseTagsModule { }
