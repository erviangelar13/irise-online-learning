import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { CourseTags } from "../../../entities/course-tags.entity";


@InputType()
export class CourseTagsInput {

    @Field()
    name: string;

    @Field()
    description: string;
}

@ObjectType()
export class CourseTagsPagination extends PaginationPage {
    @Field(type => [CourseTags])
    data: CourseTags[];
}
