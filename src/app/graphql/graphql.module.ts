import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { UsersModule } from './users/users.module';
import { InstructorInformationModule } from './instructor-information/instructor-information.module';
import { UserHasRolesModule } from './user-has-roles/user-has-roles.module';
import { RolesModule } from './roles/roles.module';
import { UserProfileModule } from './user-profile/user-profile.module';
import { UserFcmModule } from './user-fcm/user-fcm.module';
import { MasterReferenceModule } from './master-reference/master-reference.module';
import { MasterCategoryModule } from './master-category/master-category.module';
import { UserBillingInformationModule } from './user-billing-information/user-billing-information.module';
import { MasterTypeModule } from './master-type/master-type.module';
import { ContentModule } from './content/content.module';
import { ContentMediaModule } from './content-media/content-media.module';
import { MembershipTypeModule } from './membership-type/membership-type.module';
import { PaymentMethodModule } from './payment-method/payment-method.module';
import { PaymentProviderModule } from './payment-provider/payment-provider.module';
import { PromoCodeModule } from './promo-code/promo-code.module';
import { CourseModule } from './course/course.module';
import { CourseOrganizerModule } from './course-organizer/course-organizer.module';
import { CourseCategoryModule } from './course-category/course-category.module';
import { CourseModulModule } from './course-modul/course-modul.module';
import { CourseMaterialModule } from './course-material/course-material.module';
import { CourseResourceModule } from './course-resource/course-resource.module';
import { CourseTagsModule } from './course-tags/course-tags.module';
import { CourseScheduleModule } from './course-schedule/course-schedule.module';
import { CourseEvaluationModule } from './course-evaluation/course-evaluation.module';
import { OrdersModule } from './orders/orders.module';
import { InstitutionModule } from './institution/institution.module';
import { TransactionModule } from './transaction/transaction.module';
import { TransactionHistoryModule } from './transaction-history/transaction-history.module';
import { VoucherModule } from './voucher/voucher.module';
import { UserCourseModule } from './user-course/user-course.module';
import { UserCourseMaterialModule } from './user-course-material/user-course-material.module';
import { UserInstitutionModule } from './user-institution/user-institution.module';
import { UserCertificateModule } from './user-certificate/user-certificate.module';

@Module({
    imports: [
        GraphQLModule.forRoot({
            autoSchemaFile: true,
            context: ({ req, res }) => ({ req, res }),
            cors: {
                credentials: true,
                origin: true
            },
        }),
        UsersModule,
        InstructorInformationModule,
        UserHasRolesModule,
        RolesModule,
        UserProfileModule,
        UserFcmModule,
        MasterReferenceModule,
        MasterCategoryModule,
        UserBillingInformationModule,
        MasterTypeModule,
        ContentModule,
        ContentMediaModule,
        MembershipTypeModule,
        PaymentMethodModule,
        PaymentProviderModule,
        PromoCodeModule,
        CourseModule,
        CourseOrganizerModule,
        CourseCategoryModule,
        CourseModulModule,
        CourseMaterialModule,
        CourseResourceModule,
        CourseTagsModule,
        CourseScheduleModule,
        CourseEvaluationModule,
        OrdersModule,
        InstitutionModule,
        TransactionModule,
        TransactionHistoryModule,
        VoucherModule,
        UserCourseModule,
        UserCourseMaterialModule,
        UserInstitutionModule,
        UserCertificateModule
    ],
})
export class GraphqlModule { }
