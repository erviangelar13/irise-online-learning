import { Resolver, ResolveField, Parent, Mutation, Args } from '@nestjs/graphql';
import { UserBillingInformation } from '../../../entities/user-billing-information.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { UserBillingInformationService } from './user-billing-information.service';
import { Users } from '../../../entities/users.entity';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { ResponseMutation } from '../../../helpers/response.helper';
import { UserBillingInformationInput } from './user-billing-information.dto';

@Resolver(of => UserBillingInformation)
export class UserBillingInformationResolver {

    constructor(
        @Inject(UserBillingInformationService) private userBillingInformationService: UserBillingInformationService,
    ) { }

    @ResolveField(returns => Users)
    async user(@Parent() userBillingInformation: UserBillingInformation) {
        const { id } = userBillingInformation;
        return this.userBillingInformationService.findUserById(id);
    }

    @Scopes('create', 'update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async setUserBillingInformation(
        @Args({ name: 'userBillingInformationInput', type: () => UserBillingInformationInput }) userBillingInformationInput: UserBillingInformationInput,
    ) {
        try {
            const data = await this.userBillingInformationService.setUserBillingInformation(userBillingInformationInput);
            return { id: data.id, message: 'Success set user billing information' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
