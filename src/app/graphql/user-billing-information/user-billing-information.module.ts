import { Module } from '@nestjs/common';
import { UserBillingInformationResolver } from './user-billing-information.resolver';
import { UserBillingInformationService } from './user-billing-information.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserBillingInformation } from '../../../entities/user-billing-information.entity';
import { Users } from '../../../entities/users.entity';

@Module({
    providers: [UserBillingInformationResolver, UserBillingInformationService],
    imports: [TypeOrmModule.forFeature([UserBillingInformation, Users])],
})
export class UserBillingInformationModule { }
