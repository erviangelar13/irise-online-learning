import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserBillingInformation } from '../../../entities/user-billing-information.entity';
import { Users } from '../../../entities/users.entity';
import { UserBillingInformationInput } from './user-billing-information.dto';

@Injectable()
export class UserBillingInformationService {

    constructor(
        @InjectRepository(UserBillingInformation) private userBillingInformation: Repository<UserBillingInformation>,
        @InjectRepository(Users) private usersRepo: Repository<Users>,
    ) { }

    async findUserById(id: number) {
        try {
            const data = await this.userBillingInformation.findOne(id, { relations: ['user'] });
            return data.user;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async setUserBillingInformation(dto: UserBillingInformationInput) {
        try {
            const userData = await this.usersRepo.findOne(dto.userId);
            if (!userData) throw new Error(`User with id = ${dto.userId} is not found`);
            const data = await this.userBillingInformation.createQueryBuilder()
                .where('user_id = :userId', { userId: dto.userId })
                .getOne();

            data.name = dto.name;
            data.email = dto.email;
            data.phone = dto.phone;
            data.nationality = dto.nationality;
            data.address = dto.address;
            data.address2 = dto.address2;
            data.city = dto.city;
            data.province = dto.province;
            data.postalCode = dto.postalCode;
            await this.userBillingInformation.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
