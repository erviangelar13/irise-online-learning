import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class UserBillingInformationInput {

    @Field()
    userId: number;

    @Field()
    name: string;

    @Field()
    email: string;

    @Field()
    phone: string;

    @Field()
    nationality: string;

    @Field()
    address: string;

    @Field()
    address2: string;

    @Field()
    city: string;

    @Field()
    province: string;

    @Field()
    postalCode: string;
}
