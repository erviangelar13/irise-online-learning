import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from '../../../entities/course.entity';
import { getConnection, In, Repository } from 'typeorm';
import { CourseFilter, CourseInput, SearchFilter } from './course.dto';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { generateRandomCode } from '../../../helpers/generate.helper';
import { MasterReference } from '../../../entities/master-reference.entity';
import { MasterType } from '../../../entities/master-type.entity';
import { MasterCategory } from '../../../entities/master-category.entity';
import { CourseOrganizer } from '../../../entities/course-organizer.entity';
import { dateMoment } from '../../../helpers/date.helper';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { CourseCategory } from '../../../entities/course-category.entity';
import { Config } from '../../../helpers/config.helper';
import { join } from 'path';
import * as fs from 'fs';
import { getMime } from '../../../helpers/mime.helper';
import { IUploadOptions } from '../../../helpers/file.helper';

@Injectable()
export class CourseService {

    constructor(
        @InjectRepository(Course) private courseRepo: Repository<Course>,
        @InjectRepository(MasterType) private masterTypeRepo: Repository<MasterType>,
        @InjectRepository(MasterCategory) private masterCategoryRepo: Repository<MasterCategory>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
        @InjectRepository(CourseOrganizer) private courseOrganizerRepo: Repository<CourseOrganizer>,
        @InjectRepository(InstructorInformation) private instructorInformationRepo: Repository<InstructorInformation>,
    ) { }

    async find(filter: CourseFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.courseRepo.createQueryBuilder('course')
                .leftJoin(
                    'course.courseCategory',
                    'courseCategory',
                )
                .where(
                    `(LOWER(course.name) LIKE :search
                    OR LOWER(course.tags) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            if (filter.courseType) list.andWhere('course.course_type = :courseType', { courseType: filter.courseType });
            if (filter.organizerId) list.andWhere('course.organizer_id = :organizerId', { organizerId: filter.organizerId });
            if (filter.categoryId) list.andWhere('courseCategory.id = :categoryId', { categoryId: filter.categoryId });
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findPublished(filter: CourseFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.courseRepo.createQueryBuilder('course')
                .leftJoin(
                    'course.courseCategory',
                    'courseCategory',
                )
                .where(
                    `(LOWER(course.name) LIKE :search
                    OR LOWER(course.tags) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                )
                .andWhere('course.courseStatusId = 1');
            if (filter.courseType) list.andWhere('course.course_type = :courseType', { courseType: filter.courseType });
            if (filter.organizerId) list.andWhere('course.organizer_id = :organizerId', { organizerId: filter.organizerId });
            if (filter.categoryId) list.andWhere('courseCategory.id = :categoryId', { categoryId: filter.categoryId });
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList(filter: SearchFilter) {
        try {
            const list = this.courseRepo.createQueryBuilder('course')
                .leftJoin(
                    'course.courseCategory',
                    'courseCategory',
                )
                .where(
                    `(LOWER(course.name) LIKE :search
                    OR LOWER(course.tags) LIKE :search)`,
                    { search: '%' + filter.keyword + '%' },
                );
            return await list.getMany();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['courseCategory', 'courseCategory.category'] });
            if (!data) throw new Error(`Course with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findByCode(code: string) {
        try {
            const where = {
                code : code
            }
            const data = await this.courseRepo.findOne(where, { relations: ['courseCategory', 'courseCategory.category'] });
            if (!data) throw new Error(`Course with code = ${code} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findMasterTypeById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['courseType'] });
            return data.courseType;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findCourseCategoryById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['courseCategory', 'courseCategory.category'] });
            return data.courseCategory;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async courseCategoryGrid(id: number) {
        try {
            const data = await this.findCourseCategoryById(id);
            let courseCategory = '';
            for (const item of data) {
                courseCategory += `${item.category.name}, `;
            }

            if (courseCategory) {
                courseCategory = courseCategory.substring(0, (courseCategory.length - 2));
            }

            return courseCategory;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findLanguageById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'language', idx: idx });
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrganizerById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['courseOrganizer'] });
            return data.courseOrganizer;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findInstructorById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['instructorInformation'] });
            return data.instructorInformation;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findAccessTypeById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'access_type', idx: idx });
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findCourseModulById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['courseModul'] });
            return data.courseModul.sort(x => x.seqNo);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findCourseStatusById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'course_status', idx: idx });
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findCourseScheduleById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['courseSchedule'] });
            return data.courseSchedule;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findLearnActiveById(id: number) {
        try {
            const data = await this.courseRepo.findOne(id, { relations: ['courseModul', 'courseModul.courseMaterial', 'courseModul.courseMaterial.courseResource', 'courseModul.courseMaterial.usersCourseMaterial']});
            // console.log(data)
            var activeMaterial = null
            const sortedModule = [...data.courseModul].sort((a, b) => (a.seqNo < b.seqNo ? -1 : 1));
            for await (const module of sortedModule) {
                const sortedMaterial = [...module.courseMaterial].sort((a, b) => (a.id < b.id ? -1 : 1));
                for await (const material of sortedMaterial) {
                    if(material.usersCourseMaterial.length == 0) {
                        material.viewed = false;
                        activeMaterial = material;
                        break;
                    }
                }
                if(activeMaterial != null) {
                    break;
                }
            }
            if(activeMaterial == null) {
                var cMaterials = [...sortedModule[0].courseMaterial].sort((a, b) => (a.id < b.id ? -1 : 1));
                if(cMaterials.length > 0) {
                    var cMaterial = cMaterials[0];
                    cMaterial.viewed = true;
                    return cMaterial;
                }
            }
            return activeMaterial;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async viewCover(id: number) {
        try {
            const data = await this.findById(id);
            // console.log(data.coverName);
            // console.log(join(`${Config.get('BASE_PATH_FILE')}course_cover/`, data.coverName));
            const path = join(`${Config.get('BASE_PATH_FILE')}course_cover/`, data.coverName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: CourseInput) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const courseValidationData = await this.courseValidation(dto);

            const courseData = new Course();
            courseData.name = dto.name;
            courseData.code = await generateRandomCode(Course, `CRS${dateMoment().format('ss')}`, 15, queryRunner);
            courseData.courseType = courseValidationData.courseTypeData;
            courseData.languageId = dto.languageId;
            courseData.information = dto.information;
            courseData.tags = dto.tags;
            courseData.courseOrganizer = courseValidationData.courseOrganizerData;
            courseData.accessTypeId = dto.accessTypeId;
            courseData.duration = dto.duration;
            courseData.periode = dto.periode;
            courseData.quota = dto.quota;
            courseData.currentQuota = dto.quota;
            courseData.price = dto.price;
            courseData.evaluation = dto.evaluation;
            courseData.certificate = dto.certificate;
            courseData.courseStatusId = dto.courseStatusId;
            courseData.instructorInformation = courseValidationData.instructorInformationData;

            await queryRunner.manager.save(courseData);

            const courseCategoryDataList: CourseCategory[] = [];
            for (const categoryData of courseValidationData.categoryData) {
                const courseCategoryData = new CourseCategory();
                courseCategoryData.course = courseData;
                courseCategoryData.category = categoryData;
                courseCategoryDataList.push(courseCategoryData);
            }

            if (courseCategoryDataList.length) {
                await queryRunner.manager.save(courseCategoryDataList);
            }

            await queryRunner.commitTransaction();

            return courseData;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async update(id: number, dto: CourseInput) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const courseData = await this.findById(id);
            const courseValidationData = await this.courseValidation(dto);

            courseData.name = dto.name;
            courseData.courseType = courseValidationData.courseTypeData;
            courseData.languageId = dto.languageId;
            courseData.information = dto.information;
            courseData.tags = dto.tags;
            courseData.courseOrganizer = courseValidationData.courseOrganizerData;
            courseData.accessTypeId = dto.accessTypeId;
            courseData.duration = dto.duration;
            courseData.periode = dto.periode;
            courseData.quota = dto.quota;
            courseData.price = dto.price;
            courseData.evaluation = dto.evaluation;
            courseData.courseStatusId = courseData.courseStatusId ? courseData.courseStatusId : 0;
            courseData.instructorInformation = courseValidationData.instructorInformationData;

            // courseData.courseCategory.
            queryRunner.manager.remove(courseData.courseCategory.filter(fItem => {
                return !courseValidationData.categoryData.some(x => { return x.id === fItem.category.id });
            }));

            await queryRunner.manager.save(courseData);

            const courseCategoryDataList: CourseCategory[] = [];
            for (const categoryData of courseValidationData.categoryData.filter(fItem => {
                return !courseData.courseCategory.map(x => x.category).some(x => { return x.id === fItem.id });
            })) {
                const courseCategoryData = new CourseCategory();
                courseCategoryData.course = courseData;
                courseCategoryData.category = categoryData;
                courseCategoryDataList.push(courseCategoryData);
            }

            if (courseCategoryDataList.length) {
                await queryRunner.manager.save(courseCategoryDataList);
            }

            await queryRunner.commitTransaction();

            return courseData;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async setCover(id: number, attachment: IUploadOptions) {
        try {
            const data = await this.findById(id);
            const oldCoverPath = data.coverPath && data.coverName ? join(data.coverPath, data.coverName) : null;

            data.coverName = attachment.filename;
            data.coverOriginalName = attachment.originalname;
            data.coverPath = `${Config.get('BASE_PATH_FILE')}course_cover/`;
            data.coverExtension = attachment.filename.substring(attachment.filename.lastIndexOf('.'), attachment.filename.length);

            await this.courseRepo.save(data);
            return oldCoverPath;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async coursePublishUnpublish(courseId: number, courseStatusId: number) {
        try {
            const courseStatusData = await this.masterReferenceRepo.findOne({ group: 'course_status', idx: courseStatusId });
            if (!courseStatusData) throw new Error(`Course status with id = ${courseStatusId} is not found`);
            const courseData = await this.findById(courseId);
            courseData.courseStatusId = courseStatusId;
            await this.courseRepo.save(courseData);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    private async courseValidation(dto: CourseInput) {
        try {
            const courseTypeData = await this.masterTypeRepo.findOne(dto.courseType);
            if (!courseTypeData) throw new Error(`Course type with id = ${dto.courseType} is not found`);

            let categoryData: MasterCategory[] = [];
            if (dto.categoryId.length) {
                categoryData = await this.masterCategoryRepo.find({ id: In(dto.categoryId) });
            }

            const languageData = await this.masterReferenceRepo.findOne({ group: 'language', idx: dto.languageId });
            if (!languageData) throw new Error(`Language with id = ${dto.languageId} is not found`);

            const courseOrganizerData = await this.courseOrganizerRepo.findOne(dto.organizerId);
            if (dto.organizerId && !courseOrganizerData) throw new Error(`Course organizer with id = ${dto.organizerId} is not found`);

            const instructorInformationData = await this.instructorInformationRepo.findOne(dto.instructorId);
            if (dto.instructorId && !instructorInformationData) throw new Error(`Instructor with id = ${dto.instructorId} is not found`);

            const accessTypeData = await this.masterReferenceRepo.findOne({ group: 'access_type', idx: dto.accessTypeId });
            if (!accessTypeData) throw new Error(`Access type with id = ${dto.accessTypeId} is not found`);

            // const courseStatusData = await this.masterReferenceRepo.findOne({ group: 'course_status', idx: dto.courseStatusId });
            // if (!courseStatusData) throw new Error(`Course status with id = ${dto.courseStatusId} is not found`);

            return {
                courseTypeData,
                categoryData,
                languageData,
                courseOrganizerData,
                instructorInformationData,
                accessTypeData,
                // courseStatusData,
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }

}
