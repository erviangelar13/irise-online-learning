import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Course } from "../../../entities/course.entity";

@InputType()
export class CourseFilter {

    @Field({ nullable: true })
    courseType: number;

    @Field({ nullable: true })
    categoryId: number;

    @Field({ nullable: true })
    organizerId: number;
}

@InputType()
export class SearchFilter {

    @Field({ nullable: true })
    keyword: string;
}

@InputType()
export class CourseInput {

    @Field()
    name: string;

    @Field()
    courseType: number;

    @Field(type => [Number])
    categoryId: number[];

    @Field({ nullable: true })
    tags: string;

    @Field({ nullable: true })
    periode: number;

    @Field()
    languageId: number;

    @Field({ nullable: true })
    organizerId: number;

    @Field({ nullable: true })
    instructorId: number;

    @Field()
    information: string;

    @Field()
    duration: number;

    @Field()
    accessTypeId: number;

    @Field()
    quota: number;

    @Field()
    price: number;

    @Field()
    evaluation: boolean;

    @Field({ nullable: true})
    certificate: boolean;

    @Field({ nullable: true})
    courseStatusId: number;
}

@ObjectType()
export class CoursePagination extends PaginationPage {
    @Field(type => [Course])
    data: Course[];
}
