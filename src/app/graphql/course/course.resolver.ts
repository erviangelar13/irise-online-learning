import { Inject, UseGuards } from '@nestjs/common';
import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { createPaginationOptions, PaginationInputType } from '../../../helpers/pagination.helper';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Course } from '../../../entities/course.entity';
import { CourseFilter, CourseInput, CoursePagination, SearchFilter } from './course.dto';
import { CourseService } from './course.service';
import { MasterType } from '../../../entities/master-type.entity';
import { CourseOrganizer } from '../../../entities/course-organizer.entity';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { CourseCategory } from '../../../entities/course-category.entity';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { ResponseMutation } from '../../../helpers/response.helper';
import { CourseModul } from '../../../entities/course-modul.entity';
import { CourseSchedule } from '../../../entities/course-schedule.entity';
import { Config } from '../../../helpers/config.helper';
import { CourseMaterial } from '../../../entities/course-material.entity';

@Resolver(of => Course)
export class CourseResolver {

    constructor(
        @Inject(CourseService) private courseService: CourseService,
    ) { }

    @Query(returns => CoursePagination)
    async courseList(
        @Args({ name: 'courseFilter', type: () => CourseFilter, nullable: true })
        courseFilter: CourseFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.courseService.find(courseFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Query(returns => CoursePagination)
    async courseClientList(
        @Args({ name: 'courseFilter', type: () => CourseFilter, nullable: true })
        courseFilter: CourseFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.courseService.findPublished(courseFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Query(returns => [Course])
    async courses(
        @Args({ name: 'searchFilter', type: () => SearchFilter, nullable: true })
        searchFilter: SearchFilter
    ) {
        try {
            return await this.courseService.findList(searchFilter);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Query(returns => Course)
    async course(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.courseService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Query(returns => Course)
    async courseData(
        @Args({ name: 'code', type: () => String }) code: string,
    ) {
        try {
            return await this.courseService.findByCode(code);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => MasterType)
    async courseType(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.findMasterTypeById(id);
    }

    @ResolveField(returns => [CourseCategory])
    async courseCategory(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.findCourseCategoryById(id);
    }

    @ResolveField(returns => String)
    async courseCategoryGrid(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.courseCategoryGrid(id);
    }

    @ResolveField(returns => String)
    async language(@Parent() course: Course) {
        const { languageId } = course;
        return this.courseService.findLanguageById(languageId);
    }

    @ResolveField(returns => CourseOrganizer)
    async courseOrganizer(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.findOrganizerById(id);
    }

    @ResolveField(returns => InstructorInformation)
    async instructorInformation(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.findInstructorById(id);
    }

    @ResolveField(returns => String)
    async accessType(@Parent() course: Course) {
        const { accessTypeId } = course;
        return this.courseService.findAccessTypeById(accessTypeId);
    }

    @ResolveField(returns => [CourseModul])
    async courseModul(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.findCourseModulById(id);
    }

    @ResolveField(returns => String)
    async courseStatus(@Parent() course: Course) {
        const { courseStatusId } = course;
        return this.courseService.findCourseStatusById(courseStatusId);
    }

    @ResolveField(returns => String)
    async coverURL(@Parent() course: Course) {
        const { id, coverName } = course;
        return coverName ? `${Config.get('BASE_URL')}view/course/cover/${id}` : null;
    }

    @ResolveField(returns => [CourseSchedule])
    async courseSchedule(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.findCourseScheduleById(id);
    }

    @ResolveField(returns => CourseMaterial)
    async activeLearn(@Parent() course: Course) {
        const { id } = course;
        return this.courseService.findLearnActiveById(id);
    }

    @Scopes('create')
    @RolesGuard('admin', 'operator')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourse(
        @Args({ name: 'courseInput', type: () => CourseInput }) courseInput: CourseInput,
    ) {
        try {
            const data = await this.courseService.create(courseInput);
            return { id: data.id, code: data.code, message: 'Success create course' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin', 'operator')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourse(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseInput', type: () => CourseInput }) courseInput: CourseInput,
    ) {
        try {
            const data = await this.courseService.update(id, courseInput);
            return { id: data.id, message: 'Success update course' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin', 'operator')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async publishCourse(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseService.coursePublishUnpublish(id, 1);
            return { id: id, message: 'Success publish course' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin', 'operator')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async unpublishCourse(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseService.coursePublishUnpublish(id, 0);
            return { id: id, data: data, message: 'Success unpublish course' };
        } catch (error) {
            throw new Error(error.message);
        }
    }


}
