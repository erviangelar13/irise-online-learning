import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MasterReference } from '../../../entities/master-reference.entity';
import { Course } from '../../../entities/course.entity';
import { CourseResolver } from './course.resolver';
import { CourseService } from './course.service';
import { MasterType } from '../../../entities/master-type.entity';
import { MasterCategory } from '../../../entities/master-category.entity';
import { CourseOrganizer } from '../../../entities/course-organizer.entity';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { CourseMaterial } from '../../../entities/course-material.entity';

@Module({
    providers: [CourseResolver, CourseService],
    imports: [
        TypeOrmModule.forFeature([
            Course,
            MasterType,
            MasterCategory,
            MasterReference,
            CourseOrganizer,
            InstructorInformation,
            CourseMaterial
        ]),
    ],
})
export class CourseModule { }
