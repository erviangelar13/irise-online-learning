import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { PaymentMethod } from "../../../entities/payment-method.entity";


@InputType()
export class PaymentMethodInput {

    @Field()
    name: string;

    @Field()
    description: string;

    @Field()
    termAndCondition: string;
}

@ObjectType()
export class PaymentMethodPagination extends PaginationPage {
    @Field(type => [PaymentMethod])
    data: PaymentMethod[];
}
