import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentMethod } from '../../../entities/payment-method.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { PaymentMethodInput } from './payment-method.dto';
import { IUploadOptions } from '../../../helpers/file.helper';
import { Config } from '../../../helpers/config.helper';
import { join } from 'path';
import * as fs from 'fs';
import { getMime } from '../../../helpers/mime.helper';

@Injectable()
export class PaymentMethodService {

    constructor(
        @InjectRepository(PaymentMethod) private paymentMethodRepo: Repository<PaymentMethod>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.paymentMethodRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findAll() {
        try {
            return await this.paymentMethodRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.paymentMethodRepo.findOne(id);
            if (!data) throw new Error(`Payment method with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async viewLogo(id: number) {
        try {
            const data = await this.findById(id);
            const path = join(`${Config.get('BASE_PATH_FILE')}payment_method/`, data.logoName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: PaymentMethodInput) {
        try {
            const data = new PaymentMethod();
            data.name = dto.name;
            data.description = dto.description;
            data.termAndCondition = dto.termAndCondition;
            await this.paymentMethodRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: PaymentMethodInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.description = dto.description;
            data.termAndCondition = dto.termAndCondition;
            await this.paymentMethodRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.paymentMethodRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async setLogo(id: number, attachment: IUploadOptions) {
        try {
            const data = await this.findById(id);
            const oldLogoPath = data.logoPath && data.logoName ? join(data.logoPath, data.logoName) : null;

            data.logoName = attachment.filename;
            data.logoOriginalName = attachment.originalname;
            data.logoPath = `${Config.get('BASE_PATH_FILE')}payment_method/`;
            data.logoExtension = attachment.filename.substring(attachment.filename.lastIndexOf('.'), attachment.filename.length);

            await this.paymentMethodRepo.save(data);
            return oldLogoPath;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
