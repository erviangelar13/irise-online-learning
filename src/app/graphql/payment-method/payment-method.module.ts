import { Module } from '@nestjs/common';
import { PaymentMethodResolver } from './payment-method.resolver';
import { PaymentMethodService } from './payment-method.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentMethod } from '../../../entities/payment-method.entity';

@Module({
    providers: [PaymentMethodResolver, PaymentMethodService],
    imports: [
        TypeOrmModule.forFeature([
            PaymentMethod,
        ]),
    ],
})
export class PaymentMethodModule { }
