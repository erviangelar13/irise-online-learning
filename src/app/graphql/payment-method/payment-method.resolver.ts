import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { PaymentMethod } from '../../../entities/payment-method.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { PaymentMethodService } from './payment-method.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { PaymentMethodPagination, PaymentMethodInput } from './payment-method.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { Config } from '../../../helpers/config.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { removeFile } from '../../../helpers/file.helper';

@Resolver(of => PaymentMethod)
export class PaymentMethodResolver {

    constructor(
        @Inject(PaymentMethodService) private paymentMethodService: PaymentMethodService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => PaymentMethodPagination)
    async paymentMethodList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.paymentMethodService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [PaymentMethod])
    async paymentMethods() {
        try {
            return await this.paymentMethodService.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => PaymentMethod)
    async paymentMethod(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.paymentMethodService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async logoURL(@Parent() paymentMethod: PaymentMethod) {
        const { id, logoName } = paymentMethod;
        return logoName ? `${Config.get('BASE_URL')}view/payment-method/logo/${id}` : null;
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createPaymentMethod(
        @Args({ name: 'paymentMethodInput', type: () => PaymentMethodInput }) paymentMethodInput: PaymentMethodInput,
    ) {
        try {
            const data = await this.paymentMethodService.create(paymentMethodInput);
            return { id: data.id, message: 'Success create payment method' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updatePaymentMethod(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'paymentMethodInput', type: () => PaymentMethodInput }) paymentMethodInput: PaymentMethodInput,
    ) {
        try {
            const data = await this.paymentMethodService.update(id, paymentMethodInput);
            return { id: data.id, message: 'Success update payment method' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deletePaymentMethod(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.paymentMethodService.delete(id);
            if (data.logoName) await removeFile(data.logoPath, data.logoName);
            return { id: data.id, message: 'Success delete payment method' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
