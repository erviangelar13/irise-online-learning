import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { Transactions } from '../../../entities/transaction.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { TransactionInput, TransactionFilter } from './transaction.dto';
import { MasterReference } from '../../../entities/master-reference.entity';
import { Orders } from '../../../entities/orders.entity';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { Voucher } from '../../../entities/voucher.entity';
import { dateMoment } from '../../../helpers/date.helper';
import { generateCode, generateInvoice, generateRandomCode } from '../../../helpers/generate.helper';

@Injectable()
export class TransactionService {

    constructor(
        @InjectRepository(Transactions) private transactionRepo: Repository<Transactions>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
        @InjectRepository(Orders) private ordersRepo: Repository<Orders>,
        @InjectRepository(PaymentProvider) private paymentProviderRepo: Repository<PaymentProvider>,
    ) { }

    async find(filter: TransactionFilter,paginationOptions: PaginationOptions) {
        try {
            const list = this.transactionRepo.createQueryBuilder('transactions')
                .leftJoin(
                    'transactions.orders',
                    'Orders'
                )
                .where(
                    `LOWER(transactions.invoiceNo) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            if (filter.paymentProvider) list.andWhere('transaction.paymentProviderId = :provider', { provider: filter.paymentProvider });
            if (filter.paymentType) list.andWhere('transaction.paymentTypeId = :type', { type: filter.paymentType });
            if (filter.dateFrom && filter.dateTo) list.andWhere('transaction.date BETWEEN = :dateFrom AND :dateTo', { dateFrom: filter.dateFrom, dateTo: filter.dateTo });
            if (filter.dateTo) list.andWhere('transaction.date < :dateTo', { dateTo: filter.dateTo });
            if (filter.dateFrom) list.andWhere('transaction.date > :dateFrom', { dateFrom: filter.dateFrom });
            return await list.orderBy("Orders.orderNo","ASC").orderBy("transactions.date","ASC").pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findAll() {
        try {
            return await this.transactionRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.transactionRepo.findOne(id, { relations: ['transactionsHistory','orders'] });
            // console.log(data.transactionsHistory)
            if (!data) throw new Error(`Transaction with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findByInvoiceNo(invoiceNo: string) {
        try {
            const where = {
                invoiceNo: invoiceNo
            }
            const data = await this.transactionRepo.findOne(where, { relations: ['orders','orders.vouchers'] });
            if (!data) throw new Error(`Transaction with invoice no = ${invoiceNo} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderById(id: number) {
        try {
            const data = await this.transactionRepo.findOne(id, { relations: ['orders'] });
            return data.orders;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findPaymentProviderById(id: number) {
        try {
            const data = await this.paymentProviderRepo.findOne(id);
            if (!data) throw new Error(`Payment Provider with id = ${id} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findPaymentTypeById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'payment_type', idx: idx });
            if (!data) throw new Error(`Payment Type with idx = ${idx} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findPaymentStatusById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'payment_status', idx: idx });
            if (!data) throw new Error(`Payment Status with idx = ${idx} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findTransactionHistoryById(id: number) {
        try {
            const data = await this.transactionRepo.findOne(id, { relations: ['transactionsHistory'] });
            // console.log(data)
            return data.transactionsHistory;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async isVoucherIssued(id: number) {
        try {
            const data = await this.transactionRepo.findOne(id, { relations: ['orders', 'orders.vouchers'] });
            return data.orders.vouchers.length > 0;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: TransactionInput) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const ordersValidationData = await this.transactionValidation(dto);

            const transactionCode = await generateCode(Transactions, `TRN_${dateMoment().format('MM')}`, 16, queryRunner);
            const invoiceNo = await generateInvoice(Transactions, 'INV', 12, queryRunner);

            const order = await this.ordersRepo.findOne(dto.orderid);
            order.statusId = 3;
            await queryRunner.manager.save(order);

            const data = new Transactions();
            data.orders = ordersValidationData.order;
            data.date = dateMoment().toDate();
            data.amount = dto.amount;
            data.notes = dto.note;
            data.paymentProviderId = dto.paymentProviderId;
            data.transactionCode = transactionCode;
            data.invoiceNo = invoiceNo;
            data.paymentStatusId = 2;
            data.paymentTypeId = dto.paymentTypeId;

            const transactionHistory: TransactionHistory = new TransactionHistory();
            transactionHistory.transaction = data;
            transactionHistory.date = dateMoment().toDate();
            transactionHistory.info = '';
            transactionHistory.order = ordersValidationData.order;
            transactionHistory.paymentStatusId = 2

            // var item : TransactionHistory[];
            // item.push(transactionHistory);

            // data.transactionsHistory = item;

            await queryRunner.manager.save(data);
            await queryRunner.manager.save(transactionHistory);

            if(dto.voucherIssued) {
                const vouchers: Voucher[] = [];
                for (let index = 0; index < ordersValidationData.order.qty; index++) {
                    const voucher = new Voucher();
                    voucher.code = await generateRandomCode(Voucher, `VC${dateMoment().format('ss')}`, 10, queryRunner);
                    voucher.orders = ordersValidationData.order;
                    voucher.activeDate = dateMoment().toDate();
                    voucher.invitationCode = await generateRandomCode(Transactions, `${transactionCode}_${dateMoment().format('ss')}`, 25, queryRunner);
                    vouchers.push(voucher);
                }
                await queryRunner.manager.save(vouchers);
            }

            await queryRunner.commitTransaction();

            return data;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async update(id: number, dto: TransactionInput) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const data = await this.findById(id);
            data.paymentStatusId = 2;
            data.date = dateMoment().toDate();
            data.amount = dto.amount;
            data.notes = dto.note;
            data.paymentProviderId = dto.paymentProviderId;
            data.paymentStatusId = 2;
            data.paymentTypeId = dto.paymentTypeId;

            await queryRunner.manager.save(data);

            const transactionHistory: TransactionHistory = new TransactionHistory();
            transactionHistory.transaction = data;
            transactionHistory.date = dateMoment().toDate();
            transactionHistory.info = '';
            transactionHistory.order = data.orders;
            transactionHistory.paymentStatusId = 2

            await queryRunner.manager.save(transactionHistory);

            if(dto.voucherIssued && !data.voucherIssued) {
                const vouchers: Voucher[] = [];
                for (let index = 0; index < data.orders.qty; index++) {
                    const voucher = new Voucher();
                    voucher.code = await generateRandomCode(Voucher, `VC${dateMoment().format('ss')}`, 10, queryRunner);
                    voucher.orders = data.orders;
                    voucher.activeDate = dateMoment().toDate();
                    voucher.invitationCode = await generateRandomCode(Transactions, `${data.transactionCode}_${dateMoment().format('ss')}`, 25, queryRunner);
                    vouchers.push(voucher);
                }
                await queryRunner.manager.save(vouchers);
            }

            await queryRunner.commitTransaction();

            return data;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.transactionRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    private async transactionValidation(dto: TransactionInput) {
        try {
            const paymentStatus = await this.masterReferenceRepo.findOne({ group: 'payment_status', idx: 2 });
            if (!paymentStatus) throw new Error(`Payment Status with id = 2 is not found`);

            const paymentType = await this.masterReferenceRepo.findOne({ group: 'payment_type', idx: dto.paymentTypeId });
            if (!paymentType) throw new Error(`Payment Type with id = ${dto.paymentTypeId} is not found`);

            const order = await this.ordersRepo.findOne(dto.orderid);
            if (!order) throw new Error(`Order with id = ${dto.orderid} is not found`);

            const paymentProvider = await this.paymentProviderRepo.findOne(dto.paymentProviderId);
            if (!paymentProvider) throw new Error(`Order with id = ${dto.paymentProviderId} is not found`);

            return {
                paymentStatus,
                paymentType,
                order,
                paymentProvider,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
