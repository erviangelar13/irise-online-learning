import { Module } from '@nestjs/common';
import { TransactionResolver } from './transaction.resolver';
import { TransactionService } from './transaction.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transactions } from '../../../entities/transaction.entity';
import { Orders } from '../../../entities/orders.entity';
import { MasterReference } from '../../../entities/master-reference.entity';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { TransactionHistory } from '../../../entities/transaction-history.entity';

@Module({
    providers: [TransactionResolver, TransactionService],
    imports: [
        TypeOrmModule.forFeature([
            Transactions,
            MasterReference,
            Orders,
            PaymentProvider,
            TransactionHistory
        ]),
    ],
})
export class TransactionModule { }
