import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { Transactions } from '../../../entities/transaction.entity';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { TransactionPagination, TransactionInput, TransactionFilter } from './transaction.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => Transactions)
export class TransactionResolver {

    constructor(
        @Inject(TransactionService) private transactionService: TransactionService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => TransactionPagination)
    async transactionList(
        @Args({ name: 'transactionFilter', type: () => TransactionFilter, nullable: true })
        transactionFilter: TransactionFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.transactionService.find(transactionFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [Transactions])
    async transactions() {
        try {
            return await this.transactionService.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => Transactions)
    async transaction(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.transactionService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => Transactions)
    async transactionVoucher(
        @Args({ name: 'invoiceNo', type: () => String }) invoiceNo: string,
    ) {
        try {
            return await this.transactionService.findByInvoiceNo(invoiceNo);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async paymentProvider(@Parent() transactions: Transactions) {
        const { paymentProviderId } = transactions;
        return this.transactionService.findPaymentProviderById(paymentProviderId);
    }

    @ResolveField(returns => String)
    async paymentStatus(@Parent() transactions: Transactions) {
        const { paymentStatusId } = transactions;
        return this.transactionService.findPaymentStatusById(paymentStatusId);
    }

    @ResolveField(returns => String)
    async paymentType(@Parent() transactions: Transactions) {
        const { paymentTypeId } = transactions;
        return this.transactionService.findPaymentTypeById(paymentTypeId);
    }

    @ResolveField(returns => Boolean)
    async voucherIssued(@Parent() transactions: Transactions) {
        const { id } = transactions;
        return this.transactionService.isVoucherIssued(id);
    }

    @ResolveField(returns => [TransactionHistory])
    async transactionsHistory(@Parent() transactions: Transactions) {
        const { id } = transactions;
        return this.transactionService.findTransactionHistoryById(id);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createTransaction(
        @Args({ name: 'transactionInput', type: () => TransactionInput }) transactionInput: TransactionInput,
    ) {
        try {
            const data = await this.transactionService.create(transactionInput);
            return { id: data.id, message: 'Success create transaction' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateTransaction(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'transactionInput', type: () => TransactionInput }) transactionInput: TransactionInput,
    ) {
        try {
            const data = await this.transactionService.update(id, transactionInput);
            return { id: data.id, message: 'Success update transaction' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteTransaction(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.transactionService.delete(id);
            return { id: data.id, message: 'Success delete transaction' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
