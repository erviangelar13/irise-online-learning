import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Transactions } from "../../../entities/transaction.entity";

@InputType()
export class TransactionFilter {

    @Field({ nullable: true })
    paymentType: number;

    @Field({ nullable: true })
    paymentProvider: number;

    @Field({ nullable: true })
    dateFrom: Date;
    
    @Field({ nullable: true })
    dateTo: Date;
}

@InputType()
export class TransactionInput {

    @Field()
    orderid: number;

    @Field()
    amount: number;

    @Field({ nullable: true })
    note?: string;
    
    @Field()
    paymentProviderId: number;

    @Field()
    paymentTypeId: number;
    
    @Field({ defaultValue: false })
    voucherIssued: boolean;
}

@ObjectType()
export class TransactionPagination extends PaginationPage {
    @Field(type => [Transactions])
    data: Transactions[];
}
