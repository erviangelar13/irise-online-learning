import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { MembershipType, PeriodeUnit } from "../../../entities/membership-type.entity";


@InputType()
export class MembershipTypeInput {

    @Field()
    name: string;

    @Field()
    periode: number;

    @Field(type => PeriodeUnit)
    periodeUnit: PeriodeUnit;

    @Field()
    price: number;

    @Field()
    information: string;
}

@ObjectType()
export class MembershipTypePagination extends PaginationPage {
    @Field(type => [MembershipType])
    data: MembershipType[];
}
