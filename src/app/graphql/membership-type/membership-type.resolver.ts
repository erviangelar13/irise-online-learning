import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { MembershipType } from '../../../entities/membership-type.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { MembershipTypeService } from './membership-type.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { MembershipTypePagination, MembershipTypeInput } from './membership-type.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => MembershipType)
export class MembershipTypeResolver {

    constructor(
        @Inject(MembershipTypeService) private membershipTypeService: MembershipTypeService,
    ) { }

    @Scopes('read')
    @Query(returns => MembershipTypePagination)
    async membershipTypeList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.membershipTypeService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @Query(returns => [MembershipType])
    async membershipTypes() {
        try {
            return await this.membershipTypeService.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @Query(returns => MembershipType)
    async membershipType(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.membershipTypeService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createMembershipType(
        @Args({ name: 'membershipTypeInput', type: () => MembershipTypeInput }) membershipTypeInput: MembershipTypeInput,
    ) {
        try {
            const data = await this.membershipTypeService.create(membershipTypeInput);
            return { id: data.id, message: 'Success create membership type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateMembershipType(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'membershipTypeInput', type: () => MembershipTypeInput }) membershipTypeInput: MembershipTypeInput,
    ) {
        try {
            const data = await this.membershipTypeService.update(id, membershipTypeInput);
            return { id: data.id, message: 'Success update membership type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteMembershipType(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.membershipTypeService.delete(id);
            return { id: data.id, message: 'Success delete membership type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
