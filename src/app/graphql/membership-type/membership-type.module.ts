import { Module } from '@nestjs/common';
import { MembershipTypeResolver } from './membership-type.resolver';
import { MembershipTypeService } from './membership-type.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MembershipType } from '../../../entities/membership-type.entity';

@Module({
    providers: [MembershipTypeResolver, MembershipTypeService],
    imports: [
        TypeOrmModule.forFeature([
            MembershipType,
        ]),
    ],
})
export class MembershipTypeModule { }
