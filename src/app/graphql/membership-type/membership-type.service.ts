import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MembershipType } from '../../../entities/membership-type.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { MembershipTypeInput } from './membership-type.dto';
import { generateCode } from '../../../helpers/generate.helper';

@Injectable()
export class MembershipTypeService {

    constructor(
        @InjectRepository(MembershipType) private membershipTypeRepo: Repository<MembershipType>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.membershipTypeRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findAll() {
        try {
            return await this.membershipTypeRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.membershipTypeRepo.findOne(id);
            if (!data) throw new Error(`Membership type with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: MembershipTypeInput) {
        try {
            const data = new MembershipType();
            data.name = dto.name;
            data.code = await generateCode(MembershipType, 'MS', 10);
            data.periode = dto.periode;
            data.periodeUnit = dto.periodeUnit;
            data.price = dto.price;
            data.information = dto.information;
            await this.membershipTypeRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: MembershipTypeInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            // data.code = await generateCode(MembershipType, 'MS', 10);
            data.periode = dto.periode;
            data.periodeUnit = dto.periodeUnit;
            data.price = dto.price;
            data.information = dto.information;
            await this.membershipTypeRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.membershipTypeRepo.delete(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
