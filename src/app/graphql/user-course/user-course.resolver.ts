import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { UsersCourse } from '../../../entities/users-course.entity';
import { Users } from '../../../entities/users.entity';
import { Course } from '../../../entities/course.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { UsersCourseService } from './user-course.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { AuthUser } from '../../../decorators/auth.decorator';
import { Auth } from '../../../helpers/auth.helper';
import { UserCoursePagination, UserCourseFilter } from './user-course.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => UsersCourse)
export class UsersCourseResolver {

    constructor(
        @Inject(UsersCourseService) private userCourseService: UsersCourseService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UserCoursePagination)
    async userCourseList(
        @AuthUser() auth: Auth,
        @Args({ name: 'userCourseFilter', type: () => UserCourseFilter })
        userCourseFilter: UserCourseFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.userCourseService.find(auth.id, userCourseFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersCourse)
    async userCourse(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.userCourseService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersCourse)
    async userCourseLearn(
        @AuthUser() auth: Auth,
        @Args({ name: 'code', type: () => String }) code: string,
    ) {
        try {
            return await this.userCourseService.findByCode(auth.id,code);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => Users)
    async user(@Parent() usersCourse: UsersCourse) {
        const { id } = usersCourse;
        return this.userCourseService.user(id);
    }

    @ResolveField(returns => Course)
    async course(@Parent() usersCourse: UsersCourse) {
        const { id } = usersCourse;
        return this.userCourseService.course(id);
    }

    @ResolveField(returns => String)
    async status(@Parent() usersCourse: UsersCourse) {
        const { statusId } = usersCourse;
        return this.userCourseService.status(statusId);
    }

    @ResolveField(returns => Number)
    async progress(@Parent() usersCourse: UsersCourse) {
        const { id } = usersCourse;
        return this.userCourseService.progress(id);
    }

}
