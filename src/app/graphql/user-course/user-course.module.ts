import { Module } from '@nestjs/common';
import { UsersCourseResolver } from './user-course.resolver';
import { UsersCourseService } from './user-course.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersCourse } from '../../../entities/users-course.entity';
import { Users } from '../../../entities/users.entity';
import { Course } from '../../../entities/course.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [UsersCourseResolver, UsersCourseService],
    imports: [
        TypeOrmModule.forFeature([
            UsersCourse,
            Users,
            Course,
            MasterReference
        ]),
    ],
})
export class UserCourseModule { }
