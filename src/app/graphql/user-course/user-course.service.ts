import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserCourseFilter } from './user-course.dto';
import { UsersCourse } from '../../../entities/users-course.entity';
import { Users } from '../../../entities/users.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { MasterReference } from '../../../entities/master-reference.entity';

@Injectable()
export class UsersCourseService {

    constructor(
        @InjectRepository(Users) private usersRepo: Repository<Users>,
        @InjectRepository(UsersCourse) private usersCourseRepo: Repository<UsersCourse>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>
    ) { }

    async find(userId: number, filter: UserCourseFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.usersCourseRepo.createQueryBuilder('usercourse')
                .leftJoin(
                    'usercourse.user',
                    'users',
                )
                .leftJoin(
                    'usercourse.course',
                    'course',
                )
                .leftJoin(
                    'course.courseType',
                    'MasterType',
                )
                .leftJoin(
                    'course.courseCategory',
                    'CourseCategory',
                )
                .leftJoin(
                    'usercourse.usersCourseMaterial',
                    'UsersCourseMaterial',
                )
                .where(
                    `(LOWER(course.name) LIKE :search
                    OR LOWER(usercourse.courseCode) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                )
                .andWhere('users.id = :userId', { userId : userId});
                if(filter.typeId) list.andWhere('MasterType.id = :TypeId', { TypeId : filter.typeId })
                if(filter.categoryId) list.andWhere('CourseCategory.id = :CategoryId', { CategoryId : filter.categoryId })
                if(filter.status) {
                    if(filter.status == "onprogress"){
                        list.andWhere('usercourse.statusId = 2')
                    }
                    if(filter.status == "done"){
                        list.andWhere('usercourse.statusId = 3')
                    }
                }
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.usersCourseRepo.findOne(id);
            if (!data) throw new Error(`User Course with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findByCode(id :number, code: string) {
        try {
            const user = await this.usersRepo.findOne(id);
            const where = {
                user: user,
                courseCode: code
            }
            const data = await this.usersCourseRepo.findOne(where);
            if (!data) throw new Error(`User Course with where = ${code} is not found`);

            data.isVisited = true;
            data.statusId = 2;
            await this.usersCourseRepo.save(data);

            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async user(id: number) {
        try {
            const data = await this.usersCourseRepo.findOne(id, { relations: ['user'] });
            if (!data) throw new Error(`User Course with idx = ${id} is not found`);
            return data.user;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async course(id: number) {
        try {
            const data = await this.usersCourseRepo.findOne(id, { relations: ['course', 'usersCourseMaterial'] });
            if (!data) throw new Error(`User Course with idx = ${id} is not found`);
            return data.course;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async status(idx: number) {
        try {
            const where = {
                idx : idx,
                group : 'learn_status'
            };
            const data = await this.masterReferenceRepo.findOne(where);
            if (!data) throw new Error(`Learn Status with idx = ${idx} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async progress(id: number) {
        try {
            const data = await this.usersCourseRepo.findOne(id, { relations: ['course','course.courseModul', 'usersCourseMaterial'] });
            if (!data) throw new Error(`User Course with id = ${id} is not found`);

            var totalDone = 0
            data.usersCourseMaterial.forEach(item => {
                totalDone += item.courseMaterial.learningTime
            })
            var total = 0
            data.course.courseModul.forEach(item => {
                total += item.learningTime
            });

            return totalDone/total * 100;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
