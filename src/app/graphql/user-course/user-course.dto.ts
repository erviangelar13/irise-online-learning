import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { UsersCourse } from "../../../entities/users-course.entity";


@InputType()
export class UserCourseFilter {

    @Field({ nullable: true })
    typeId: number;

    @Field({ nullable: true })
    categoryId: number;

    @Field({ nullable: true })
    status: string;
}

@ObjectType()
export class UserCoursePagination extends PaginationPage {
    @Field(type => [UsersCourse])
    data: UsersCourse[];
}
