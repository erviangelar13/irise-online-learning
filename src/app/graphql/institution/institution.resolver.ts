import { Resolver, Query, Args, Mutation, ResolveField, Parent } from '@nestjs/graphql';
import { Institution } from '../../../entities/institution.entity';
import { Voucher } from '../../../entities/voucher.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { InstitutionService } from './institution.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { InstitutionPagination, InstitutionInput } from './institution.dto';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { OrderInstitution } from '../../../entities/order-institution.entity';

@Resolver(of => Institution)
export class InstitutionResolver {

    constructor(
        @Inject(InstitutionService) private institutionService: InstitutionService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => InstitutionPagination)
    async institutionList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.institutionService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    // @UseGuards(Oauth2Guard)
    @Query(returns => [Institution])
    async institutions() {
        try {
            return await this.institutionService.findList();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => Institution)
    async institution(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.institutionService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => [Voucher])
    async vouchers(@Parent() transactions: Institution) {
        const { id } = transactions;
        return this.institutionService.findVoucher(id);
    }

    @ResolveField(returns => [OrderInstitution])
    async orderInstitution(@Parent() transactions: Institution) {
        const { id } = transactions;
        return this.institutionService.findOrderInstitution(id);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createInstitution(
        @Args({ name: 'institutionInput', type: () => InstitutionInput }) institutionInput: InstitutionInput,
    ) {
        try {
            const data = await this.institutionService.create(institutionInput);
            return { id: data.id, message: 'Success create institution' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateInstitution(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'institutionInput', type: () => InstitutionInput }) institutionInput: InstitutionInput,
    ) {
        try {
            const data = await this.institutionService.update(id, institutionInput);
            return { id: data.id, message: 'Success update institution' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteInstitution(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.institutionService.delete(id);
            return { id: data.id, message: 'Success delete institution' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
