import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Institution } from '../../../entities/institution.entity';
import { InstitutionInput } from './institution.dto';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { generateCharacter } from '../../../helpers/generate.helper';
import { OrderInstitution } from '../../../entities/order-institution.entity';

@Injectable()
export class InstitutionService {

    constructor(
        @InjectRepository(Institution) private institutionRepo: Repository<Institution>,
        @InjectRepository(OrderInstitution) private orderInstitutionRepo: Repository<OrderInstitution>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.institutionRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList() {
        try {
            return await this.institutionRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.institutionRepo.findOne(id);
            if (!data) throw new Error(`Institution with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findVoucher(id: number) {
        try {
            const data = await this.institutionRepo.findOne(id,  { relations: ['orderInstitution','orderInstitution.orders'] });
            if (!data) throw new Error(`Institution with id = ${id} is not found`);
            if(data.orderInstitution) {
                // console.log(data.orderInstitution)
            }
            return [];
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderInstitution(id: number) {
        try {
            const data = await this.institutionRepo.findOne(id,  { relations: ['orderInstitution','orderInstitution.orders'] });
            if (!data) throw new Error(`Institution with id = ${id} is not found`);
            return data.orderInstitution;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: InstitutionInput) {
        try {
            const data = new Institution();
            data.name = dto.name;
            data.address = dto.address;
            data.phone = dto.phone;
            data.pic = dto.pic;
            data.picPhone = dto.picPhone;
            data.invitationCode = dto.code? dto.code : generateCharacter(10);
            await this.institutionRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: InstitutionInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.address = dto.address;
            data.phone = dto.phone;
            data.pic = dto.pic;
            data.picPhone = dto.picPhone;
            await this.institutionRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            if (!data) throw new Error(`Master category with id = ${id} is not found`);
            await this.institutionRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
