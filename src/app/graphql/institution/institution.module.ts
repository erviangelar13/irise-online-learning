import { Module } from '@nestjs/common';
import { InstitutionResolver } from './institution.resolver';
import { InstitutionService } from './institution.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Institution } from '../../../entities/institution.entity';
import { Voucher } from '../../../entities/voucher.entity';
import { OrderInstitution } from '../../../entities/order-institution.entity';

@Module({
    providers: [InstitutionResolver, InstitutionService],
    imports: [
        TypeOrmModule.forFeature([Institution, Voucher, OrderInstitution, OrderInstitution]),
    ],
})
export class InstitutionModule { }
