import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Institution } from "../../../entities/institution.entity";


@InputType()
export class InstitutionInput {

    @Field()
    name: string;

    @Field({ nullable: true })
    address: string;

    @Field()
    phone: string;

    @Field()
    pic: string;

    @Field()
    picPhone: string;

    @Field({ nullable: true })
    code: string;
}

@ObjectType()
export class InstitutionPagination extends PaginationPage {
    @Field(type => [Institution])
    data: Institution[];
}
