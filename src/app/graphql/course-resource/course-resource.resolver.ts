import { Resolver, ResolveField, Parent, Mutation, Args } from '@nestjs/graphql';
import { CourseResource } from '../../../entities/course-resource.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { CourseResourceService } from './course-resource.service';
import { Config } from '../../../helpers/config.helper';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { Oauth2Guard } from '../../../guards/guards';
import { removeFile } from '../../../helpers/file.helper';
import { CourseResourceInput } from './course-resource.dto';

@Resolver(of => CourseResource)
export class CourseResourceResolver {

    constructor(
        @Inject(CourseResourceService) private courseResourceService: CourseResourceService,
    ) { }

    @ResolveField(returns => String)
    async fileURL(@Parent() courseResource: CourseResource) {
        const { id, fileName } = courseResource;
        return fileName ? `${Config.get('BASE_URL')}view/course-resource/file/${id}` : null;
    }

    @ResolveField(returns => String)
    async fileType(@Parent() courseResource: CourseResource) {
        const { fileTypeId } = courseResource;
        const data = await this.courseResourceService.fileType(fileTypeId);
        return data.name;
    }

    @ResolveField(returns => String)
    async resourceType(@Parent() courseResource: CourseResource) {
        const { resourceTypeId } = courseResource;
        const data = await this.courseResourceService.resourceType(resourceTypeId);
        return data.name;
    }

    @ResolveField(returns => Number)
    async resourceTypeId(@Parent() courseResource: CourseResource) {
        const { resourceTypeId } = courseResource;
        const data = await this.courseResourceService.resourceType(resourceTypeId);
        return data.idx;
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseResource(
        @Args({ name: 'courseResourceInput', type: () => CourseResourceInput }) courseResourceInput: CourseResourceInput,
    ) {
        try {
            const data = await this.courseResourceService.createByUrlAttachment(courseResourceInput);
            return { id: data.id, message: 'Success create course resource' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourseResource(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseResourceInput', type: () => CourseResourceInput }) courseResourceInput: CourseResourceInput,
    ) {
        try {
            const data = await this.courseResourceService.updateByUrlAttachment(id, courseResourceInput);
            return { id: data.id, message: 'Success create course resource' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteCourseResource(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseResourceService.delete(id);
            if (data.fileName) await removeFile(data.filePath, data.fileName);
            return { id: data.id, message: 'Success delete course resource' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
