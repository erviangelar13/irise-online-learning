import { Module } from '@nestjs/common';
import { CourseResourceResolver } from './course-resource.resolver';
import { CourseResourceService } from './course-resource.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseResource } from '../../../entities/course-resource.entity';
import { CourseMaterial } from '../../../entities/course-material.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [CourseResourceResolver, CourseResourceService],
    imports: [
        TypeOrmModule.forFeature([CourseResource, CourseMaterial, MasterReference]),
    ],
})
export class CourseResourceModule { }
