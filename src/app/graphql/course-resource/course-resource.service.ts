import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CourseResource } from '../../../entities/course-resource.entity';
import { CourseMaterial } from '../../../entities/course-material.entity';
import { MasterReference } from '../../../entities/master-reference.entity';
import { join } from 'path';
import * as fs from 'fs';
import { getMime } from '../../../helpers/mime.helper';
import { Config } from '../../../helpers/config.helper';
import { CourseResourceInput } from './course-resource.dto';
import { IUploadOptions, removeFile } from '../../../helpers/file.helper';

@Injectable()
export class CourseResourceService {

    constructor(
        @InjectRepository(CourseResource) private courseResourceRepo: Repository<CourseResource>,
        @InjectRepository(CourseMaterial) private courseMaterialRepo: Repository<CourseMaterial>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
    ) { }

    async findById(id: number) {
        try {
            const data = await this.courseResourceRepo.findOne(id);
            if (!data) throw new Error(`Course resource with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async fileType(fileTypeId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'file_type', idx: fileTypeId });
            if (!data) throw new Error(`File type with idx = ${fileTypeId} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async resourceType(resourceTypeId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'resource_type', idx: resourceTypeId });
            if (!data) throw new Error(`Resource type with idx = ${resourceTypeId} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async viewFile(id: number) {
        try {
            const data = await this.findById(id);
            const path = join(`${Config.get('BASE_PATH_FILE')}course_resource/`, data.fileName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createByLocalStorage(dto: CourseResourceInput, attachment: IUploadOptions) {
        try {
            const courseMaterialData = await this.courseMaterialRepo.findOne(dto.courseMaterialId, { relations: ['courseResource'] });
            if (!courseMaterialData) throw new Error(`Course material with id = ${dto.courseMaterialId} is not found`);
            
            if(courseMaterialData.courseResource.length > 0) {
                var resource = courseMaterialData.courseResource[0];
                await this.courseResourceRepo.delete(resource.id);
                if (resource.fileName) await removeFile(resource.filePath, resource.fileName);
            }

            const data = new CourseResource();
            data.fileName = attachment.filename;
            data.fileOriginalName = attachment.originalname;
            data.filePath = `${Config.get('BASE_PATH_FILE')}course_resource/`;
            data.fileExtension = attachment.filename.substring(attachment.filename.lastIndexOf('.'), attachment.filename.length);
            data.fileTypeId = dto.fileTypeId;
            data.resourceTypeId = dto.resourceTypeId;
            data.courseMaterial = courseMaterialData;

            await this.courseResourceRepo.save(data);
            data.fileURL = `${Config.get('BASE_URL')}view/course-resource/file/${data.id}`;
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createByUrlAttachment(dto: CourseResourceInput) {
        try {
            const courseMaterialData = await this.courseMaterialRepo.findOne(dto.courseMaterialId, { relations: ['courseResource'] });
            if (!courseMaterialData) throw new Error(`Course material with id = ${dto.courseMaterialId} is not found`);
            
            if(courseMaterialData.courseResource.length > 0) {
                var resource = courseMaterialData.courseResource[0];
                await this.courseResourceRepo.delete(resource.id);
                if (resource.fileName) await removeFile(resource.filePath, resource.fileName);
            }

            const data = new CourseResource();
            data.fileName = dto.fileName;
            data.fileTypeId = dto.fileTypeId;
            data.resourceTypeId = dto.resourceTypeId;
            data.courseMaterial = courseMaterialData;
            data.urlAttachment = dto.urlAttachment;

            await this.courseResourceRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async updateByUrlAttachment(id: number, dto: CourseResourceInput) {
        try {
            const courseResourceData = await this.findById(id);

            const courseMaterialData = await this.courseMaterialRepo.findOne(dto.courseMaterialId, { relations: ['courseResource'] });
            if (!courseMaterialData) throw new Error(`Course material with id = ${dto.courseMaterialId} is not found`);

            if (courseResourceData.fileName) await removeFile(courseResourceData.filePath, courseResourceData.fileName);

            courseResourceData.fileName = dto.fileName;
            courseResourceData.fileTypeId = dto.fileTypeId;
            courseResourceData.resourceTypeId = dto.resourceTypeId;
            courseResourceData.courseMaterial = courseMaterialData;
            courseResourceData.urlAttachment = dto.urlAttachment;

            await this.courseResourceRepo.save(courseResourceData);
            return courseResourceData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            if (!data) throw new Error(`Course resource with id = ${id} is not found`);
            await this.courseResourceRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
