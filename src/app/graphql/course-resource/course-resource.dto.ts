import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class CourseResourceInput {

    @Field()
    courseMaterialId: number;

    @Field({ nullable: true })
    fileTypeId: number;

    @Field()
    resourceTypeId: number;

    @Field({ nullable: true })
    fileName: string;

    @Field({ nullable: true })
    urlAttachment: string;
}
