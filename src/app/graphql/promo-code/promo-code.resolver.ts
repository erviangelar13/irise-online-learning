import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { PromoCode } from '../../../entities/promo-code.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { PromoCodeService } from './promo-code.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { PromoCodePagination, PromoCodeInput, PromoFilter } from './promo-code.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => PromoCode)
export class PromoCodeResolver {

    constructor(
        @Inject(PromoCodeService) private promoCodeService: PromoCodeService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => PromoCodePagination)
    async promoCodeList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.promoCodeService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Query(returns => [PromoCode])
    async promoCodes(
        @Args({ name: 'searchFilter', type: () => PromoFilter, nullable: true })
        searchFilter: PromoFilter
    ) {
        try {
            return await this.promoCodeService.findList(searchFilter);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => PromoCode)
    async promoCode(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.promoCodeService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async promoType(@Parent() promoCode: PromoCode) {
        const { promoTypeId } = promoCode;
        return this.promoCodeService.promoType(promoTypeId);
    }

    @ResolveField(returns => String)
    async promoCategory(@Parent() promoCode: PromoCode) {
        const { promoCategoryId } = promoCode;
        return this.promoCodeService.promoCategory(promoCategoryId);
    }

    @ResolveField(returns => String)
    async valueType(@Parent() promoCode: PromoCode) {
        const { valueTypeId } = promoCode;
        return this.promoCodeService.valueType(valueTypeId);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createPromoCode(
        @Args({ name: 'promoCodeInput', type: () => PromoCodeInput }) promoCodeInput: PromoCodeInput,
    ) {
        try {
            const data = await this.promoCodeService.create(promoCodeInput);
            return { id: data.id, message: 'Success create promo code' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updatePromoCode(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'promoCodeInput', type: () => PromoCodeInput }) promoCodeInput: PromoCodeInput,
    ) {
        try {
            const data = await this.promoCodeService.update(id, promoCodeInput);
            return { id: data.id, message: 'Success update promo code' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deletePromoCode(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.promoCodeService.delete(id);
            return { id: data.id, message: 'Success delete promo code' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
