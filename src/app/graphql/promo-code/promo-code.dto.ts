import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { PromoCode } from "../../../entities/promo-code.entity";


@InputType()
export class PromoCodeInput {

    @Field()
    code: string;

    @Field()
    name: string;

    @Field({ nullable: true })
    dateStart: string;

    @Field({ nullable: true })
    dateEnd: string;

    @Field({ nullable: true })
    promoTypeId: number;

    @Field({ nullable: true })
    promoCategoryId: number;

    @Field()
    valueTypeId: number;

    @Field()
    value: number;
}

@InputType()
export class PromoFilter {

    @Field({ nullable: true })
    keyword: string;
}

@ObjectType()
export class PromoCodePagination extends PaginationPage {
    @Field(type => [PromoCode])
    data: PromoCode[];
}
