import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PromoCode } from '../../../entities/promo-code.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { PromoCodeInput, PromoFilter } from './promo-code.dto';
import { generateCharacter } from '../../../helpers/generate.helper';
import { MasterReference } from '../../../entities/master-reference.entity';

@Injectable()
export class PromoCodeService {

    constructor(
        @InjectRepository(PromoCode) private promoCodeRepo: Repository<PromoCode>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.promoCodeRepo.createQueryBuilder()
                .where(
                    `(LOWER(name) LIKE :search
                    OR LOWER(code) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList(filter: PromoFilter) {
        try {
            const list = this.promoCodeRepo.createQueryBuilder('promo')
                .where(
                    `(LOWER(promo.name) LIKE :search
                    OR LOWER(promo.code) LIKE :search)`,
                    { search: '%' + filter.keyword + '%' },
                );
            return await list.getMany();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.promoCodeRepo.findOne(id);
            if (!data) throw new Error(`Promo code with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async promoType(promoTypeId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'promo_type', idx: promoTypeId });
            if (!data) throw new Error(`Promo type with idx = ${promoTypeId} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async promoCategory(promoCategoryId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'promo_category', idx: promoCategoryId });
            if (!data) throw new Error(`Promo category with idx = ${promoCategoryId} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async valueType(valueTypeId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'promo_value_type', idx: valueTypeId });
            if (!data) throw new Error(`Value type with idx = ${valueTypeId} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: PromoCodeInput) {
        try {
            const data = new PromoCode();
            data.code = dto.code != "" ? dto.code : generateCharacter(15);
            data.name = dto.name;
            data.dateStart = dto.dateStart ? new Date(dto.dateStart) : null;
            data.dateEnd = dto.dateEnd ? new Date(dto.dateEnd) : null;
            data.promoTypeId = dto.promoTypeId;
            data.promoCategoryId = dto.promoCategoryId;
            data.valueTypeId = dto.valueTypeId;
            data.value = dto.value;
            await this.promoCodeRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: PromoCodeInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.dateStart = dto.dateStart ? new Date(dto.dateStart) : null;
            data.dateEnd = dto.dateEnd ? new Date(dto.dateEnd) : null;
            data.promoTypeId = dto.promoTypeId;
            data.promoCategoryId = dto.promoCategoryId;
            data.valueTypeId = dto.valueTypeId;
            data.value = dto.value;
            await this.promoCodeRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.promoCodeRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
