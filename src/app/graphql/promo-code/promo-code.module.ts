import { Module } from '@nestjs/common';
import { PromoCodeResolver } from './promo-code.resolver';
import { PromoCodeService } from './promo-code.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PromoCode } from '../../../entities/promo-code.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [PromoCodeResolver, PromoCodeService],
    imports: [
        TypeOrmModule.forFeature([
            PromoCode,
            MasterReference,
        ]),
    ],
})
export class PromoCodeModule { }
