import { Module } from '@nestjs/common';
import { UserFcmResolver } from './user-fcm.resolver';
import { UserFcmService } from './user-fcm.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserFcm } from '../../../entities/user-fcm.entity';

@Module({
    providers: [UserFcmResolver, UserFcmService],
    imports: [TypeOrmModule.forFeature([UserFcm])],
})
export class UserFcmModule { }
