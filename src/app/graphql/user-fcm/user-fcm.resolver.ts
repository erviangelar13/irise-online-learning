import { Resolver, ResolveField, Parent } from '@nestjs/graphql';
import { UserFcm } from '../../../entities/user-fcm.entity';
import { Inject } from '@nestjs/common';
import { UserFcmService } from './user-fcm.service';
import { Users } from '../../../entities/users.entity';

@Resolver(of => UserFcm)
export class UserFcmResolver {

    constructor(
        @Inject(UserFcmService) private userFcmService: UserFcmService,
    ) { }

    @ResolveField(returns => Users)
    async user(@Parent() userFcm: UserFcm) {
        const { id } = userFcm;
        return this.userFcmService.findUserById(id);
    }
}
