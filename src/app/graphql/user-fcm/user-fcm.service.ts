import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserFcm } from '../../../entities/user-fcm.entity';

@Injectable()
export class UserFcmService {

    constructor(
        @InjectRepository(UserFcm) private userFcmRepo: Repository<UserFcm>,
    ) { }

    async findUserById(id: number) {
        try {
            const data = await this.userFcmRepo.findOne(id, { relations: ['user'] });
            return data.user;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
