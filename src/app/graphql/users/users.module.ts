import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from '../../../entities/users.entity';
import { InstructorInformation } from '../../../entities/instructor-information.entity';

@Module({
    providers: [
        UsersService,
        UsersResolver,
    ],
    imports: [
        TypeOrmModule.forFeature([Users, InstructorInformation]),
    ],
})
export class UsersModule { }
