import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from '../../../entities/users.entity';
import { Repository, getConnection } from 'typeorm';
import { UsersFilter, UsersInput, UsersOperatorInput, UsersDashboardInfo, PaswordChangeInput } from './users.dto';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { generateCode } from '../../../helpers/generate.helper';
import { Roles } from '../../../entities/roles.entity';
import { UserHasRoles } from '../../../entities/user-has-roles.entity';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { MailerService } from '@nestjs-modules/mailer';
import { MailService } from '../../mail/mail.service';
import { generateCharacter } from '../../../helpers/generate.helper';
import { UsersInstitution } from 'src/entities/users-institution.entity';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(Users) private usersRepo: Repository<Users>,
        @InjectRepository(InstructorInformation) private instructorInformationRepo: Repository<InstructorInformation>,
        private readonly mailerService: MailerService,
    ) { }

    async find(filter: UsersFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.usersRepo.createQueryBuilder()
                .where(
                    `(LOWER(code) LIKE :search
                    OR LOWER(email) LIKE :search
                    OR LOWER(name) LIKE :search
                    OR LOWER(phone) LIKE :search
                    OR LOWER(username) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            if (filter.isActive) {
                const isActive = filter.isActive === 1 ? true : false;
                list.andWhere('is_active = :isActive', { isActive });
            }
            if (filter.isEmailVerified) {
                const isEmailVerified = filter.isEmailVerified === 1 ? true : false;
                list.andWhere('is_email_verified = :isEmailVerified', { isEmailVerified });
            }
            if (filter.isPhoneVerified) {
                const isPhoneVerified = filter.isPhoneVerified === 1 ? true : false;
                list.andWhere('is_phone_verified = :isPhoneVerified', { isPhoneVerified });
            }
            if (filter.instructorId) list.andWhere('instructor_id = :instructorId', { instructorId: filter.instructorId });
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.usersRepo.findOne(id);
            if (!data) throw new Error(`Users with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findInstructorById(id: number) {
        try {
            const data = await this.usersRepo.findOne(id, { relations: ['instructor'] });
            return data.instructor;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findUserHasRolesById(id: number) {
        try {
            const data = await this.usersRepo.findOne(id, { relations: ['userHasRoles'] });
            return data.userHasRoles;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findProfileById(id: number) {
        try {
            const data = await this.usersRepo.findOne(id, { relations: ['profile'] });
            return data.profile;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findBillingById(id: number) {
        try {
            const data = await this.usersRepo.findOne(id, { relations: ['billing'] });
            return data.billing;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findUserFcmById(id: number) {
        try {
            const data = await this.usersRepo.findOne(id, { relations: ['userFcm'] });
            return data.userFcm;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createUserOperator(dto: UsersOperatorInput) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            const usersRepo = queryRunner.manager.getRepository(Users);

            // Start ==> Check username, email, and phone exist
            const userIsExist = await usersRepo.findOne({ username: dto.username });
            if (userIsExist) throw new Error('Username already exist');

            const emailIsExist = await usersRepo.findOne({ email: dto.email });
            if (emailIsExist) throw new Error('Email already exist');

            const phoneIsExist = await usersRepo.findOne({ phone: dto.phone });
            if (phoneIsExist) throw new Error('Phone number already exist');
            // End ==> Check username, email, and phone exist

            const userData = new Users();
            userData.code = await generateCode(Users, 'USR', 10);
            userData.isActive = true;
            userData.email = dto.email;
            userData.name = dto.name;
            userData.phone = dto.phone;
            userData.username = dto.username;

            if(dto.passwordType == 1) {
                userData.password = generateCharacter(10)
            }else{
                userData.password = dto.password;
            }
            await queryRunner.manager.save(userData);

            const roles: Roles[] = await queryRunner.manager.getRepository(Roles).find();
            const rolesData: UserHasRoles[] = [];

            const filterRoleUser = roles.filter(fItem => {
                return fItem.id == dto.role
                // return fItem.name === 'operator';
            });
            const roleUser = new UserHasRoles();
            roleUser.user = userData;
            roleUser.role = filterRoleUser.length ? filterRoleUser[0] : null;
            rolesData.push(roleUser);
            await queryRunner.manager.save(rolesData);

            await queryRunner.commitTransaction();
            // console.log(filterRoleUser)

            if(filterRoleUser[0].name == 'institution') {
                const mailService = new MailService(this.mailerService);
                await mailService.sendEmail(
                    dto.email,
                    'User Account Information',
                    'user-register',
                    {
                        name: userData.name,
                        email: userData.email,
                        password: userData.password
                    },
                );
            }

            return userData;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async updateUserOperator(id: number, dto: UsersOperatorInput) {
        try {
            const userData = await this.findById(id);
            userData.name = dto.name;
            if (dto.email) {
                if (userData.isEmailVerified) {
                    const checkEmail = await this.usersRepo.findOne({ email: dto.email });
                    if (checkEmail) throw new Error('Email already exist');
                    userData.email = dto.email;
                    userData.name = dto.name;
                    userData.phone = dto.phone;
                    userData.isEmailVerified = false;
                } else {
                    throw new Error('Please verify your email before change email');
                }
            }

            if (dto.phone) {
                if (userData.isPhoneVerified) {
                    const checkPhone = await this.usersRepo.findOne({ phone: dto.phone });
                    if (checkPhone) throw new Error('Phone number already exist');
                    userData.phone = dto.phone;
                    userData.isPhoneVerified = false;
                } else {
                    throw new Error('Please verify your phone number before change phone number');
                }
            }

            await this.usersRepo.save(userData);
            return userData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async updateUser(id: number, dto: UsersInput) {
        try {
            const userData = await this.findById(id);
            userData.name = dto.name;
            if (dto.email) {
                if (userData.isEmailVerified) {
                    const checkEmail = await this.usersRepo.findOne({ email: dto.email });
                    if (checkEmail) throw new Error('Email already exist');
                    userData.email = dto.email;
                    userData.isEmailVerified = false;
                } else {
                    throw new Error('Please verify your email before change email');
                }
            }

            if (dto.phone) {
                if (userData.isPhoneVerified) {
                    const checkPhone = await this.usersRepo.findOne({ phone: dto.phone });
                    if (checkPhone) throw new Error('Phone number already exist');
                    userData.phone = dto.phone;
                    userData.isPhoneVerified = false;
                } else {
                    throw new Error('Please verify your phone number before change phone number');
                }
            }

            await this.usersRepo.save(userData);
            return userData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async changePassword(id: number, dto: PaswordChangeInput) {
        try {
            const userData = await this.findById(id);
            // console.log(dto)
            if(userData.password != dto.oldPassword) throw new Error("Password is Wrong");
            if(dto.newPassword != dto.retypePassword) throw new Error("New Passowrd and Retype Password is difference");
            userData.password = dto.newPassword;

            await this.usersRepo.save(userData);
            return userData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async updateActiveUser(id: number, isActive: boolean) {
        try {
            const data = await this.findById(id);
            data.isActive = isActive;
            await this.usersRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async setUserInstructor(userId: number, instructorId: number) {
        try {
            const userData = await this.findById(userId);
            const instructorData = await this.instructorInformationRepo.findOne(instructorId);
            if (!instructorData) throw new Error(`Instructor with id = ${instructorId} is not found`);
            userData.instructor = instructorData;
            await this.usersRepo.save(userData);
            return userData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async getUsersDashboardInfo() {
        try {
            const data: UsersDashboardInfo = new UsersDashboardInfo();
            data.user = {
                total: 124500,
                increment: 1.25
            };
            data.userVerified = {
                total: 123120,
                increment: 1.52,
            };
            data.member = {
                total: 15100,
                increment: 2.2,
            };
            data.merchant = {
                total: 5,
                increment: 0,
            };
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
