import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { Inject, UseGuards } from '@nestjs/common';
import { Users } from '../../../entities/users.entity';
import { UsersService } from './users.service';
import { Oauth2Guard } from '../../../guards/guards';
import { AuthUser } from '../../../decorators/auth.decorator';
import { Auth } from '../../../helpers/auth.helper';
import { InstructorInformation } from '../../../entities/instructor-information.entity';
import { UserHasRoles } from '../../../entities/user-has-roles.entity';
import { UserProfile } from '../../../entities/user-profile.entity';
import { UserBillingInformation } from '../../../entities/user-billing-information.entity';
import { UserFcm } from '../../../entities/user-fcm.entity';
import { UsersPagination, UsersFilter, UsersInput, UsersOperatorInput, UsersDashboardInfo, PaswordChangeInput } from './users.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => Users)
export class UsersResolver {

    constructor(
        @Inject(UsersService) private usersService: UsersService,
    ) { }

    @Scopes('read')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersPagination)
    async userList(
        @Args({ name: 'usersFilter', type: () => UsersFilter, nullable: true })
        usersFilter: UsersFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.usersService.find(usersFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => Users)
    async getMyUser(
        @AuthUser() auth: Auth,
    ) {
        try {
            return await this.usersService.findById(auth.id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Query(returns => Users)
    async getUser(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.usersService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersDashboardInfo)
    async getUsersDashboardInfo() {
        try {
            return await this.usersService.getUsersDashboardInfo();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => InstructorInformation)
    async instructor(@Parent() users: Users) {
        const { id } = users;
        return this.usersService.findInstructorById(id);
    }

    @ResolveField(returns => [UserHasRoles])
    async userHasRoles(@Parent() users: Users) {
        const { id } = users;
        return this.usersService.findUserHasRolesById(id);
    }

    @ResolveField(returns => UserProfile)
    async profile(@Parent() users: Users) {
        const { id } = users;
        return this.usersService.findProfileById(id);
    }

    @ResolveField(returns => UserBillingInformation)
    async billing(@Parent() users: Users) {
        const { id } = users;
        return this.usersService.findBillingById(id);
    }

    @ResolveField(returns => [UserFcm])
    async userFcm(@Parent() users: Users) {
        const { id } = users;
        return this.usersService.findUserFcmById(id);
    }

    @Scopes('create')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createUserOperator(
        @Args({ name: 'usersOperatorInput', type: () => UsersOperatorInput }) usersOperatorInput: UsersOperatorInput,
    ) {
        try {
            const data = await this.usersService.createUserOperator(usersOperatorInput);
            return { id: data.id, message: 'Success create user operator' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateUserOperator(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'usersOperatorUpdate', type: () => UsersOperatorInput }) usersOperatorUpdate: UsersOperatorInput,
    ) {
        try {
            const data = await this.usersService.updateUserOperator(id, usersOperatorUpdate);
            return { id: data.id, message: 'Success update user' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateUser(
        @AuthUser() auth: Auth,
        @Args({ name: 'usersInput', type: () => UsersInput }) usersInput: UsersInput,
    ) {
        try {
            const data = await this.usersService.updateUser(auth.id, usersInput);
            return { id: data.id, message: 'Success update user' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async changePassword(
        @AuthUser() auth: Auth,
        @Args({ name: 'paswordChangeInput', type: () => PaswordChangeInput }) paswordChangeInput: PaswordChangeInput,
    ) {
        try {
            const data = await this.usersService.changePassword(auth.id, paswordChangeInput);
            return { id: data.id, message: 'Success update password' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async activatedUser(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.usersService.updateActiveUser(id, true);
            return { id: data.id, message: 'Success activated user' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async suspendUser(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.usersService.updateActiveUser(id, false);
            return { id: data.id, message: 'Success suspend user' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async setUserInstructor(
        @Args({ name: 'userId', type: () => Number }) userId: number,
        @Args({ name: 'instructorId', type: () => Number }) instructorId: number,
    ) {
        try {
            const data = await this.usersService.setUserInstructor(userId, instructorId);
            return { id: data.id, message: 'Success set user instructor' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
