import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Users } from "../../../entities/users.entity";

@InputType()
export class UsersFilter {

    @Field()
    isActive: number;

    @Field()
    isEmailVerified: number;

    @Field()
    isPhoneVerified: number;

    @Field()
    instructorId: number;
}

@InputType()
export class UsersInput {

    @Field()
    name: string;

    @Field({ nullable: true })
    email: string;

    @Field({ nullable: true })
    phone: string;
}

@InputType()
export class PaswordChangeInput {

    @Field()
    oldPassword: string;

    @Field({ nullable: true })
    newPassword: string;

    @Field({ nullable: true })
    retypePassword: string;
}

@InputType()
export class UsersOperatorInput {

    @Field()
    name: string;

    @Field({ nullable: true })
    email: string;

    @Field({ nullable: true })
    phone: string;

    @Field()
    username: string;

    @Field()
    passwordType: number;

    @Field()
    password: string;

    @Field()
    role: number;
    
}

@ObjectType()
export class UsersPagination extends PaginationPage {
    @Field(type => [Users])
    data: Users[];
}

@ObjectType()
export class UsersDashboardInfoDetail {

    @Field()
    total: number;

    @Field()
    increment: number;
}

@ObjectType()
export class UsersDashboardInfo {

    @Field(type => UsersDashboardInfoDetail)
    user: UsersDashboardInfoDetail;

    @Field(type => UsersDashboardInfoDetail)
    userVerified: UsersDashboardInfoDetail;

    @Field(type => UsersDashboardInfoDetail)
    member: UsersDashboardInfoDetail;

    @Field(type => UsersDashboardInfoDetail)
    merchant: UsersDashboardInfoDetail;
}
