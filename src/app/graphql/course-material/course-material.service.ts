import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CourseMaterial } from '../../../entities/course-material.entity';
import { CourseModul } from '../../../entities/course-modul.entity';
import { CourseMaterialInput } from './course-material.dto';
import { MasterReference } from '../../../entities/master-reference.entity';

@Injectable()
export class CourseMaterialService {

    constructor(
        @InjectRepository(CourseMaterial) private courseMaterialRepo: Repository<CourseMaterial>,
        @InjectRepository(CourseModul) private courseModulRepo: Repository<CourseModul>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
    ) { }

    async findByCourseModulId(id: number) {
        try {
            const data = await this.courseMaterialRepo.createQueryBuilder()
                .where('course_modul_id = :courseModulId', { courseModulId: id })
                .getMany();

            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const courseMaterialData = await this.courseMaterialRepo.findOne(id);
            if (!courseMaterialData) throw new Error(`Course material with id = ${id} is not found`);

            return courseMaterialData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findCourseResourceById(id: number) {
        const data = await this.courseMaterialRepo.findOne(id, { relations: ['courseResource'] });
        return data.courseResource;
    }

    async materialType(materialTypeId: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'material_type', idx: materialTypeId });
            if (!data) throw new Error(`Material type with idx = ${materialTypeId} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async userCourseMaterial(id: number) {
        const data = await this.courseMaterialRepo.findOne(id, { relations: ['usersCourseMaterial'] });
        return data.usersCourseMaterial;
    }

    async create(dto: CourseMaterialInput) {
        try {
            const courseMaterialValidationData = await this.courseMaterialValidation(dto);
            const courseModul = courseMaterialValidationData.courseModul;

            const courseMaterialData = new CourseMaterial();
            courseMaterialData.courseModul = courseModul;
            courseMaterialData.name = dto.name;
            courseMaterialData.learningTime = dto.learningTime;
            courseMaterialData.materialTypeId = dto.materialTypeId;
            // courseMaterialData.exercise = dto.exercise? dto.exercise : false;
            courseMaterialData.information = dto.information;

            await this.courseMaterialRepo.save(courseMaterialData);

            return courseMaterialData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createMany(dto: CourseMaterialInput[]) {
        try {
            const courseMaterialsData : CourseMaterial[] = [];
            dto.forEach(async (item) => {
                
                const courseMaterialValidationData = await this.courseMaterialValidation(item);
                const courseModul = courseMaterialValidationData.courseModul;

                const courseMaterialData = new CourseMaterial();
                courseMaterialData.courseModul = courseModul;
                courseMaterialData.name = item.name;
                courseMaterialData.learningTime = item.learningTime;
                courseMaterialData.materialTypeId = item.materialTypeId;
                // courseMaterialData.exercise = item.exercise? item.exercise : false;
                courseMaterialData.information = item.information;

                courseMaterialsData.push(courseMaterialData);
            });

            await this.courseMaterialRepo.save(courseMaterialsData);

            return courseMaterialsData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: CourseMaterialInput) {
        try {
            const courseMaterialData = await this.findById(id);

            const courseMaterialValidationData = await this.courseMaterialValidation(dto);
            const courseModul = courseMaterialValidationData.courseModul;

            courseMaterialData.courseModul = courseModul;
            courseMaterialData.name = dto.name;
            courseMaterialData.learningTime = dto.learningTime;
            courseMaterialData.materialTypeId = dto.materialTypeId;
            courseMaterialData.information = dto.information;

            await this.courseMaterialRepo.save(courseMaterialData);

            return courseMaterialData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const courseMaterialData = await this.findById(id);
            await this.courseMaterialRepo.delete(courseMaterialData);

            return courseMaterialData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    private async courseMaterialValidation(dto: CourseMaterialInput) {
        try {
            const courseModul = await this.courseModulRepo.findOne(dto.courseModulId);
            if (!courseModul) throw new Error(`Course modul with id = ${dto.courseModulId} is not found`);

            const materialTypeData = await this.masterReferenceRepo.findOne({ group: 'material_type', idx: dto.materialTypeId });
            if (!materialTypeData) throw new Error(`Material type with id = ${dto.materialTypeId} is not found`);

            return {
                courseModul,
                materialTypeData,
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
