import { Module } from '@nestjs/common';
import { CourseMaterialResolver } from './course-material.resolver';
import { CourseMaterialService } from './course-material.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseMaterial } from '../../../entities/course-material.entity';
import { CourseModul } from '../../../entities/course-modul.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [CourseMaterialResolver, CourseMaterialService],
    imports: [
        TypeOrmModule.forFeature([
            CourseMaterial,
            CourseModul,
            MasterReference,
        ]),
    ],
})
export class CourseMaterialModule { }
