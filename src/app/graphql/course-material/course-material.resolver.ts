import { Resolver, Query, Args, Mutation, ResolveField, Parent } from '@nestjs/graphql';
import { CourseMaterial } from '../../../entities/course-material.entity';
import { CourseResource } from '../../../entities/course-resource.entity';
import { UsersCourseMaterial } from '../../../entities/users-course-material.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { CourseMaterialService } from './course-material.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { CourseMaterialInput } from './course-material.dto';

@Resolver(of => CourseMaterial)
export class CourseMaterialResolver {

    constructor(
        @Inject(CourseMaterialService) private courseMaterialService: CourseMaterialService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [CourseMaterial])
    async courseMaterialList(
        @Args({ name: 'courseModulId', type: () => Number }) courseModulId: number,
    ) {
        try {
            return await this.courseMaterialService.findByCourseModulId(courseModulId);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseMaterial)
    async courseMaterial(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.courseMaterialService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => [CourseResource])
    async courseResource(@Parent() courseMaterial: CourseMaterial) {
        const { id } = courseMaterial;
        return this.courseMaterialService.findCourseResourceById(id);
    }

    @ResolveField(returns => String)
    async materialType(@Parent() courseMaterial: CourseMaterial) {
        const { materialTypeId } = courseMaterial;
        return this.courseMaterialService.materialType(materialTypeId);
    }

    @ResolveField(returns => [UsersCourseMaterial])
    async usersCourseMaterial(@Parent() courseMaterial: CourseMaterial) {
        const { id } = courseMaterial;
        return this.courseMaterialService.userCourseMaterial(id);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseMaterial(
        @Args({ name: 'courseMaterialInput', type: () => CourseMaterialInput }) courseMaterialInput: CourseMaterialInput,
    ) {
        try {
            const data = await this.courseMaterialService.create(courseMaterialInput);
            return { id: data.id, message: 'Success create course material' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseMaterials(
        @Args({ name: 'courseMaterialInput', type: () => CourseMaterialInput }) courseMaterialInput: CourseMaterialInput[],
    ) {
        try {
            const data = await this.courseMaterialService.createMany(courseMaterialInput);
            return { id: data, message: 'Success create course material' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourseMaterial(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseMaterialInput', type: () => CourseMaterialInput }) courseMaterialInput: CourseMaterialInput,
    ) {
        try {
            const data = await this.courseMaterialService.update(id, courseMaterialInput);
            return { id: data.id, message: 'Success update course material' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteCourseMaterial(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseMaterialService.delete(id);
            return { id: data.id, message: 'Success delete course material' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
