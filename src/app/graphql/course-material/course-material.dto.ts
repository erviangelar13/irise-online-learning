import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { CourseMaterial } from "../../../entities/course-material.entity";


@InputType()
export class CourseMaterialInput {

    @Field()
    courseModulId: number;

    @Field()
    name: string;

    @Field()
    learningTime: number;

    @Field()
    materialTypeId: number;

    @Field({ nullable: true })
    information: string;

    // @Field({ nullable: true })
    // exercise: boolean;
}

@ObjectType()
export class CourseMaterialPagination extends PaginationPage {
    @Field(type => [CourseMaterial])
    data: CourseMaterial[];
}
