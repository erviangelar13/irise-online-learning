import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { CourseSchedule } from '../../../entities/course-schedule.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { CourseScheduleService } from './course-schedule.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { ResponseMutation } from '../../../helpers/response.helper';
import { CourseScheduleInput } from './course-schedule.dto';

@Resolver(of => CourseSchedule)
export class CourseScheduleResolver {

    constructor(
        @Inject(CourseScheduleService) private courseScheduleService: CourseScheduleService,
    ) { }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseSchedule(
        @Args({ name: 'courseScheduleInput', type: () => CourseScheduleInput }) courseScheduleInput: CourseScheduleInput,
    ) {
        try {
            const data = await this.courseScheduleService.create(courseScheduleInput);
            return { id: data.id, message: 'Success create course schedule' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourseSchedule(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseScheduleInput', type: () => CourseScheduleInput }) courseScheduleInput: CourseScheduleInput,
    ) {
        try {
            const data = await this.courseScheduleService.update(id, courseScheduleInput);
            return { id: data.id, message: 'Success update course schedule' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteCourseSchedule(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseScheduleService.delete(id);
            return { id: data.id, message: 'Success delete course schedule' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
