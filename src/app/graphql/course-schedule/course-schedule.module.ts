import { Module } from '@nestjs/common';
import { CourseScheduleResolver } from './course-schedule.resolver';
import { CourseScheduleService } from './course-schedule.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseSchedule } from '../../../entities/course-schedule.entity';
import { Course } from '../../../entities/course.entity';

@Module({
    providers: [CourseScheduleResolver, CourseScheduleService],
    imports: [
        TypeOrmModule.forFeature([CourseSchedule, Course]),
    ],
})
export class CourseScheduleModule { }
