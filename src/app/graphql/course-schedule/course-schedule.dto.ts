import { InputType, Field, ObjectType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { CourseSchedule } from "../../../entities/course-schedule.entity";

@InputType()
export class CourseScheduleInput {

    @Field()
    courseId: number;

    @Field()
    date: string;

    @Field()
    timeStart: string;

    @Field()
    timeEnd: string;
}

@ObjectType()
export class CourseSchedulePagination extends PaginationPage {
    @Field(type => [CourseSchedule])
    data: CourseSchedule[];
}
