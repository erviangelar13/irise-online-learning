import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CourseSchedule } from '../../../entities/course-schedule.entity';
import { Course } from '../../../entities/course.entity';
import { CourseScheduleInput } from './course-schedule.dto';

@Injectable()
export class CourseScheduleService {

    constructor(
        @InjectRepository(CourseSchedule) private courseScheduleRepo: Repository<CourseSchedule>,
        @InjectRepository(Course) private courseRepo: Repository<Course>,
    ) { }

    async findById(id: number) {
        try {
            const data = await this.courseScheduleRepo.findOne(id);
            if (!data) throw new Error(`Course schedule with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: CourseScheduleInput) {
        try {
            const courseScheduleValidationData = await this.courseScheduleValidation(dto);
            const data = new CourseSchedule();
            data.date = dto.date;
            data.timeStart = dto.timeStart;
            data.timeEnd = dto.timeEnd;
            data.course = courseScheduleValidationData.courseData;
            await this.courseScheduleRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: CourseScheduleInput) {
        try {
            const data = await this.findById(id);
            const courseScheduleValidationData = await this.courseScheduleValidation(dto);
            data.date = dto.date;
            data.timeStart = dto.timeStart;
            data.timeEnd = dto.timeEnd;
            data.course = courseScheduleValidationData.courseData;
            await this.courseScheduleRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.courseScheduleRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    private async courseScheduleValidation(dto: CourseScheduleInput) {
        try {
            const courseData = await this.courseRepo.findOne(dto.courseId);
            if (!courseData) throw new Error(`Course with id = ${dto.courseId} is not found`);

            return {
                courseData,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
