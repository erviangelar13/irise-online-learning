import { Module } from '@nestjs/common';
import { CourseEvaluationResolver } from './course-evaluation.resolver';
import { CourseEvaluationService } from './course-evaluation.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseEvaluation } from '../../../entities/course-evaluation.entity';
import { Course } from '../../../entities/course.entity';

@Module({
    providers: [CourseEvaluationResolver, CourseEvaluationService],
    imports: [
        TypeOrmModule.forFeature([
            CourseEvaluation,
            Course,
        ]),
    ],
})
export class CourseEvaluationModule { }
