import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { CourseEvaluation } from "../../../entities/course-evaluation.entity";


@InputType()
export class CourseEvaluationInput {

    @Field()
    courseId: number;

    @Field()
    name: string;

    @Field()
    evaluationTime: number;

    @Field()
    passingGrade: number;

    questions: EvaluationQuestion[];
}

export class EvaluationQuestion {

    @Field()
    id: number;

    @Field()
    question: string;

    @Field()
    questionTypeId: number;

    @Field()
    options: EvaluationQuestionOptions[];
}

export class EvaluationQuestionOptions {

    @Field()
    id: number;

    @Field()
    answer: string;

    @Field()
    questionTypeId: number;

    @Field()
    correctAnswer: boolean;
}


@ObjectType()
export class CourseEvaluationPagination extends PaginationPage {
    @Field(type => [CourseEvaluation])
    data: CourseEvaluation[];
}
