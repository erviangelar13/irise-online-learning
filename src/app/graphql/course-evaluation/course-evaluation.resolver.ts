import { Inject, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { RolesGuard } from 'src/decorators/roles.decorator';
import { Scopes } from 'src/decorators/scopes.decorator';
import { CourseEvaluation } from 'src/entities/course-evaluation.entity';
import { Oauth2Guard } from 'src/guards/guards';
import { createPaginationOptions, PaginationInputType } from 'src/helpers/pagination.helper';
import { ResponseMutation } from 'src/helpers/response.helper';
import { CourseEvaluationInput, CourseEvaluationPagination } from './course-evaluation.dto';
import { CourseEvaluationService } from './course-evaluation.service';

@Resolver()
export class CourseEvaluationResolver {
    
    constructor(
        @Inject(CourseEvaluationService) private courseEvaluationService: CourseEvaluationService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseEvaluationPagination)
    async courseEvaluationList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.courseEvaluationService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseEvaluation)
    async courseEvaluation(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.courseEvaluationService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseEvaluation(
        @Args({ name: 'courseEvaluationInput', type: () => CourseEvaluationInput }) courseEvaluationInput: CourseEvaluationInput,
    ) {
        try {
            const data = await this.courseEvaluationService.create(courseEvaluationInput);
            return { id: data.id, message: 'Success create master type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourseEvaluation(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseEvaluationInput', type: () => CourseEvaluationInput }) courseEvaluationInput: CourseEvaluationInput,
    ) {
        try {
            const data = await this.courseEvaluationService.update(id, courseEvaluationInput);
            return { id: data.id, message: 'Success update master type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteCourseEvaluation(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseEvaluationService.delete(id);
            return { id: data.id, message: 'Success delete master type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
