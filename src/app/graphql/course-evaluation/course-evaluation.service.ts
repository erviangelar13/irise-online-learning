import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CourseEvaluation } from '../../../entities/course-evaluation.entity';
import { Repository } from 'typeorm';
import { PaginationOptions } from 'src/helpers/pagination.helper';
import { CourseEvaluationInput } from './course-evaluation.dto';
import { Course } from 'src/entities/course.entity';
import { Question } from 'src/entities/question.entity';
import { QuestionOptions } from 'src/entities/question-options.entity';

@Injectable()
export class CourseEvaluationService {
    
    constructor(
        @InjectRepository(CourseEvaluation) private courseEvaluationRepo: Repository<CourseEvaluation>,
        @InjectRepository(Course) private courseRepo: Repository<Course>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.courseEvaluationRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList() {
        try {
            return await this.courseEvaluationRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.courseEvaluationRepo.findOne(id);
            if (!data) throw new Error(`Master category with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: CourseEvaluationInput) {
        try {
            const validation = await this.dataValidation(dto)

            const data = new CourseEvaluation();
            data.course = validation.course;
            data.name = dto.name;
            data.evaluationTime = dto.evaluationTime;
            data.passingGrade = dto.passingGrade;

            var questions = [];
            dto.questions.forEach(item => {
                var question = new Question();
                question.question = item.question
                question.questionTypeId = item.questionTypeId

                var options = [];
                item.options.forEach(item2 => {
                    var option = new QuestionOptions();
                    option.answer = item2.answer;
                    option.correctAnswer = item2.correctAnswer;
                    options.push(option)
                })
                question.questionOptions = options;

                questions.push(question);
            });

            data.question = questions
            await this.courseEvaluationRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: CourseEvaluationInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.evaluationTime = dto.evaluationTime;
            data.passingGrade = dto.passingGrade;

            data.question.forEach(item => {
                // var question = dto.questions.find(x => x.id == item.id)
                dto.questions.forEach(element => {
                    if(item.id == element.id) {
                        item.question = element.question
                        item.questionTypeId = element.questionTypeId

                        item.questionOptions.forEach(option => {
                            element.options.forEach(dtoOption => {
                                if(option.id == dtoOption.id) {
                                    option.answer = dtoOption.answer
                                    option.correctAnswer = dtoOption.correctAnswer
                                }
                            })
                        });
                    }
                });
            })

            await this.courseEvaluationRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            if (!data) throw new Error(`Master category with id = ${id} is not found`);
            await this.courseEvaluationRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    private async dataValidation(dto: CourseEvaluationInput) {
        try {
            const course = await this.courseRepo.findOne(dto.courseId);
            if (!course) throw new Error(`Course with id = ${dto.courseId} is not found`);

            return {
                course,
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
