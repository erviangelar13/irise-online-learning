import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { dateMoment } from '../../../helpers/date.helper';
import { Content } from '../../../entities/content.entity';
import { ContentInput } from './content.dto';
import { IUploadOptions } from 'src/helpers/file.helper';
import { join } from 'path';
import * as fs from 'fs';
import { Config } from '../../../helpers/config.helper';
import { getMime } from '../../../helpers/mime.helper';

@Injectable()
export class ContentService {

    constructor(
        @InjectRepository(Content) private contentRepo: Repository<Content>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            const list = this.contentRepo.createQueryBuilder()
                .where(
                    `LOWER(title) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.contentRepo.findOne(id);
            if (!data) throw new Error(`Content with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findMediaById(id: number) {
        try {
            const data = await this.contentRepo.findOne(id, { relations: ['media'] });
            return data.media;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async viewBanner(id: number) {
        try {
            const data = await this.findById(id);
            const path = join(`${Config.get('BASE_PATH_FILE')}content/banner/`, data.bannerName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: ContentInput) {
        try {
            const data = new Content();
            data.contentTypeId = dto.contentTypeId;
            data.title = dto.title;
            data.slug = dto.title.toLowerCase().replace(' ', '-').replace(/[^a-zA-Z- ]/g, '');
            data.description = dto.description;
            data.isPublish = false;
            await this.contentRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: ContentInput) {
        try {
            const data = await this.findById(id);
            data.contentTypeId = dto.contentTypeId;
            data.title = dto.title;
            data.slug = dto.title.toLowerCase().replace(' ', '-').replace(/[^a-zA-Z- ]/g, "");
            data.description = dto.description;
            await this.contentRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.contentRepo.delete(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async publish(id: number) {
        try {
            const data = await this.findById(id);
            data.isPublish = true;
            data.publishDate = dateMoment(dateMoment().format('YYYY-MM-DD HH:mm')).toDate();
            await this.contentRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async unpublish(id: number) {
        try {
            const data = await this.findById(id);
            data.isPublish = false;
            await this.contentRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async setBanner(id: number, attachment: IUploadOptions) {
        try {
            const data = await this.findById(id);
            const oldBannerPath = data.bannerPath && data.bannerName ? join(data.bannerPath, data.bannerName) : null;

            data.bannerName = attachment.filename;
            data.bannerOriginalName = attachment.originalname;
            data.bannerPath = `${Config.get('BASE_PATH_FILE')}content/banner/`;
            data.bannerExtension = attachment.filename.substring(attachment.filename.lastIndexOf('.'), attachment.filename.length);

            await this.contentRepo.save(data);
            return oldBannerPath;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
