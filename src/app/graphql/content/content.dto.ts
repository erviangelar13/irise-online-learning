import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Content } from "../../../entities/content.entity";


@InputType()
export class ContentInput {

    @Field()
    contentTypeId: number;

    @Field()
    title: string;

    @Field()
    description: string;
}

@ObjectType()
export class ContentPagination extends PaginationPage {
    @Field(type => [Content])
    data: Content[];
}
