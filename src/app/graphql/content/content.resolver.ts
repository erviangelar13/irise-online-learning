import { Resolver, Query, Args, Mutation, ResolveField, Parent } from '@nestjs/graphql';
import { Content } from '../../../entities/content.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { ContentService } from './content.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { ContentPagination, ContentInput } from './content.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { Config } from '../../../helpers/config.helper';

@Resolver(of => Content)
export class ContentResolver {

    constructor(
        @Inject(ContentService) private contentService: ContentService,
    ) { }

    @Scopes('read')
    @Query(returns => ContentPagination)
    async contentList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.contentService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @Query(returns => Content)
    async content(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.contentService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async bannerURL(@Parent() content: Content) {
        const { id, bannerName } = content;
        return bannerName ? `${Config.get('BASE_URL')}view/content/banner/${id}` : null;
    }

    @ResolveField(returns => String)
    async media(@Parent() content: Content) {
        const { id } = content;
        return await this.contentService.findMediaById(id);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createContent(
        @Args({ name: 'contentInput', type: () => ContentInput }) contentInput: ContentInput,
    ) {
        try {
            const data = await this.contentService.create(contentInput);
            return { id: data.id, message: 'Success create content' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateContent(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'contentInput', type: () => ContentInput }) contentInput: ContentInput,
    ) {
        try {
            const data = await this.contentService.update(id, contentInput);
            return { id: data.id, message: 'Success update content' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteContent(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.contentService.delete(id);
            return { id: data.id, message: 'Success delete content' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async publishContent(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.contentService.publish(id);
            return { id: data.id, message: 'Success publish content' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async unpublishContent(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.contentService.unpublish(id);
            return { id: data.id, message: 'Success unpublish content' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
