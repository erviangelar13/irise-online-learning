import { Module } from '@nestjs/common';
import { ContentResolver } from './content.resolver';
import { ContentService } from './content.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Content } from '../../../entities/content.entity';

@Module({
    providers: [ContentResolver, ContentService],
    imports: [
        TypeOrmModule.forFeature([Content]),
    ],
})
export class ContentModule { }
