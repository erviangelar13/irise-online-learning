import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserCertificateFilter, UserCertificateInput } from './user-certificate.dto';
import { UsersCertificate } from '../../../entities/users-certificate.entity';
import { UsersCourse } from '../../../entities/users-course.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { join } from 'path';
import * as fs from 'fs';
import { Config } from '../../../helpers/config.helper';
import { getMime } from '../../../helpers/mime.helper';
import { dateMoment } from '../../../helpers/date.helper';
import { Certificate, generateCertificate } from '../../../helpers/certificate.helper';

@Injectable()
export class UsersCertificateService {

    constructor(
        @InjectRepository(UsersCertificate) private usersCertificateRepo: Repository<UsersCertificate>,
        @InjectRepository(UsersCourse) private usersCourseRepo: Repository<UsersCourse>
    ) { }

    async find(filter: UserCertificateFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.usersCertificateRepo.createQueryBuilder('usercertificate')
                .leftJoin(
                    'usercertificate.usersCourse',
                    'usersCourse',
                )
                .leftJoin(
                    'usersCourse.course',
                    'course',
                )
                .leftJoin(
                    'course.courseCategory',
                    'CourseCategory',
                )
                .where(
                    `(LOWER(course.name) LIKE :search
                    OR LOWER(usercourse.courseCode) LIKE :search)`,
                    { search: '%' + paginationOptions.search + '%' },
                );
                if(filter.categoryId) list.andWhere('CourseCategory.id = :CategoryId', { CategoryId : filter.categoryId })
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.usersCertificateRepo.findOne(id);
            if (!data) throw new Error(`User Certificate with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async fileURL(id: number) {
        try {
            const data = await this.findById(id);
            const path = join(`${Config.get('BASE_PATH_FILE')}course/certificate/`, data.fileName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async course(id: number) {
        try {
            const data = await this.usersCertificateRepo.findOne(id, { relations: ['usersCourse','usersCourse.course'] });
            if (!data) throw new Error(`User Course with idx = ${id} is not found`);
            return data.usersCourse.course;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async userCourse(id: number) {
        try {
            const data = await this.usersCertificateRepo.findOne(id, { relations: ['usersCourse'] });
            if (!data) throw new Error(`User Course with idx = ${id} is not found`);
            return data.usersCourse;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async generateCertificate(dto: UserCertificateInput) {
        try {
            const userCourse = await this.usersCourseRepo.findOne(dto.userCourseId, { relations: ['course'] });
            
            const userCertificate = new UsersCertificate();
            userCertificate.usersCourse = userCourse;
            userCertificate.date = dateMoment().toDate();

            var certificate = new Certificate();
            certificate.name = "Ervian Gelar Winahyu"
            certificate.course = `"Pengenalan Retro"`
            certificate.date = dateMoment().toDate().toLocaleDateString()
            var fileName = userCourse.courseCode + "-" + userCourse.user.name;
            const item = await generateCertificate(certificate,"certificate", "Certificate-Template.png", fileName)

            userCertificate.fileName = fileName;
            userCertificate.fileOriginalName = fileName;
            userCertificate.filePath = item.path; //`${Config.get('BASE_PATH_FILE')}course/certificate/`;
            userCertificate.fileExtension = item.extension;

            await this.usersCertificateRepo.save(userCertificate);
            return certificate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async testGenerate() {
        try {
            var certificate = new Certificate();
            certificate.name = "Ervian Gelar Winahyu"
            certificate.course = `"Pengenalan Retro"`
            certificate.date = dateMoment().toDate().toLocaleDateString()
            await generateCertificate(certificate,"certificate", "Certificate-Template.png", "Ervian-Cert")
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
