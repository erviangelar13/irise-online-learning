import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { UsersCourse } from '../../../entities/users-course.entity';
import { UsersCertificate } from '../../../entities/users-certificate.entity';
import { Users } from '../../../entities/users.entity';
import { Course } from '../../../entities/course.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { UsersCertificateService } from './user-certificate.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { UsersCertificatePagination, UserCertificateFilter } from './user-certificate.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => UsersCertificate)
export class UsersCertificateResolver {

    constructor(
        @Inject(UsersCertificateService) private usersCertificateService: UsersCertificateService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersCertificatePagination)
    async userCertificateList(
        @Args({ name: 'userCertificateFilter', type: () => UserCertificateFilter })
        userCertificateFilter: UserCertificateFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.usersCertificateService.find(userCertificateFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersCertificate)
    async userCertificate(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.usersCertificateService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => UsersCourse)
    async userCourse(@Parent() usersCertificate: UsersCertificate) {
        const { id } = usersCertificate;
        return this.usersCertificateService.userCourse(id);
    }

    @ResolveField(returns => Course)
    async course(@Parent() usersCertificate: UsersCertificate) {
        const { id } = usersCertificate;
        return this.usersCertificateService.course(id);
    }

    @ResolveField(returns => String)
    async fileURL(@Parent() usersCertificate: UsersCertificate) {
        const { id } = usersCertificate;
        return this.usersCertificateService.fileURL(id);
    }

    @Scopes('create')
    // @RolesGuard('admin')
    // @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async generateCertificate(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            await this.usersCertificateService.testGenerate();
            return { id: id, message: 'Success generate certificate' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
