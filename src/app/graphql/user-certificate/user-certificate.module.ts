import { Module } from '@nestjs/common';
import { UsersCertificateResolver } from './user-certificate.resolver';
import { UsersCertificateService } from './user-certificate.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersCourse } from '../../../entities/users-course.entity';
import { UsersCertificate } from '../../../entities/users-certificate.entity';
import { Users } from '../../../entities/users.entity';
import { Course } from '../../../entities/course.entity';

@Module({
    providers: [UsersCertificateResolver, UsersCertificateService],
    imports: [
        TypeOrmModule.forFeature([
            UsersCertificate,
            UsersCourse,
            Users,
            Course
        ]),
    ],
})
export class UserCertificateModule { }
