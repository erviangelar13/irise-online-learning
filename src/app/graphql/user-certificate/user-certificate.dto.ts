import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { UsersCertificate } from "../../../entities/users-certificate.entity";


@InputType()
export class UserCertificateFilter {

    @Field({ nullable: true })
    categoryId: number;
}

@InputType()
export class UserCertificateInput {

    @Field({ nullable: true })
    userCourseId: number;
}

@ObjectType()
export class UsersCertificatePagination extends PaginationPage {
    @Field(type => [UsersCertificate])
    data: UsersCertificate[];
}
