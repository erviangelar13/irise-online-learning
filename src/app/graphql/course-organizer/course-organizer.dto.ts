import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { CourseOrganizer } from "../../../entities/course-organizer.entity";

@InputType()
export class CourseOrganizerFilter {

    @Field({ nullable: true})
    organizationTypeId: number;
}

@InputType()
export class CourseOrganizerInput {

    @Field()
    name: string;

    @Field()
    organizationTypeId: number;

    @Field()
    description: string;
}

@ObjectType()
export class CourseOrganizerPagination extends PaginationPage {
    @Field(type => [CourseOrganizer])
    data: CourseOrganizer[];
}
