import { Module } from '@nestjs/common';
import { CourseOrganizerResolver } from './course-organizer.resolver';
import { CourseOrganizerService } from './course-organizer.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseOrganizer } from '../../../entities/course-organizer.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [CourseOrganizerResolver, CourseOrganizerService],
    imports: [
        TypeOrmModule.forFeature([CourseOrganizer, MasterReference]),
    ],
})
export class CourseOrganizerModule { }
