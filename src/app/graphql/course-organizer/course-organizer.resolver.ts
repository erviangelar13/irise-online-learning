import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { CourseOrganizer } from '../../../entities/course-organizer.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { CourseOrganizerService } from './course-organizer.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { CourseOrganizerPagination, CourseOrganizerFilter, CourseOrganizerInput } from './course-organizer.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { Config } from '../../../helpers/config.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { removeFile } from '../../../helpers/file.helper';

@Resolver(of => CourseOrganizer)
export class CourseOrganizerResolver {

    constructor(
        @Inject(CourseOrganizerService) private courseOrganizerService: CourseOrganizerService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseOrganizerPagination)
    async courseOrganizerList(
        @Args({ name: 'courseOrganizationFilter', type: () => CourseOrganizerFilter, nullable: true })
        courseOrganizerFilter: CourseOrganizerFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.courseOrganizerService.find(courseOrganizerFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [CourseOrganizer])
    async courseOrganizes(
        @Args({ name: 'courseOrganizationFilter', type: () => CourseOrganizerFilter, nullable: true })
        courseOrganizerFilter: CourseOrganizerFilter,
    ) {
        try {
            return await this.courseOrganizerService.findAll(courseOrganizerFilter);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseOrganizer)
    async courseOrganizer(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.courseOrganizerService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async photoURL(@Parent() courseOrganizer: CourseOrganizer) {
        const { id, photoName } = courseOrganizer;
        return photoName ? `${Config.get('BASE_URL')}view/course-organizer/photo/${id}` : null;
    }

    @ResolveField(returns => String)
    async organizationType(@Parent() courseOrganizer: CourseOrganizer) {
        const { organizationTypeId } = courseOrganizer;
        return await this.courseOrganizerService.organizationType(organizationTypeId);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseOrganizer(
        @Args({ name: 'courseOrganizerInput', type: () => CourseOrganizerInput }) courseOrganizerInput: CourseOrganizerInput,
    ) {
        try {
            const data = await this.courseOrganizerService.create(courseOrganizerInput);
            return { id: data.id, message: 'Success create course organizer' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourseOrganizer(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseOrganizerInput', type: () => CourseOrganizerInput }) courseOrganizerInput: CourseOrganizerInput,
    ) {
        try {
            const data = await this.courseOrganizerService.update(id, courseOrganizerInput);
            return { id: data.id, message: 'Success update course organizer' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteCourseOrganizer(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseOrganizerService.delete(id);
            if (data.photoName) await removeFile(data.photoPath, data.photoName);
            return { id: data.id, message: 'Success delete course organizer' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
