import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CourseOrganizer } from '../../../entities/course-organizer.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { CourseOrganizerFilter, CourseOrganizerInput } from './course-organizer.dto';
import { join } from 'path';
import { getMime } from '../../../helpers/mime.helper';
import * as fs from 'fs';
import { Config } from '../../../helpers/config.helper';
import { IUploadOptions } from '../../../helpers/file.helper';
import { MasterReference } from '../../../entities/master-reference.entity';

@Injectable()
export class CourseOrganizerService {

    constructor(
        @InjectRepository(CourseOrganizer) private courseOrganizerRepo: Repository<CourseOrganizer>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
    ) { }

    async find(filter: CourseOrganizerFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.courseOrganizerRepo.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                );

            if (filter.organizationTypeId) list.andWhere('organization_type_id = :organizationTypeId', { organizationTypeId: filter.organizationTypeId });
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findAll(filter: CourseOrganizerFilter) {
        try {
            const list = this.courseOrganizerRepo;

            if (filter.organizationTypeId) return await list.find({ organizationTypeId: filter.organizationTypeId });
            return await list.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.courseOrganizerRepo.findOne(id);
            if (!data) throw new Error(`Course organizer with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async viewPhoto(id: number) {
        try {
            const data = await this.findById(id);
            const path = join(`${Config.get('BASE_PATH_FILE')}course_organizer/`, data.photoName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async organizationType(id: number) {
        const data = await this.masterReferenceRepo.findOne({ group: 'organization_type', idx: id });
        return data.name;
    }

    async create(dto: CourseOrganizerInput) {
        try {
            const data = new CourseOrganizer();
            data.name = dto.name;
            data.organizationTypeId = dto.organizationTypeId;
            data.description = dto.description;
            await this.courseOrganizerRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: CourseOrganizerInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.organizationTypeId = dto.organizationTypeId;
            data.description = dto.description;
            await this.courseOrganizerRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.courseOrganizerRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async setPhoto(id: number, attachment: IUploadOptions) {
        try {
            const data = await this.findById(id);
            const oldPhotoPath = data.photoPath && data.photoName ? join(data.photoPath, data.photoName) : null;

            data.photoName = attachment.filename;
            data.photoOriginalName = attachment.originalname;
            data.photoPath = `${Config.get('BASE_PATH_FILE')}course_organizer/`;
            data.photoExtension = attachment.filename.substring(attachment.filename.lastIndexOf('.'), attachment.filename.length);

            await this.courseOrganizerRepo.save(data);
            return oldPhotoPath;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
