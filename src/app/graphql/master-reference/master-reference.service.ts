import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MasterReference } from '../../../entities/master-reference.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { MasterReferenceInput } from './master-reference.dto';

@Injectable()
export class MasterReferenceService {

    constructor(
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
    ) { }

    async find(group: string, paginationOptions: PaginationOptions) {
        try {
            const list = this.masterReferenceRepo.createQueryBuilder('mr')
                .where(
                    `LOWER(mr.name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            if (group) {
                list.andWhere('mr.group = :group', { group });
            }
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList(group: string) {
        try {
            const list = this.masterReferenceRepo //.createQueryBuilder('mr');
            if (group) {
                return await list.find({group : group});
            }
            return await list.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.masterReferenceRepo.findOne(id);
            if (!data) throw new Error(`Master referenece with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findGroup() {
        try {
            const data = await this.masterReferenceRepo.createQueryBuilder('mr')
                .select('mr.group')
                .groupBy('mr.group')
                .getRawMany();

            const groups: string[] = [];
            for (const item of data) {
                groups.push(item.mr_group);
            }
            return groups;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: MasterReferenceInput) {
        try {
            const data = new MasterReference();
            data.group = dto.group;
            data.idx = dto.idx;
            data.name = dto.name;
            data.description = dto.description;
            await this.masterReferenceRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: MasterReferenceInput) {
        try {
            const data = await this.findById(id);
            data.group = dto.group;
            data.idx = dto.idx;
            data.name = dto.name;
            data.description = dto.description;
            await this.masterReferenceRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.masterReferenceRepo.delete(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
