import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { MasterReference } from "../../../entities/master-reference.entity";


@InputType()
export class MasterReferenceInput {

    @Field()
    group: string;

    @Field()
    idx: number;

    @Field()
    name: string;

    @Field()
    description: string;
}

@ObjectType()
export class MasterReferencePagination extends PaginationPage {
    @Field(type => [MasterReference])
    data: MasterReference[];
}
