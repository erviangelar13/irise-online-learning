import { Module } from '@nestjs/common';
import { MasterReferenceResolver } from './master-reference.resolver';
import { MasterReferenceService } from './master-reference.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [MasterReferenceResolver, MasterReferenceService],
    imports: [
        TypeOrmModule.forFeature([MasterReference]),
    ],
})
export class MasterReferenceModule { }
