import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { MasterReference } from '../../../entities/master-reference.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { MasterReferenceService } from './master-reference.service';
import { Oauth2Guard } from '../../../guards/guards';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { MasterReferencePagination, MasterReferenceInput } from './master-reference.dto';
import { Scopes } from '../../../decorators/scopes.decorator';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => MasterReference)
export class MasterReferenceResolver {

    constructor(
        @Inject(MasterReferenceService) private masterReferenceService: MasterReferenceService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => MasterReferencePagination)
    async masterReferenceList(
        @Args({ name: 'group', type: () => String, nullable: true }) group: string,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.masterReferenceService.find(group, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [MasterReference])
    async masterReferences(
        @Args({ name: 'group', type: () => String, nullable: true }) group: string
    ) {
        try {
            return await this.masterReferenceService.findList(group);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => MasterReference)
    async masterReference(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.masterReferenceService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [String])
    async masterReferenceGroup() {
        try {
            return await this.masterReferenceService.findGroup();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createMasterReference(
        @Args({ name: 'masterReferenceInput', type: () => MasterReferenceInput }) masterReferenceInput: MasterReferenceInput,
    ) {
        try {
            const data = await this.masterReferenceService.create(masterReferenceInput);
            return { id: data.id, message: 'Success create master reference' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateMasterReference(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'masterReferenceInput', type: () => MasterReferenceInput }) masterReferenceInput: MasterReferenceInput,
    ) {
        try {
            const data = await this.masterReferenceService.update(id, masterReferenceInput);
            return { id: data.id, message: 'Success update master reference' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteMasterReference(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.masterReferenceService.delete(id);
            return { id: data.id, message: 'Success delete master reference' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
