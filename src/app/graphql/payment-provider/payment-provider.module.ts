import { Module } from '@nestjs/common';
import { PaymentProviderResolver } from './payment-provider.resolver';
import { PaymentProviderService } from './payment-provider.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { PaymentMethod } from '../../../entities/payment-method.entity';

@Module({
    providers: [PaymentProviderResolver, PaymentProviderService],
    imports: [
        TypeOrmModule.forFeature([
            PaymentProvider,
            PaymentMethod,
        ]),
    ],
})
export class PaymentProviderModule { }
