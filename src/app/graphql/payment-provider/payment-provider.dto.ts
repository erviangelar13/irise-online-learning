import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { PaymentProvider } from "../../../entities/payment-provider.entity";


@InputType()
export class PaymentProviderInput {

    @Field()
    name: string;

    @Field()
    paymentMethodId: number;

    @Field()
    description: string;

    @Field({ nullable: true })
    urlPayment: string;
}

@ObjectType()
export class PaymentProviderPagination extends PaginationPage {
    @Field(type => [PaymentProvider])
    data: PaymentProvider[];
}
