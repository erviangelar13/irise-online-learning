import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { PaymentProviderInput } from './payment-provider.dto';
import { PaymentMethod } from '../../../entities/payment-method.entity';
import { IUploadOptions } from '../../../helpers/file.helper';
import { Config } from '../../../helpers/config.helper';
import { join } from 'path';
import * as fs from 'fs';
import { getMime } from '../../../helpers/mime.helper';

@Injectable()
export class PaymentProviderService {

    constructor(
        @InjectRepository(PaymentProvider) private paymentProvider: Repository<PaymentProvider>,
        @InjectRepository(PaymentMethod) private paymentMethod: Repository<PaymentMethod>,
    ) { }

    async find(method_Id: number, paginationOptions: PaginationOptions) {
        try {
            var list = this.paymentProvider.createQueryBuilder()
                .where(
                    `LOWER(name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                );
                
            if (method_Id != -1) {
                list.andWhere('payment_method_id = :method_Id', { method_Id });
            }
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList(method_Id: number) {
        try {
            var list = this.paymentProvider.createQueryBuilder();
            if (method_Id != -1 && method_Id != null) {
                list.andWhere('payment_method_id = :method_Id', { method_Id });
            }
            return await list.getMany();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.paymentProvider.findOne(id);
            if (!data) throw new Error(`Payment provider with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findPaymentMethodById(id: number) {
        try {
            const data = await this.paymentProvider.findOne(id, { relations: ['paymentMethod'] });
            return data.paymentMethod;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async viewLogo(id: number) {
        try {
            const data = await this.findById(id);
            const path = join(`${Config.get('BASE_PATH_FILE')}payment_provider/`, data.logoName),
                mimeFile = getMime(path),
                file = fs.readFileSync(path);

            return {
                mime: mimeFile,
                file: file,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: PaymentProviderInput) {
        try {
            const paymentMethodData = await this.paymentMethod.findOne(dto.paymentMethodId);
            if (!paymentMethodData) throw new Error(`Payment method with id = ${dto.paymentMethodId} is not found`);

            const data = new PaymentProvider();
            data.name = dto.name;
            data.description = dto.description;
            data.urlPayment = dto.urlPayment;
            data.paymentMethod = paymentMethodData;
            await this.paymentProvider.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: PaymentProviderInput) {
        try {
            const paymentMethodData = await this.paymentMethod.findOne(dto.paymentMethodId);
            if (!paymentMethodData) throw new Error(`Payment method with id = ${dto.paymentMethodId} is not found`);

            const data = await this.findById(id);
            data.name = dto.name;
            data.description = dto.description;
            data.urlPayment = dto.urlPayment;
            data.paymentMethod = paymentMethodData;
            await this.paymentProvider.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            await this.paymentProvider.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async setLogo(id: number, attachment: IUploadOptions) {
        try {
            const data = await this.findById(id);
            const oldLogoPath = data.logoPath && data.logoName ? join(data.logoPath, data.logoName) : null;

            data.logoName = attachment.filename;
            data.logoOriginalName = attachment.originalname;
            data.logoPath = `${Config.get('BASE_PATH_FILE')}payment_provider/`;
            data.logoExtension = attachment.filename.substring(attachment.filename.lastIndexOf('.'), attachment.filename.length);

            await this.paymentProvider.save(data);
            return oldLogoPath;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
