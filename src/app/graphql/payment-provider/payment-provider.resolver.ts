import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { PaymentProviderService } from './payment-provider.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { PaymentProviderPagination, PaymentProviderInput } from './payment-provider.dto';
import { Config } from '../../../helpers/config.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { removeFile } from '../../../helpers/file.helper';
import { PaymentMethod } from '../../../entities/payment-method.entity';

@Resolver(of => PaymentProvider)
export class PaymentProviderResolver {

    constructor(
        @Inject(PaymentProviderService) private paymentProviderService: PaymentProviderService,
    ) { }

    @Scopes('read')
    @Query(returns => PaymentProviderPagination)
    async paymentProviderList(
        @Args({ name: 'method', type: () => Number }) method: number,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.paymentProviderService.find(method,paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [PaymentProvider])
    async paymentProviders(
        @Args({ name: 'method', type: () => Number, nullable: true }) method: number
    ) {
        try {
            return await this.paymentProviderService.findList(method);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => PaymentProvider)
    async paymentProvider(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.paymentProviderService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async logoURL(@Parent() paymentProvider: PaymentProvider) {
        const { id, logoName } = paymentProvider;
        return logoName ? `${Config.get('BASE_URL')}view/payment-provider/logo/${id}` : null;
    }

    @ResolveField(returns => PaymentMethod)
    async paymentMethod(@Parent() paymentProvider: PaymentProvider) {
        const { id } = paymentProvider;
        return await this.paymentProviderService.findPaymentMethodById(id);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createPaymentProvider(
        @Args({ name: 'paymentProviderInput', type: () => PaymentProviderInput }) paymentProviderInput: PaymentProviderInput,
    ) {
        try {
            const data = await this.paymentProviderService.create(paymentProviderInput);
            return { id: data.id, message: 'Success create payment provider' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updatePaymentProvider(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'paymentProviderInput', type: () => PaymentProviderInput }) paymentProviderInput: PaymentProviderInput,
    ) {
        try {
            const data = await this.paymentProviderService.update(id, paymentProviderInput);
            return { id: data.id, message: 'Success update payment provider' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deletePaymentProvider(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.paymentProviderService.delete(id);
            if (data.logoName) await removeFile(data.logoPath, data.logoName);
            return { id: data.id, message: 'Success delete payment provider' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
