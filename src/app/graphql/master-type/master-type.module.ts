import { Module } from '@nestjs/common';
import { MasterTypeResolver } from './master-type.resolver';
import { MasterTypeService } from './master-type.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MasterType } from '../../../entities/master-type.entity';

@Module({
    providers: [MasterTypeResolver, MasterTypeService],
    imports: [
        TypeOrmModule.forFeature([MasterType]),
    ],
})
export class MasterTypeModule { }
