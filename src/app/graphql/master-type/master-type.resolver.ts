import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { MasterType } from '../../../entities/master-type.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { MasterTypeService } from './master-type.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { MasterTypeInput, MasterTypePagination } from './master-type.dto';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => MasterType)
export class MasterTypeResolver {

    constructor(
        @Inject(MasterTypeService) private masterTypeService: MasterTypeService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => MasterTypePagination)
    async masterTypeList(
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.masterTypeService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [MasterType])
    async masterTypes() {
        try {
            return await this.masterTypeService.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => MasterType)
    async masterType(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.masterTypeService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createMasterType(
        @Args({ name: 'masterTypeInput', type: () => MasterTypeInput }) masterTypeInput: MasterTypeInput,
    ) {
        try {
            const data = await this.masterTypeService.create(masterTypeInput);
            return { id: data.id, message: 'Success create master type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateMasterType(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'masterTypeInput', type: () => MasterTypeInput }) masterTypeInput: MasterTypeInput,
    ) {
        try {
            const data = await this.masterTypeService.update(id, masterTypeInput);
            return { id: data.id, message: 'Success update master type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteMasterType(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.masterTypeService.delete(id);
            return { id: data.id, message: 'Success delete master type' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

}
