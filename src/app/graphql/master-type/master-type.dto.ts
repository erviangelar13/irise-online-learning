import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { MasterType } from "../../../entities/master-type.entity";


@InputType()
export class MasterTypeInput {

    @Field()
    name: string;

    @Field()
    description: string;
}

@ObjectType()
export class MasterTypePagination extends PaginationPage {
    @Field(type => [MasterType])
    data: MasterType[];
}
