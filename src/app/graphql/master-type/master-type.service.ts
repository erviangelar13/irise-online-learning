import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MasterType } from '../../../entities/master-type.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { MasterTypeInput } from './master-type.dto';

@Injectable()
export class MasterTypeService {

    constructor(
        @InjectRepository(MasterType) private masterTypeRepo: Repository<MasterType>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            const list = this.masterTypeRepo.createQueryBuilder('mr')
                .where(
                    `LOWER(mr.name) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findAll() {
        try {
            return await this.masterTypeRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.masterTypeRepo.findOne(id);
            if (!data) throw new Error(`Master type with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: MasterTypeInput) {
        try {
            const data = new MasterType();
            data.name = dto.name;
            data.description = dto.description;
            await this.masterTypeRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: MasterTypeInput) {
        try {
            const data = await this.findById(id);
            data.name = dto.name;
            data.description = dto.description;
            await this.masterTypeRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const data = await this.findById(id);
            if (!data) throw new Error(`Master type with id = ${id} is not found`);
            await this.masterTypeRepo.delete(id);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
