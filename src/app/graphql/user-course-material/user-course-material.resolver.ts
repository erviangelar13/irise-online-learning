import { Resolver, Query, Args, ResolveField, Parent, Mutation } from '@nestjs/graphql';
import { UsersCourseMaterial } from '../../../entities/users-course-material.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { UsersCourseMaterialService } from './user-course-material.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { AuthUser } from '../../../decorators/auth.decorator';
import { Auth } from '../../../helpers/auth.helper';
import { UserCourseMaterialPagination, UserCourseMaterialInput } from './user-course-material.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';

@Resolver(of => UsersCourseMaterial)
export class UsersCourseMaterialResolver {

    constructor(
        @Inject(UsersCourseMaterialService) private usersCourseMaterialService: UsersCourseMaterialService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UserCourseMaterialPagination)
    async userCourseMaterialList(
        @AuthUser() auth: Auth,
        @Args({ name: 'userCourseId', type: () => Number }) userCourseId: number,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.usersCourseMaterialService.find(userCourseId, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => UsersCourseMaterial)
    async userCourseMaterial(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.usersCourseMaterialService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async status(@Parent() usersCourseMaterial: UsersCourseMaterial) {
        const { statusId } = usersCourseMaterial;
        return this.usersCourseMaterialService.status(statusId);
    }

    @Scopes('create')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createUserCourseMaterial(
        @Args({ name: 'userCourseMaterialInput', type: () => UserCourseMaterialInput }) userCourseMaterialInput: UserCourseMaterialInput,
    ) {
        try {
            const data = await this.usersCourseMaterialService.create(userCourseMaterialInput);
            return { id: data.id, message: 'Success create user course material' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateUserCourseMaterial(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.usersCourseMaterialService.updateDone(id);
            return { id: data.id, message: 'Success update user course material' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

}
