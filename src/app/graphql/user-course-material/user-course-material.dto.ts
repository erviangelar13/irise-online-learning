import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { UsersCourseMaterial } from "../../../entities/users-course-material.entity";


@InputType()
export class UserCourseMaterialInput {

    @Field({ nullable: true })
    userCourseId: number;

    @Field({ nullable: true })
    materialId: number;
}

@ObjectType()
export class UserCourseMaterialPagination extends PaginationPage {
    @Field(type => [UsersCourseMaterial])
    data: UsersCourseMaterial[];
}
