import { Module } from '@nestjs/common';
import { UsersCourseMaterialResolver } from './user-course-material.resolver';
import { UsersCourseMaterialService } from './user-course-material.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersCourse } from '../../../entities/users-course.entity';
import { CourseMaterial } from '../../../entities/course-material.entity';
import { UsersCourseMaterial } from '../../../entities/users-course-material.entity';
import { MasterReference } from '../../../entities/master-reference.entity';

@Module({
    providers: [UsersCourseMaterialResolver, UsersCourseMaterialService],
    imports: [
        TypeOrmModule.forFeature([
            UsersCourse,
            CourseMaterial,
            UsersCourseMaterial,
            MasterReference
        ]),
    ],
})
export class UserCourseMaterialModule { }
