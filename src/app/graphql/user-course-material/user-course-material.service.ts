import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { UserCourseMaterialInput } from './user-course-material.dto';
import { UsersCourse } from '../../../entities/users-course.entity';
import { CourseMaterial } from '../../../entities/course-material.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { MasterReference } from '../../../entities/master-reference.entity';
import { UsersCourseMaterial } from '../../../entities/users-course-material.entity';
import { dateMoment } from '../../../helpers/date.helper';
import { UsersCertificate } from '../../../entities/users-certificate.entity';
import { Certificate, generateCertificate } from '../../../helpers/certificate.helper';

@Injectable()
export class UsersCourseMaterialService {

    constructor(
        @InjectRepository(UsersCourse) private usersCourseRepo: Repository<UsersCourse>,
        @InjectRepository(CourseMaterial) private courseMaterialRepo: Repository<CourseMaterial>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
        @InjectRepository(UsersCourseMaterial) private usersCourseMaterialRepo: Repository<UsersCourseMaterial>,
        @InjectRepository(UsersCertificate) private usersCertificateRepo: Repository<UsersCertificate>
    ) { }

    async find(userCourseId: number, paginationOptions: PaginationOptions) {
        try {
            const list = this.usersCourseMaterialRepo.createQueryBuilder('usercoursematerial')
                .leftJoin(
                    'usercoursematerial.usersCourse',
                    'usersCourse',
                )
                .leftJoin(
                    'usercoursematerial.courseMaterial',
                    'courseMaterial',
                )
                .leftJoin(
                    'usersCourse.user',
                    'user',
                )
                .leftJoin(
                    'usersCourse.course',
                    'course',
                )
                .where(
                    `usersCourse.id == :userCourseId`,
                    { userCourseId: userCourseId },
                );
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.usersCourseMaterialRepo.findOne(id);
            if (!data) throw new Error(`User Course Material with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async status(idx: number) {
        try {
            const where = {
                idx : idx,
                group : 'learn_status'
            };
            const data = await this.masterReferenceRepo.findOne(where);
            if (!data) throw new Error(`Learn Status with idx = ${idx} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }
    
    async create(dto: UserCourseMaterialInput) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const validationData = await this.transactionValidation(dto);
            // console.log(validationData)
            const data = new UsersCourseMaterial();
            data.courseMaterial = validationData.courseMaterial;
            data.usersCourse = validationData.userCourse;
            data.date = dateMoment().toDate();
            data.statusId = 2;
            await queryRunner.manager.save(data);

            var total_material = 0
            for await (const modules of validationData.userCourse.course.courseModul) {
                modules.courseMaterial.forEach(() => {
                    ++total_material;
                })
            }
            if(total_material == (validationData.userCourse.usersCourseMaterial.length + 1)) {
                var userCourse = validationData.userCourse;
                userCourse.statusId = 3
                await queryRunner.manager.save(userCourse);

                const userCertificate = new UsersCertificate();
                userCertificate.usersCourse = userCourse;
                userCertificate.date = dateMoment().toDate();
    
                var certificate = new Certificate();
                certificate.name = userCourse.user.name.toUpperCase()
                certificate.course = `"${userCourse.course.name}"`
                certificate.date = dateMoment().toDate().toLocaleDateString()
                var fileName = userCourse.courseCode + "-" + userCourse.user.name;
                const item = await generateCertificate(certificate,"certificate", "Certificate-Template.png", fileName)
    
                userCertificate.fileName = fileName;
                userCertificate.fileOriginalName = fileName;
                userCertificate.filePath = item.path;
                userCertificate.fileExtension = item.extension;
    
                await this.usersCertificateRepo.save(userCertificate);
            }

            await queryRunner.commitTransaction();
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async updateDone(id: number) {
        try {
            const data = await this.findById(id);
            data.statusId = 3;
            await this.usersCourseMaterialRepo.save(data);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    private async transactionValidation(dto: UserCourseMaterialInput) {
        try {

            const userCourse = await this.usersCourseRepo.findOne(dto.userCourseId, { relations: ['course', 'course.courseModul', 'course.courseModul.courseMaterial', 'usersCourseMaterial'] });
            if (!userCourse) throw new Error(`User Course with id = ${dto.userCourseId} is not found`);
            
            const courseMaterial = await this.courseMaterialRepo.findOne(dto.materialId);
            if (!courseMaterial) throw new Error(`Course Material with id = ${dto.materialId} is not found`);

            return {
                userCourse,
                courseMaterial,
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
