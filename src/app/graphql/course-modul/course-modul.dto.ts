import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { CourseModul } from "../../../entities/course-modul.entity";


@InputType()
export class CourseModulInput {

    @Field()
    courseId: number;

    @Field()
    name: string;

    @Field()
    learningTime: number;

    @Field({ nullable: true })
    seqNo: number;
}

@ObjectType()
export class CourseModulPagination extends PaginationPage {
    @Field(type => [CourseModul])
    data: CourseModul[];
}
