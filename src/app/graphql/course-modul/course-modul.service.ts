import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CourseModul } from '../../../entities/course-modul.entity';
import { CourseModulInput } from './course-modul.dto';
import { Course } from '../../../entities/course.entity';

@Injectable()
export class CourseModulService {

    constructor(
        @InjectRepository(CourseModul) private courseModulRepo: Repository<CourseModul>,
        @InjectRepository(Course) private courseRepo: Repository<Course>,
    ) { }

    async findByCourseId(id: number) {
        try {
            const data = await this.courseModulRepo.createQueryBuilder()
                .where('course_id = :courseId', { courseId: id })
                .orderBy('seq_no', 'ASC')
                .getMany();

            return data;
        } catch (error) {
            throw new Error(error.messsage);
        }
    }

    async findById(id: number) {
        try {
            const courseModulData = await this.courseModulRepo.findOne(id);
            if (!courseModulData) throw new Error(`Course modul with id = ${id} is not found`);

            return courseModulData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findCourseMaterialById(id: number) {
        try {
            const data = await this.courseModulRepo.findOne(id, { relations: ['courseMaterial'] });
            return data.courseMaterial;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async create(dto: CourseModulInput) {
        try {
            const courseModulValidationData = await this.courseModulValidation(dto);
            const course = courseModulValidationData.course;

            const courseModulData = new CourseModul();
            courseModulData.course = course;
            courseModulData.name = dto.name;
            courseModulData.learningTime = dto.learningTime;
            courseModulData.seqNo = dto.seqNo;

            await this.courseModulRepo.save(courseModulData);

            return courseModulData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async update(id: number, dto: CourseModulInput) {
        try {
            const courseModulData = await this.findById(id);

            const courseModulValidationData = await this.courseModulValidation(dto);
            const course = courseModulValidationData.course;

            courseModulData.course = course;
            courseModulData.name = dto.name;
            courseModulData.learningTime = dto.learningTime;
            courseModulData.seqNo = dto.seqNo;

            await this.courseModulRepo.save(courseModulData);

            return courseModulData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async delete(id: number) {
        try {
            const courseModulData = await this.findById(id);
            await this.courseModulRepo.delete(id);
            return courseModulData;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    private async courseModulValidation(dto: CourseModulInput) {
        try {
            const course = await this.courseRepo.findOne(dto.courseId);
            if (!course) throw new Error(`Course with id = ${dto.courseId} is not found`);

            return {
                course,
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
