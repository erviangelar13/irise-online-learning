import { Module } from '@nestjs/common';
import { CourseModulResolver } from './course-modul.resolver';
import { CourseModulService } from './course-modul.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseModul } from '../../../entities/course-modul.entity';
import { Course } from '../../../entities/course.entity';

@Module({
    providers: [CourseModulResolver, CourseModulService],
    imports: [
        TypeOrmModule.forFeature([
            CourseModul,
            Course,
        ]),
    ],
})
export class CourseModulModule { }
