import { Resolver, Query, Args, Mutation, ResolveField, Parent } from '@nestjs/graphql';
import { CourseModul } from '../../../entities/course-modul.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { CourseModulService } from './course-modul.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { CourseModulInput } from './course-modul.dto';
import { CourseMaterial } from '../../../entities/course-material.entity';

@Resolver(of => CourseModul)
export class CourseModulResolver {

    constructor(
        @Inject(CourseModulService) private courseModulService: CourseModulService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [CourseModul])
    async courseModulList(
        @Args({ name: 'courseId', type: () => Number }) courseId: number,
    ) {
        try {
            return await this.courseModulService.findByCourseId(courseId);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => CourseModul)
    async courseModul(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.courseModulService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => [CourseMaterial])
    async courseMaterial(@Parent() courseModul: CourseModul) {
        const { id } = courseModul;
        return this.courseModulService.findCourseMaterialById(id);
    }

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async createCourseModul(
        @Args({ name: 'courseModulInput', type: () => CourseModulInput }) courseModulInput: CourseModulInput,
    ) {
        try {
            const data = await this.courseModulService.create(courseModulInput);
            return { id: data.id, message: 'Success create course modul' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async updateCourseModul(
        @Args({ name: 'id', type: () => Number }) id: number,
        @Args({ name: 'courseModulInput', type: () => CourseModulInput }) courseModulInput: CourseModulInput,
    ) {
        try {
            const data = await this.courseModulService.update(id, courseModulInput);
            return { id: data.id, message: 'Success update course modul' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('delete')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deleteCourseModul(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.courseModulService.delete(id);
            return { id: data.id, message: 'Success delete course modul' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
