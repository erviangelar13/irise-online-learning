import { Resolver, Query, Args, Mutation, ResolveField, Parent } from '@nestjs/graphql';
import { Voucher } from '../../../entities/voucher.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { VoucherService } from './voucher.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { AuthUser } from '../../../decorators/auth.decorator';
import { Auth } from '../../../helpers/auth.helper';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { VoucherPagination, VoucherFilter } from './voucher.dto';
import { RolesGuard } from '../../../decorators/roles.decorator';
import { ResponseMutation } from '../../../helpers/response.helper';
import { Users } from '../../../entities/users.entity';

@Resolver(of => Voucher)
export class VoucherResolver {

    constructor(
        @Inject(VoucherService) private voucherService: VoucherService,
    ) { }

    @Scopes('read')
    @Query(returns => VoucherPagination)
    async voucherList(
        @Args({ name: 'voucherFilter', type: () => VoucherFilter, nullable: true })
        voucherFilter: VoucherFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.voucherService.find(voucherFilter, paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    // @UseGuards(Oauth2Guard)
    @Query(returns => [Voucher])
    async vocuhers() {
        try {
            return await this.voucherService.findList();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => Voucher)
    async voucher(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.voucherService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async invoiceNo(@Parent() voucher: Voucher) {
        const { id } = voucher;
        return this.voucherService.findInvoiceNoById(id);
    }

    @ResolveField(returns => String)
    async orderNo(@Parent() voucher: Voucher) {
        const { id } = voucher;
        return this.voucherService.findOrderNoById(id);
    }

    @ResolveField(returns => String)
    async used(@Parent() voucher: Voucher) {
        const { redeemDate } = voucher;
        return !redeemDate == null;
    }

    @ResolveField(returns => String)
    async orders(@Parent() voucher: Voucher) {
        const { id } = voucher;
        return this.voucherService.findOrderById(id);
    }

    @ResolveField(returns => Users)
    async user(@Parent() voucher: Voucher) {
        const { userReedemId } = voucher;
        return !userReedemId? null : this.voucherService.findUserById(userReedemId);
    }

    @ResolveField(returns => Users)
    async userCourse(@Parent() voucher: Voucher) {
        const { id } = voucher;
        return this.voucherService.findUserCourseById(id);
    }
    

    @Scopes('create')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async issueVoucher(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.voucherService.issueVoucher(id);
            return { id: data.id, message: 'Success create voucher' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('create')
    @RolesGuard('user')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async redeemVoucher(
        @AuthUser() auth: Auth,
        @Args({ name: 'code', type: () => String }) code: string,
    ) {
        try {
            const data = await this.voucherService.redeemVoucher(auth, code);
            return { id: data.id, message: 'Success create voucher' };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('update')
    @RolesGuard('admin')
    @UseGuards(Oauth2Guard)
    @Mutation(returns => ResponseMutation)
    async deactiveVoucher(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            const data = await this.voucherService.deactiveVoucher(id);
            return { id: data.id, message: 'Success deactive voucher' };
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
