import { Module } from '@nestjs/common';
import { VoucherResolver } from './voucher.resolver';
import { VoucherService } from './voucher.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Voucher } from '../../../entities/voucher.entity';
import { Transactions } from '../../../entities/transaction.entity';
import { UsersCourse } from '../../../entities/users-course.entity';
import { Users } from '../../../entities/users.entity';

@Module({
    providers: [VoucherResolver, VoucherService],
    imports: [
        TypeOrmModule.forFeature([Voucher, Transactions, UsersCourse, Users])
    ],
})
export class VoucherModule { }
