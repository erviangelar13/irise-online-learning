import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { Voucher } from "../../../entities/voucher.entity";


@InputType()
export class VoucherFilter {

    @Field({ nullable: true})
    dateFrom: Date;

    @Field({ nullable: true})
    dateTo: Date;
}

@ObjectType()
export class VoucherPagination extends PaginationPage {
    @Field(type => [Voucher])
    data: Voucher[];
}
