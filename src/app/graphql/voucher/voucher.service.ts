import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { Voucher } from '../../../entities/voucher.entity';
import { Transactions } from '../../../entities/transaction.entity';
import { UsersCourse } from '../../../entities/users-course.entity';
import { Users } from '../../../entities/users.entity';
import { VoucherFilter } from './voucher.dto';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { dateMoment } from '../../../helpers/date.helper';
import { Auth } from '../../../helpers/auth.helper';
import { generateRandomCode, generatePrefix } from '../../../helpers/generate.helper';

@Injectable()
export class VoucherService {

    constructor(
        @InjectRepository(Voucher) private voucherRepo: Repository<Voucher>,
        @InjectRepository(Transactions) private transactionRepo: Repository<Transactions>,
        @InjectRepository(UsersCourse) private userCourseRepo: Repository<UsersCourse>,
        @InjectRepository(Users) private userRepo: Repository<Users>,
    ) { }

    async find(filter: VoucherFilter, paginationOptions: PaginationOptions) {
        try {
            const list = this.voucherRepo.createQueryBuilder('voucher')
                .leftJoin(
                    'voucher.orders',
                    'orders'
                )
                .leftJoin(
                    'orders.transactions',
                    'transaction'
                )
                .where(
                    `LOWER(transaction.invoiceNo) LIKE :search OR LOWER(voucher.code) LIKE :search OR LOWER(orders.orderNo) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                );
            return await list.pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findList() {
        try {
            return await this.voucherRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.voucherRepo.findOne(id);
            if (!data) throw new Error(`Master category with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findInvoiceNoById(id: number) {
        try {
            const data = await this.voucherRepo.findOne(id, { relations: ['orders','orders.transactions']});
            if (!data) throw new Error(`Master category with id = ${id} is not found`);
            return data.orders.transactions[0].invoiceNo;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderNoById(id: number) {
        try {
            const data = await this.voucherRepo.findOne(id, { relations: ['orders']});
            if (!data) throw new Error(`Voucher with id = ${id} is not found`);
            return data.orders.orderNo;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderById(id: number) {
        try {
            const data = await this.voucherRepo.findOne(id, { relations: ['orders', 'orders.transactions', 'orders.course']});
            if (!data) throw new Error(`Voucher with id = ${id} is not found`);
            return data.orders;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findUserById(id: number) {
        try {
            const data = await this.userRepo.findOne(id, { relations: ['usersCourse']});
            if (!data) throw new Error(`User with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findUserCourseById(id: number) {
        try {
            const data = await this.voucherRepo.findOne(id, { relations: ['userCourse']});
            if (!data) throw new Error(`Voucher with id = ${id} is not found`);
            return data.userCourse;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async issueVoucher(id: number) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            
            const data = await this.transactionRepo.findOne(id, { relations: ['orders']});
            if (!data) throw new Error(`Transaction with id = ${id} is not found`);

            const vouchers: Voucher[] = [];
            for (let index = 0; index < data.orders.qty; index++) {
                const voucher = new Voucher();
                voucher.code = await generateRandomCode(Voucher, `VC${dateMoment().format('ss')}`, 10, queryRunner);
                voucher.orders = data.orders;
                voucher.activeDate = dateMoment().toDate();
                voucher.invitationCode = await generateRandomCode(Transactions, `${data.transactionCode}_${dateMoment().format('ss')}`, 25, queryRunner);
                vouchers.push(voucher);
            }
            await queryRunner.manager.save(vouchers);
            await queryRunner.commitTransaction();
            
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async deactiveVoucher(id: number) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const data = await this.voucherRepo.findOne(id);
            if (!data) throw new Error(`Voucher with id = ${id} is not found`);
            if(data.redeemDate != null)throw new Error(`Voucher is already used`);
            data.active = false;
            await queryRunner.manager.save(data);

            await queryRunner.commitTransaction();
            
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async redeemVoucher(auth: Auth, code: string) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const where = {
                code: code,
            };
            const data = await this.voucherRepo.findOne(where, { relations: ['orders','orders.course']});
            if (!data) throw new Error(`Voucher with code = ${code} is not found`);
            if(data.redeemDate != null)throw new Error(`Voucher is already used`);

            const user = await this.userRepo.findOne(auth.id);
            
            const where2 = {
                course: data.orders.course,
                user: user
            }
            // console.log(where2);
            const userCourse = await this.userCourseRepo.findOne(where2);
            // console.log(userCourse);
            if(userCourse) throw new Error(`Course with name = ${data.orders.course.name} is already exist`);
            
            var course = new UsersCourse();
            course.course = data.orders.course;
            course.user = user;
            course.courseCode = await generateRandomCode(Transactions, generatePrefix(data.orders.course.name) + `${dateMoment().format('MM')}`, 15, queryRunner);
            course.statusId = 1;
            await queryRunner.manager.save(course);

            data.redeemDate = dateMoment().toDate();
            data.userReedemId = auth.id;
            data.userCourse = course
            await queryRunner.manager.save(data);

            await queryRunner.commitTransaction();
            
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
