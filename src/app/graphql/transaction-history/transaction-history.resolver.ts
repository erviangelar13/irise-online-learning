import { Resolver, Query, Args, ResolveField, Parent } from '@nestjs/graphql';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { Transactions } from '../../../entities/transaction.entity';
import { Inject, UseGuards } from '@nestjs/common';
import { TransactionHistoryService } from './transaction-history.service';
import { Scopes } from '../../../decorators/scopes.decorator';
import { Oauth2Guard } from '../../../guards/guards';
import { TransactionHistoryPagination, TransactionHistoryFilter } from './transaction-history.dto';
import { PaginationInputType, createPaginationOptions } from '../../../helpers/pagination.helper';
import { RolesGuard } from '../../../decorators/roles.decorator';

@Resolver(of => TransactionHistory)
export class TransactionHistoryResolver {

    constructor(
        @Inject(TransactionHistoryService) private transactionHistoryService: TransactionHistoryService,
    ) { }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => TransactionHistoryPagination)
    async transactionHistoryList(
        @Args({ name: 'transactionFilter', type: () => TransactionHistoryFilter, nullable: true })
        transactionFilter: TransactionHistoryFilter,
        @Args({ name: 'pagination', type: () => PaginationInputType })
        paginationInputType: PaginationInputType,
    ) {
        try {
            const paginationOptions = createPaginationOptions(paginationInputType);
            return await this.transactionHistoryService.find(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => [TransactionHistory])
    async transactionHistories() {
        try {
            return await this.transactionHistoryService.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @Scopes('read')
    @UseGuards(Oauth2Guard)
    @Query(returns => TransactionHistory)
    async transactionHistory(
        @Args({ name: 'id', type: () => Number }) id: number,
    ) {
        try {
            return await this.transactionHistoryService.findById(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    @ResolveField(returns => String)
    async paymentStatus(@Parent() transactionHistory: TransactionHistory) {
        console.log(transactionHistory)
        const { paymentStatusId } = transactionHistory;
        return this.transactionHistoryService.findPaymentStatusById(paymentStatusId);
    }

    @ResolveField(returns => Transactions)
    async transaction(@Parent() transactionHistory: TransactionHistory) {
        console.log(transactionHistory)
        const { paymentStatusId } = transactionHistory;
        return this.transactionHistoryService.findTransactionById(paymentStatusId);
    }

}
