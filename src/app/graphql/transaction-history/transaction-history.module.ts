import { Module } from '@nestjs/common';
import { TransactionHistoryResolver } from './transaction-history.resolver';
import { TransactionHistoryService } from './transaction-history.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { Transactions } from '../../../entities/transaction.entity';
import { Orders } from '../../../entities/orders.entity';
import { MasterReference } from '../../../entities/master-reference.entity';
import { PaymentProvider } from '../../../entities/payment-provider.entity';

@Module({
    providers: [TransactionHistoryResolver, TransactionHistoryService],
    imports: [
        TypeOrmModule.forFeature([
            TransactionHistory,
            MasterReference,
            Transactions,
            Orders
        ]),
    ],
})
export class TransactionHistoryModule { }
