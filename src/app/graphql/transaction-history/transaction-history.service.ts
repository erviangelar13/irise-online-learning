import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { Transactions } from '../../../entities/transaction.entity';
import { PaginationOptions } from '../../../helpers/pagination.helper';
import { MasterReference } from '../../../entities/master-reference.entity';
import { Orders } from '../../../entities/orders.entity';
import { PaymentProvider } from '../../../entities/payment-provider.entity';
import { TransactionHistory } from '../../../entities/transaction-history.entity';
import { Voucher } from '../../../entities/voucher.entity';
import { dateMoment } from '../../../helpers/date.helper';
import { generateCode, generateInvoice, generateRandomCode } from '../../../helpers/generate.helper';
import { Config } from '../../../helpers/config.helper';
import { join } from 'path';
import * as fs from 'fs';
import { getMime } from '../../../helpers/mime.helper';

@Injectable()
export class TransactionHistoryService {

    constructor(
        @InjectRepository(Transactions) private transactionRepo: Repository<Transactions>,
        @InjectRepository(TransactionHistory) private transactionHistoryRepo: Repository<TransactionHistory>,
        @InjectRepository(MasterReference) private masterReferenceRepo: Repository<MasterReference>,
        @InjectRepository(Orders) private ordersRepo: Repository<Orders>,
    ) { }

    async find(paginationOptions: PaginationOptions) {
        try {
            return await this.transactionHistoryRepo.createQueryBuilder('history')
                .leftJoin(
                    'history.transaction',
                    'transactions'
                )
                .where(
                    `LOWER(transactions.invoiceNo) LIKE :search OR LOWER(transactions.transactionCode) LIKE :search`,
                    { search: '%' + paginationOptions.search + '%' },
                ).pagination(paginationOptions);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findAll() {
        try {
            return await this.transactionHistoryRepo.find();
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findById(id: number) {
        try {
            const data = await this.transactionHistoryRepo.findOne(id, { relations: ['order'] });
            if (!data) throw new Error(`Transaction with id = ${id} is not found`);
            return data;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findOrderById(id: number) {
        try {
            const data = await this.transactionHistoryRepo.findOne(id, { relations: ['order'] });
            return data.order;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findTransactionById(id: number) {
        try {
            const data = await this.transactionHistoryRepo.findOne(id, { relations: ['transaction'] });
            return data.transaction;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async findPaymentStatusById(idx: number) {
        try {
            const data = await this.masterReferenceRepo.findOne({ group: 'payment_status', idx: idx });
            if (!data) throw new Error(`Payment Statuss with idx = ${idx} is not found`);
            return data.name;
        } catch (error) {
            throw new Error(error.message);
        }
    }

}
