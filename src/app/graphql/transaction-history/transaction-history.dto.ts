import { ObjectType, Field, InputType } from "@nestjs/graphql";
import { PaginationPage } from "../../../helpers/pagination.helper";
import { TransactionHistory } from "../../../entities/transaction-history.entity";

@InputType()
export class TransactionHistoryFilter {

    @Field({ nullable: true })
    invoiceNo: number;

    @Field({ nullable: true })
    transactionCode: number;

    @Field({ nullable: true })
    dateFrom: Date;
    
    @Field({ nullable: true })
    dateTo: Date;
}

@ObjectType()
export class TransactionHistoryPagination extends PaginationPage {
    @Field(type => [TransactionHistory])
    data: TransactionHistory[];
}
