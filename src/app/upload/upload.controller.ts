import { Controller, Post, UseInterceptors, UploadedFiles, UseGuards, Body, Inject } from '@nestjs/common';
import { FilesInterceptor } from "@nestjs/platform-express";
import { diskStorage } from 'multer';
import { response, responseError } from '../../helpers/response.helper';
import { generateFilename, moveFile, removeFile, failedUpload, IUploadOptions } from '../../helpers/file.helper';
import { Oauth2Guard } from '../../guards/guards';
import { ContentService } from '../graphql/content/content.service';
import { ContentMediaInput } from '../graphql/content-media/content-media.dto';
import { ContentMediaService } from '../graphql/content-media/content-media.service';
import { PaymentMethodService } from '../graphql/payment-method/payment-method.service';
import { PaymentProviderService } from '../graphql/payment-provider/payment-provider.service';
import { CourseOrganizerService } from '../graphql/course-organizer/course-organizer.service';
import { CourseResourceInput } from '../graphql/course-resource/course-resource.dto';
import { CourseResourceService } from '../graphql/course-resource/course-resource.service';
import { CourseService } from '../graphql/course/course.service';

@Controller('upload')
export class UploadController {

    constructor(
        @Inject(CourseService) private courseService: CourseService,
        @Inject(CourseOrganizerService) private courseOrganizerService: CourseOrganizerService,
        @Inject(CourseResourceService) private courseResourceService: CourseResourceService,
        @Inject(ContentService) private contentService: ContentService,
        @Inject(ContentMediaService) private contentMediaService: ContentMediaService,
        @Inject(PaymentMethodService) private paymentMethodService: PaymentMethodService,
        @Inject(PaymentProviderService) private paymentProviderService: PaymentProviderService,
    ) { }

    @Post('course/cover')
    @UseGuards(Oauth2Guard)
    @UseInterceptors(FilesInterceptor('attachment', 1, {
        storage: diskStorage({
            destination: './uploads',
            filename: generateFilename,
        }),
    }))
    async uploadCourseCover(
        @UploadedFiles() attachment: IUploadOptions[],
        @Body() body: { id: number },
    ) {
        let success: boolean = true;
        let oldCoverPath: string = null;
        try {
            if (!attachment.length) throw new Error('Required attachment');
            const dto = Object.assign({}, body);
            oldCoverPath = await this.courseService.setCover(dto.id, attachment[0]);
            return response('Success set course cover');
        } catch (error) {
            success = false;
            return responseError(error.message);
        } finally {
            if (success) {
                if (oldCoverPath) removeFile(oldCoverPath);
                moveFile('course_cover', attachment[0].filename);
            } else {
                if (attachment.length) failedUpload(attachment[0].filename);
            }
        }
    }

    @Post('course-organizer/photo')
    @UseGuards(Oauth2Guard)
    @UseInterceptors(FilesInterceptor('attachment', 1, {
        storage: diskStorage({
            destination: './uploads',
            filename: generateFilename,
        }),
    }))
    async uploadCourseOrganizerPhoto(
        @UploadedFiles() attachment: IUploadOptions[],
        @Body() body: { id: number },
    ) {
        let success: boolean = true;
        let oldPhotoPath: string = null;
        try {
            if (!attachment.length) throw new Error('Required attachment');
            const dto = Object.assign({}, body);
            oldPhotoPath = await this.courseOrganizerService.setPhoto(dto.id, attachment[0]);
            return response('Success set course organizer photo');
        } catch (error) {
            success = false;
            return responseError(error.message);
        } finally {
            if (success) {
                if (oldPhotoPath) removeFile(oldPhotoPath);
                moveFile('course_organizer', attachment[0].filename);
            } else {
                if (attachment.length) failedUpload(attachment[0].filename);
            }
        }
    }

    @Post('course-resource/file')
    @UseGuards(Oauth2Guard)
    @UseInterceptors(FilesInterceptor('file', 1, {
        storage: diskStorage({
            destination: './uploads',
            filename: generateFilename,
        }),
    }))
    async createCourseResource(
        @UploadedFiles() file: IUploadOptions[],
        @Body() body: CourseResourceInput,
    ) {
        let success: boolean = true;
        try {
            if (!file.length) throw new Error('Required attachment');
            const dto = Object.assign({}, body);
            var data = await this.courseResourceService.createByLocalStorage(dto, file[0]);
            return response('Success create course resource', data);
        } catch (error) {
            success = false;
            return responseError(error.message);
        } finally {
            if (success) {
                moveFile('course_resource', file[0].filename);
            } else {
                if (file.length) failedUpload(file[0].filename);
            }
        }
    }

    @Post('content/banner')
    @UseGuards(Oauth2Guard)
    @UseInterceptors(FilesInterceptor('attachment', 1, {
        storage: diskStorage({
            destination: './uploads',
            filename: generateFilename,
        }),
    }))
    async uploadContentBanner(
        @UploadedFiles() attachment: IUploadOptions[],
        @Body() body: { id: number },
    ) {
        let success: boolean = true;
        let oldBannerPath: string = null;
        try {
            if (!attachment.length) throw new Error('Required attachment');
            oldBannerPath = await this.contentService.setBanner(body.id, attachment[0]);
            return response('Success set content banner');
        } catch (error) {
            success = false;
            return responseError(error.message);
        } finally {
            if (success) {
                if (oldBannerPath) removeFile(oldBannerPath);
                moveFile('content/banner', attachment[0].filename);
            } else {
                if (attachment.length) failedUpload(attachment[0].filename);
            }
        }
    }

    @Post('content/media')
    @UseGuards(Oauth2Guard)
    @UseInterceptors(FilesInterceptor('attachment', 1, {
        storage: diskStorage({
            destination: './uploads',
            filename: generateFilename,
        }),
    }))
    async uploadContentMedia(
        @UploadedFiles() attachment: IUploadOptions[],
        @Body() body: ContentMediaInput,
    ) {
        let success: boolean = true;
        try {
            if (!attachment.length) throw new Error('Required attachment');
            await this.contentMediaService.create(body, attachment[0]);
            return response('Success create content media');
        } catch (error) {
            success = false;
            return responseError(error.message);
        } finally {
            if (success) {
                moveFile('content/media', attachment[0].filename);
            } else {
                if (attachment.length) failedUpload(attachment[0].filename);
            }
        }
    }

    @Post('payment-method/logo')
    @UseGuards(Oauth2Guard)
    @UseInterceptors(FilesInterceptor('attachment', 1, {
        storage: diskStorage({
            destination: './uploads',
            filename: generateFilename,
        }),
    }))
    async uploadPaymentMethodLogo(
        @UploadedFiles() attachment: IUploadOptions[],
        @Body() body: { id: number },
    ) {
        let success: boolean = true;
        let oldLogoPath: string = null;
        try {
            if (!attachment.length) throw new Error('Required attachment');
            const dto = Object.assign({}, body);
            oldLogoPath = await this.paymentMethodService.setLogo(dto.id, attachment[0]);
            return response('Success set payment method logo');
        } catch (error) {
            success = false;
            return responseError(error.message);
        } finally {
            if (success) {
                if (oldLogoPath) removeFile(oldLogoPath);
                moveFile('payment_method', attachment[0].filename);
            } else {
                if (attachment.length) failedUpload(attachment[0].filename);
            }
        }
    }

    @Post('payment-provider/logo')
    @UseGuards(Oauth2Guard)
    @UseInterceptors(FilesInterceptor('attachment', 1, {
        storage: diskStorage({
            destination: './uploads',
            filename: generateFilename,
        }),
    }))
    async uploadPaymentProviderLogo(
        @UploadedFiles() attachment: IUploadOptions[],
        @Body() body: { id: number },
    ) {
        let success: boolean = true;
        let oldLogoPath: string = null;
        try {
            if (!attachment.length) throw new Error('Required attachment');
            const dto = Object.assign({}, body);
            oldLogoPath = await this.paymentProviderService.setLogo(dto.id, attachment[0]);
            return response('Success set payment provider logo');
        } catch (error) {
            success = false;
            return responseError(error.message);
        } finally {
            if (success) {
                if (oldLogoPath) removeFile(oldLogoPath);
                moveFile('payment_provider', attachment[0].filename);
            } else {
                if (attachment.length) failedUpload(attachment[0].filename);
            }
        }
    }
}
