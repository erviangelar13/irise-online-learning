import { Module } from '@nestjs/common';
import { UploadController } from './upload.controller';
import { UploadService } from './upload.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MasterReference } from '../../entities/master-reference.entity';
import { ContentService } from '../graphql/content/content.service';
import { Content } from '../../entities/content.entity';
import { ContentMediaService } from '../graphql/content-media/content-media.service';
import { ContentMedia } from '../../entities/content-media.entity';
import { PaymentMethodService } from '../graphql/payment-method/payment-method.service';
import { PaymentMethod } from '../../entities/payment-method.entity';
import { PaymentProviderService } from '../graphql/payment-provider/payment-provider.service';
import { PaymentProvider } from '../../entities/payment-provider.entity';
import { CourseOrganizer } from '../../entities/course-organizer.entity';
import { CourseOrganizerService } from '../graphql/course-organizer/course-organizer.service';
import { CourseResourceService } from '../graphql/course-resource/course-resource.service';
import { CourseResource } from '../../entities/course-resource.entity';
import { CourseMaterial } from '../../entities/course-material.entity';
import { Course } from '../../entities/course.entity';
import { CourseService } from '../graphql/course/course.service';
import { MasterType } from '../../entities/master-type.entity';
import { MasterCategory } from '../../entities/master-category.entity';
import { InstructorInformation } from '../../entities/instructor-information.entity';

@Module({
    controllers: [UploadController],
    providers: [
        UploadService,
        CourseService,
        CourseOrganizerService,
        CourseResourceService,
        ContentService,
        ContentMediaService,
        PaymentMethodService,
        PaymentProviderService,
    ],
    imports: [
        TypeOrmModule.forFeature([
            Course,
            CourseOrganizer,
            CourseResource,
            CourseMaterial,
            MasterReference,
            MasterType,
            MasterCategory,
            InstructorInformation,
            Content,
            ContentMedia,
            PaymentMethod,
            PaymentProvider,
        ]),
    ],
})
export class UploadModule { }
