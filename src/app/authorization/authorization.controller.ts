import { Controller, Post, Body, UseGuards, Req, Get, Res, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { responseError, response } from '../../helpers/response.helper';
import { AuthorizationDTO, RegisterDTO, VerifyOtpDTO, ForgotPasswordDTO, SetFcmDTO, SendOtpDTO, EmailExistDTO } from './authorization.dto';
import { AuthorizationService } from './authorization.service';
import { Response, Request } from 'express';
import { Oauth2Guard } from '../../guards/guards';
import { Auth } from '../../helpers/auth.helper';
import { RolesGuard } from '../../decorators/roles.decorator';
import { Scopes } from '../../decorators/scopes.decorator';
import { Config } from '../../helpers/config.helper';

@Controller('authorization')
export class AuthorizationController {

    constructor(
        private authorizationService: AuthorizationService,
    ) { }

    @Get('login/google')
    @UseGuards(AuthGuard('google'))
    googleLogin() {
        return;
    }

    @Get('login/google/redirect')
    @UseGuards(AuthGuard('google'))
    async googleRedirect(@Req() req: any, @Res({ passthrough: true }) resp: Response) {
        const dataLogin = await this.authorizationService.loginGoogle(req.user);
        const oauth2Client = {
            clientId: Config.get('OAUTH2_CLIENT_ID'),
            clientSecret: Config.get('OAUTH2_CLIENT_SECRET'),
        };
        const dto: AuthorizationDTO = {
            username: dataLogin.username,
            password: dataLogin.password,
            rememberMe: true,
        };
        const data = await this.authorizationService.login(oauth2Client, dto);
        resp.cookie('ACCESS-TOKEN-BEEFSTEAK', data.accessToken);
        resp.cookie('REMEMBER-ME-BEEFSTEAK', 'yes');
        resp.cookie('REFRESH-TOKEN-BEEFSTEAK', data.refreshToken);
        resp.redirect(`${Config.get('REDIRECT_URL')}`);
    }

    @Post('login')
    @UseGuards(AuthGuard('basic'))
    async login(@Req() req: any, @Body() dto: AuthorizationDTO, @Res({ passthrough: true }) resp: Response) {
        try {
            const oauth2Client = {
                clientId: req.clientId,
                clientSecret: req.clientSecret,
            };
            const data = await this.authorizationService.login(oauth2Client, dto);
            resp.cookie('ACCESS-TOKEN-BEEFSTEAK', data.accessToken);
            if (dto.rememberMe) {
                resp.cookie('REMEMBER-ME-BEEFSTEAK', 'yes');
                resp.cookie('REFRESH-TOKEN-BEEFSTEAK', data.refreshToken);
            } else {
                resp.cookie('REMEMBER-ME-BEEFSTEAK', 'no');
            }
            return response('Success login', data);
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Post('register')
    @UseGuards(AuthGuard('basic'))
    async register(@Body() dto: RegisterDTO) {
        try {
            const data = await this.authorizationService.register(dto);
            return response('Success register user', data);
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Post('send/otp')
    async sendOtp(@Body() dto: SendOtpDTO) {
        try {
            await this.authorizationService.sendOtp(dto);
            return response('Success send otp');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Post('verify/otp')
    async verifyOtp(@Body() dto: VerifyOtpDTO) {
        try {
            await this.authorizationService.verifyOtp(dto);
            return response(`Congratulation, account has been verified`);
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('info')
    async info(@Req() req: Request, @Res({ passthrough: true }) resp: Response) {
        try {
            const token = req.headers['authorization'].split(' ');
            // console.log(token)
            var data = await this.authorizationService.info(token[1]);
            return response('Success info', data);
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('logout')
    async logout(@Req() req: Request, @Res({ passthrough: true }) resp: Response) {
        try {
            const token = req.cookies['ACCESS-TOKEN-BEEFSTEAK'];
            await this.authorizationService.logout(token);
            resp.cookie('ACCESS-TOKEN-BEEFSTEAK', '');
            resp.cookie('REFRESH-TOKEN-BEEFSTEAK', '');
            resp.cookie('REMEMBER-ME-BEEFSTEAK', '');
            return response('Success logout');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Post('email-exist')
    async emailExist(@Body() dto: EmailExistDTO) {
        try {
            await this.authorizationService.isEmailExist(dto.email);
            return response('Email is exist.');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Post('forgot-password')
    async forgotPassword(@Body() dto: ForgotPasswordDTO) {
        try {
            await this.authorizationService.forgotPassword(dto);
            return response('We have send verify link to your email.');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('verify/forgot-password/:id')
    async verifyForgotPassword(@Param('id') id: string) {
        try {
            await this.authorizationService.verifyForgotPassword(id);
            return response('Success change password, you can login now');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Post('set-fcm')
    async setUserFcm(@Req() req: any, @Body() dto: SetFcmDTO) {
        try {
            const auth: Auth = req.auth;
            await this.authorizationService.setUserFcm(auth, dto.fcm);
            return response('Success');
        } catch (error) {
            return responseError(error.message);
        }
    }

    @Get('me')
    @Scopes('read')
    @RolesGuard('admin', 'user')
    @UseGuards(Oauth2Guard)
    async auth(@Req() req: any): Promise<any> {
        // console.log(req.user);
        const auth: Auth = req.auth;
        // console.log(auth)
        return { message: 'hello' };
    }
}
