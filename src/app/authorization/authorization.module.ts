import { Module } from '@nestjs/common';
import { AuthorizationService } from './authorization.service';
import { AuthorizationController } from './authorization.controller';
import { OAuth2Strategy } from '../../strategies/oauth2.strategy';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccessTokenEntity } from 'nestjs-oauth2-server';
import { GoogleStrategy } from '../../strategies/google.strategy';
import { BasicStrategy } from '../../strategies/basic.strategy';

@Module({
    providers: [AuthorizationService, OAuth2Strategy, GoogleStrategy, BasicStrategy],
    imports: [TypeOrmModule.forFeature([AccessTokenEntity])],
    controllers: [AuthorizationController]
})
export class AuthorizationModule { }
