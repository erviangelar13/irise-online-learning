import { Injectable, Inject, ForbiddenException } from '@nestjs/common';
import { AuthorizationDTO, RegisterDTO, VerifyOtpDTO, ForgotPasswordDTO, SendOtpDTO } from './authorization.dto';
import { Users } from '../../entities/users.entity';
import { ClientRepositoryInterface, Oauth2GrantStrategyRegistry, AccessTokenEntity } from 'nestjs-oauth2-server';
import { Config } from '../../helpers/config.helper';
import { MailerService } from '@nestjs-modules/mailer';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection, QueryRunner } from 'typeorm';
import { generateCode, generateOtp } from '../../helpers/generate.helper';
import { dateMoment } from '../../helpers/date.helper';
import { UserHasRoles } from '../../entities/user-has-roles.entity';
import { Roles } from '../../entities/roles.entity';
import { UserValidation } from '../../entities/user-validation.entity';
import { Otp } from '../../entities/otp.entity';
import { Auth } from '../../helpers/auth.helper';
import { UserForgotPassword } from '../../entities/user-forgot-password.entity';
import { UserFcm } from '../../entities/user-fcm.entity';
import { MailService } from '../mail/mail.service';
import { Int } from '@nestjs/graphql';

@Injectable()
export class AuthorizationService {

    constructor(
        @Inject('ClientRepositoryInterface')
        private readonly clientRepository: ClientRepositoryInterface,
        @InjectRepository(AccessTokenEntity) private accessTokenRepo: Repository<AccessTokenEntity>,
        private readonly strategyRegistry: Oauth2GrantStrategyRegistry,
        private readonly mailerService: MailerService,
    ) { }

    async loginGoogle(user: any) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            const email = user.email;
            const name = `${user.firstName} ${user.lastName}`;

            const usersRepo = queryRunner.manager.getRepository(Users);

            let userData = await usersRepo.findOne({ email: email });
            if (!userData) {
                const newUserData = new Users();
                newUserData.code = await generateCode(Users, 'USR', 10, queryRunner);
                newUserData.email = email;
                newUserData.name = name;
                newUserData.isActive = true;
                newUserData.isEmailVerified = true;
                await queryRunner.manager.save(newUserData);

                const roles: Roles[] = await queryRunner.manager.getRepository(Roles).find();
                const rolesData: UserHasRoles[] = [];

                const filterRoleUser = roles.filter(fItem => {
                    return fItem.name === 'user';
                });
                const roleUser = new UserHasRoles();
                roleUser.user = newUserData;
                roleUser.role = filterRoleUser.length ? filterRoleUser[0] : null;
                rolesData.push(roleUser);
                await queryRunner.manager.save(rolesData);

                userData = newUserData;
            }

            const resultData = userData;
            await queryRunner.commitTransaction();

            return {
                username: resultData && resultData.username ? resultData.username : email,
                password: resultData && resultData.password ? resultData.password : '',
            };
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async login(oauth2Client: { clientId: string, clientSecret: string }, dto: AuthorizationDTO) {
        try {
            const request = {
                grantType: 'password',
                clientId: oauth2Client.clientId,
                clientSecret: oauth2Client.clientSecret,
                username: dto.username,
                password: dto.password,
            };

            const client = await this.clientRepository.findByClientId(request.clientId);
            const where = {
                isActive: true,
            };
            if (dto.password) {
                where['email'] = dto.username;
                where['password'] = dto.password;
            } else {
                where['email'] = dto.username;
            }
            const userData = await Users.findOne(where);

            const whereRole = {
                user: userData,
            };
            const role = await UserHasRoles.find({
                where: whereRole,
                relations: ['role']
            }).then((value) => {
                return value.map(x => { return x.role.name })
            });
            Object.assign(request, { scopes: JSON.parse(client.scope) });
            if (!await this.strategyRegistry.validate(request, client)) {
                throw new ForbiddenException("You are not allowed to access the given resource");
            }

            const oauth2Response = await this.strategyRegistry.getOauth2Response(request, client);
            return {
                accessToken: oauth2Response.accessToken,
                refreshToken: oauth2Response.refreshToken,
                role: role,
                verified: userData.isEmailVerified || userData.isPhoneVerified
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async info(token: string) {
        try {
            const where = {
                accessToken: token,
            }
            var accessToken = await this.accessTokenRepo.findOne(where);
            // console.log(accessToken);
            const wheres = {
                id: parseInt(accessToken.userId),
            }
            const userData = await Users.findOne(wheres);
            return {
                name: userData.name,
                avatar: ''
            };
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async register(dto: RegisterDTO) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const usersRepo = queryRunner.manager.getRepository(Users);

            // Start ==> Check username, email, and phone exist

            const emailIsExist = await usersRepo.findOne({ email: dto.email });
            if (emailIsExist) throw new Error('Email already exist');

            const userIsExist = await usersRepo.findOne({ username: dto.username });
            if (userIsExist) throw new Error('Username already exist');

            const phoneIsExist = await usersRepo.findOne({ phone: dto.phone });
            if (phoneIsExist) throw new Error('Phone number already exist');
            // End ==> Check username, email, and phone exist

            // Start ==> Prepare save user data
            const userData = new Users();
            userData.code = await generateCode(Users, 'USR', 10);
            userData.isActive = true;
            userData.email = dto.email;
            userData.name = dto.name;
            userData.phone = dto.phone;
            userData.username = dto.username;
            userData.password = dto.password;
            await queryRunner.manager.save(userData);
            // End ==> Prepare save user data

            // Start ==> Prepare save user roles data
            const roles: Roles[] = await queryRunner.manager.getRepository(Roles).find();
            const rolesData: UserHasRoles[] = [];

            const filterRoleUser = roles.filter(fItem => {
                return fItem.name === 'user';
            });
            const roleUser = new UserHasRoles();
            roleUser.user = userData;
            roleUser.role = filterRoleUser.length ? filterRoleUser[0] : null;
            rolesData.push(roleUser);
            await queryRunner.manager.save(rolesData);
            // End ==> Prepare save user roles data

            await queryRunner.commitTransaction();

            const param = new SendOtpDTO();
            param.to = dto.email;
            param.isEmail = true;

            this.sendOtp(param);

            return {
                id: userData.id,
                username: userData.username,
            };
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async sendOtp(dto: SendOtpDTO) {
        const queryRunner = getConnection().createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {

            const usersRepo = queryRunner.manager.getRepository(Users);
            const userValidationRepo = queryRunner.manager.getRepository(UserValidation);
            // const otpRepo = queryRunner.manager.getRepository(Otp);

            const otpType = dto.isEmail ? 'email' : 'phone';

            const userData = await usersRepo.createQueryBuilder()
                .where(`${otpType} = :to`, { to: dto.to })
                .getOne();

            if (!userData) throw new Error('User not found');

            const field = dto.isEmail ? 'is_email_confirmation' : 'is_phone_confirmation';

            let userValidationData = await userValidationRepo.createQueryBuilder()
                .where('user_id = :userId', {
                    userId: userData.id,
                })
                .andWhere(`${field} = True`)
                .getOne();

            if (!userValidationData) {
                userValidationData = await this.createOTP(userData, otpType, queryRunner);
            } else {
                // if (!userValidationData.active) throw new Error('OTP code already activated');

                if (userValidationData.validationTime > dateMoment().toDate()) {
                    // console.log('A')
                    userValidationData.validationTime = dateMoment().add(5, 'minutes').toDate();
                    await queryRunner.manager.save(userValidationData);
                    // await otpRepo.createQueryBuilder()
                    //     .update()
                    //     .set({
                    //         isActive: true,
                    //     })
                    //     .where('otp = :otpCode', { otpCode: userValidationData.otp })
                    //     .execute();
                } else {
                    // console.log('B')
                    const newOtpCode = await generateOtp();
                    userValidationData.otp = newOtpCode;
                    userValidationData.validationTime = dateMoment().add(5, 'minutes').toDate();
                    await queryRunner.manager.save(userValidationData);

                    const otpData = new Otp();
                    otpData.otp = newOtpCode;
                    otpData.isActive = true;
                    await queryRunner.manager.save(otpData);
                }
            }

            // console.log(dto)
            // console.log(userData)
            // console.log(userValidationData)
            if (otpType === 'email') {
                const mailService = new MailService(this.mailerService);
                await mailService.sendEmail(
                    dto.to,
                    'OTP Code',
                    'register',
                    {
                        name: userData.name,
                        otpCode: userValidationData.otp,
                    },
                );
            }


            await queryRunner.commitTransaction();

            return userValidationData.validationTime;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async verifyOtp(dto: VerifyOtpDTO) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            const usersRepo = queryRunner.manager.getRepository(Users);
            const userValidationRepo = queryRunner.manager.getRepository(UserValidation);
            const otpRepo = queryRunner.manager.getRepository(Otp);

            const userValidationData = await userValidationRepo.createQueryBuilder()
                .where('user_id = :userId AND otp = :otpCode', {
                    userId: dto.userId,
                    otpCode: dto.otpCode,
                })
                .getOne();

            if (!userValidationData) throw new Error('Otp code is not match');
            if (!userValidationData.active) throw new Error('OTP code not valid');
            if (userValidationData.validationTime < dateMoment().toDate()) throw new Error('Otp code is expired, please resend otp code');

            userValidationData.active = false;
            await queryRunner.manager.save(userValidationData);

            await otpRepo.createQueryBuilder()
                .update()
                .set({
                    isActive: false,
                })
                .where('otp = :otpCode', { otpCode: dto.otpCode })
                .execute();

            const set = {
                isActive: true,
            };
            if (userValidationData.isEmailConfirmation) {
                set['isEmailVerified'] = true;
            } else {
                set['isPhoneVerified'] = true;
            }
            await usersRepo.createQueryBuilder()
                .update()
                .set(set)
                .where('id = :userId', { userId: dto.userId })
                .execute();

            await queryRunner.commitTransaction();

            return true;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async logout(token: string) {
        try {
            await this.accessTokenRepo.createQueryBuilder()
                .delete()
                .where('access_token = :token', { token })
                .execute();

            return true;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async isEmailExist(email: string) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            const usersRepo = queryRunner.manager.getRepository(Users);
            const user = await usersRepo.findOne({ email: email });
            if (!user) throw new Error(`Account with email '${email}' is not exist`)

            await queryRunner.commitTransaction();

            return true;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async forgotPassword(dto: ForgotPasswordDTO) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            if (dto.password !== dto.reTypePassword) throw new Error('Password and Retype password not match');

            const userRepo = queryRunner.manager.getRepository(Users);
            const userForgotPwdRepo = queryRunner.manager.getRepository(UserForgotPassword);

            const userData = await userRepo.findOne({ email: dto.email });
            if (!userData) throw new Error('Email account is not found');

            const checkUserForgotPwdData = await userForgotPwdRepo.createQueryBuilder()
                .where('user_id = :userId AND validation_time > now()', {
                    userId: userData.id,
                })
                .getOne();

            const userForgotPwdData = new UserForgotPassword();
            if (!checkUserForgotPwdData) {
                userForgotPwdData.password = dto.password;
                userForgotPwdData.validationTime = dateMoment().add(5, 'minutes').toDate();
                userForgotPwdData.user = userData;
                await userForgotPwdRepo.save(userForgotPwdData);
            }

            const id = checkUserForgotPwdData ? checkUserForgotPwdData.id : userForgotPwdData.id;

            const mailService = new MailService(this.mailerService);
            await mailService.sendEmail(
                dto.email,
                'Request Forgot Password',
                'forgot-password',
                {
                    name: userData.name,
                    link: `${Config.get('BASE_URL')}authorization/verify/forgot-password/${id}`,
                },
            );

            await queryRunner.commitTransaction();

            return true;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async verifyForgotPassword(id: string) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            const userRepo = queryRunner.manager.getRepository(Users);
            const userForgotPwdRepo = queryRunner.manager.getRepository(UserForgotPassword);
            const userForgotPwdData = await userForgotPwdRepo.findOne(id, { relations: ['user'] });

            if (!userForgotPwdData) throw new Error('Invalid link verify');
            if (userForgotPwdData.validationTime < dateMoment().toDate()) throw new Error('link is expired');

            const userData = userForgotPwdData.user;
            userData.password = userForgotPwdData.password;
            await userRepo.save(userData);

            await queryRunner.commitTransaction();

            return true;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    async setUserFcm(auth: Auth, fcm: string) {
        const queryRunner = getConnection().createQueryRunner();

        try {
            await queryRunner.connect();
            await queryRunner.startTransaction();

            const usersRepo = queryRunner.manager.getRepository(Users);
            const userFcmRepo = queryRunner.manager.getRepository(UserFcm);

            const userData = await usersRepo.findOne(auth.id);

            const checkFcm = await userFcmRepo.findOne({ fcm: fcm });
            if (checkFcm) {
                checkFcm.user = userData;
                await userFcmRepo.save(checkFcm);
            } else {
                const userFcmData = new UserFcm();
                userFcmData.fcm = fcm;
                userFcmData.user = userData;
                await userFcmRepo.save(userFcmData);
            }

            await queryRunner.commitTransaction();

            return true;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new Error(error.message);
        } finally {
            await queryRunner.release();
        }
    }

    private async createOTP(userData: Users, otpType: 'email' | 'phone', queryRunner: QueryRunner) {
        try {
            const otpCode = await generateOtp();
            const userValidationData = new UserValidation();
            userValidationData.isEmailConfirmation = otpType === 'email' ? true : false;
            userValidationData.isPhoneConfirmation = otpType === 'phone' ? true : false;
            userValidationData.otp = otpCode;
            userValidationData.validationTime = dateMoment().add(5, 'minutes').toDate();
            userValidationData.user = userData;
            await queryRunner.manager.save(userValidationData);

            const otpData = new Otp();
            otpData.otp = otpCode;
            otpData.isActive = true;
            await queryRunner.manager.save(otpData);

            return userValidationData;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
