import { IsNotEmpty } from "class-validator";

export enum Gender {
    MALE = "male",
    FEMALE = "female",
}

export class AuthorizationDTO {
    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    rememberMe: boolean;
}

export class RegisterDTO {
    // @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    phone: string;
}

export class SendOtpDTO {
    @IsNotEmpty()
    to: string;

    @IsNotEmpty()
    isEmail: boolean;
}

export class VerifyOtpDTO {
    @IsNotEmpty()
    userId: number;

    @IsNotEmpty()
    otpCode: string;
}

export class EmailExistDTO {
    @IsNotEmpty()
    email: string;
}

export class ForgotPasswordDTO {
    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    reTypePassword: string;
}

export class SetFcmDTO {
    @IsNotEmpty()
    fcm: string;
}
