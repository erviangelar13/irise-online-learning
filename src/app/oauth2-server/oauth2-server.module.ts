import { Module } from '@nestjs/common';
import { UserLoader } from './utils/user-loader.util';
import { UserValidator } from './utils/user-validator.util';
import { Oauth2Module } from 'nestjs-oauth2-server';
import { getConnectionToken } from '@nestjs/typeorm';

@Module({
    controllers: [],
    providers: [],
    imports: [Oauth2Module.forRoot({
        userLoader: new UserLoader(),
        userValidator: new UserValidator(),
        connection: getConnectionToken('default') as string,
    })]
})
export class Oauth2ServerModule { }
