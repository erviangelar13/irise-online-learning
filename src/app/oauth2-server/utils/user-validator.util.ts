import { Injectable } from "@nestjs/common";
import { UserValidatorInterface, UserInterface, InvalidUserException } from "nestjs-oauth2-server";
import { Users } from "../../../entities/users.entity";

@Injectable()
export class UserValidator implements UserValidatorInterface {
    async validate(username: string, password: string): Promise<UserInterface> {
        const where = {
            isActive: true,
        };
        if (password) {
            if(username.indexOf("@") !== -1){
                where['email'] = username;
            } else {
                where['username'] = username;
            }
            where['password'] = password;
        } else {
            where['email'] = username;
        }
        const userData = await Users.findOne(where);
        if (!userData) throw InvalidUserException.withUsernameAndPassword(username, password);
        if (!password && userData.password) throw InvalidUserException.withUsernameAndPassword(username, password);
        return {
            id: userData.id.toString(),
            username: userData.username,
            email: userData.email,
        };
    }
}
