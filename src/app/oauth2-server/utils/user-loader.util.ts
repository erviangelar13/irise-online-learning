import { Injectable } from "@nestjs/common";
import { UserLoaderInterface, UserInterface, InvalidUserException } from "nestjs-oauth2-server";
import { Users } from "../../../entities/users.entity";

@Injectable()
export class UserLoader implements UserLoaderInterface {
    async load(userId: string): Promise<UserInterface> {
        const userData = await Users.findOne(userId);
        if (!userData) throw InvalidUserException.withId(userId);
        return {
            id: userId,
            username: userData.username,
            email: userData.email,
        };
    }
}
