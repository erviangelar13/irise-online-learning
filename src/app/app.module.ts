import { Module, NestModule, MiddlewareConsumer, Logger } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { ScheduleModule } from '@nestjs/schedule';
import '../helpers/query.helper';
import { ConfigModule } from '../config/config.module';
import { DatabaseModule } from '../config/database/database.module';
import { Config } from '../helpers/config.helper';
import { IndexController } from '../index.controller';
import { Oauth2ServerModule } from './oauth2-server/oauth2-server.module';
import { AuthorizationModule } from './authorization/authorization.module';
import { GraphqlModule } from './graphql/graphql.module';
import { UploadModule } from './upload/upload.module';
import { ViewModule } from './view/view.module';
import { MailModule } from './mail/mail.module';

const cronLists: any[] = [];
if (Config.getBoolean('IS_CRON_ACTIVE')) {
    Logger.log('Cron is running', 'AppModule');
    cronLists.push();
} else {
    Logger.log('Cron is not running', 'AppModule');
}


@Module({
    imports: [
        ConfigModule,
        DatabaseModule,
        ScheduleModule.forRoot(),
        MulterModule.register({
            dest: './uploads',
            limits: {
                fileSize: 104857600, // 100MB works as expected
                files: 10,
            }
        }),
        Oauth2ServerModule,
        AuthorizationModule,
        GraphqlModule,
        UploadModule,
        ViewModule,
        MailModule
    ],
    controllers: [
        IndexController,
    ],
    providers: [
        ...cronLists,
    ]
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply()
            .exclude()
            .forRoutes();
    }
}
