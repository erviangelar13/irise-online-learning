import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Config } from "../../helpers/config.helper";
import { join } from 'path';

@Injectable()
export class MailService {

    constructor(
        private readonly _mailerService: MailerService,
    ) { }

    async sendEmail(sendTo: string, subject: string, template: string, context: any = {}) {
        try {
            var appMode = Config.get('APP_MODE');
            // console.log(this._mailerService)
            const pathTemplate = appMode === 'dev' ? join(__dirname, `../../../src/templates/email-templates/${template}`) : join(__dirname, `../../../src/email-templates/${template}`);
            const send = await this._mailerService.sendMail({
                to: sendTo,
                subject: subject,
                template: pathTemplate,
                context: context,
            });

            return send;
        } catch (error) {
            throw new Error(error.message);
        }

    }
}
