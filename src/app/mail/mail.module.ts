import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { Config } from '../../helpers/config.helper';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { join } from 'path';

@Module({
  imports:[
    MailerModule.forRoot({
      transport: {
          // service: 'gmail',
          host: Config.get('EMAIL_HOST'),
          port: Config.get('EMAIL_PORT'),
          secure: false,
          auth: {
              user: Config.get('EMAIL_MAIL'),
              pass: Config.get('EMAIL_PASS')
          },
          tls: {
            rejectUnauthorized: false
          }
      },
      defaults: {
          from: '"No Reply" <no-reply@imadani.id>',
      },
      template: {
          dir: join(__dirname, `./templates/`),
          adapter: new HandlebarsAdapter(),
          options: {
              strict: true,
          },
      },
    })
  ],
  providers: [MailService]
})
export class MailModule {}
