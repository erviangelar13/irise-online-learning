import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '../config.module';
import { ConfigService } from '../config.service';
import { ClientEntity, AccessTokenEntity } from 'nestjs-oauth2-server';

/**
 * Install dependencies: npm install --save @nestjs/typeorm typeorm mysql
 */
@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (config: ConfigService) => {
                return {
                    type: 'postgres',
                    host: config.get('DB_HOST'),
                    port: Number(config.get('DB_PORT')),
                    username: config.get('DB_USERNAME'),
                    password: config.get('DB_PASSWORD'),
                    database: config.get('DB_DATABASE'),
                    entities: [__dirname + '/../../entities/*{.ts,.js}', ClientEntity, AccessTokenEntity],
                    logging: config.getBoolean('DB_LOGGING'),
                    synchronize: true,
                };
            },
        }),
    ],
})
export class DatabaseModule { }
