import { dateMoment } from "./date.helper";
import { Config } from "./config.helper";
import { join } from 'path';
import * as fs from 'fs';

export interface IUploadOptions {
    fieldname: string;
    originalname: string;
    encoding: string;
    mimeType: string;
    destination: string;
    filename: string;
    path: string;
    size: number;
}

export const generateFilename = (req: any, file: any, callback: any) => {
    const originalname: string = file.originalname;
    const ext = originalname.substring(originalname.lastIndexOf('.') + 1, originalname.length);

    const formatDate = dateMoment().format('YYYYMMDD'),
        formatTime = dateMoment().format('HHmmss');

    callback(null, `${formatDate}_${formatTime}.${ext}`);
};

export const moveFile = async (path: string, filename: string) => {
    try {
        const appMode = Config.get('APP_MODE'),
            pathFolderTemp = appMode === 'dev' ? '../../uploads/' : '../../uploads/',
            pathCurrentFile = join(__dirname, `${pathFolderTemp}${filename}`),
            pathReal = `${Config.get('BASE_PATH_FILE')}${path}/`,
            pathRealFile =  join(pathReal, filename);

        const rawPathLs = path.split('/');
        let rawPathURL = Config.get('BASE_PATH_FILE');
        for (const fd of rawPathLs) {
            rawPathURL += fd;
            fs.mkdirSync(rawPathURL, { recursive: true })
            rawPathURL += '/';
        }
        console.log(pathCurrentFile)
        console.log(pathRealFile)
        if (fs.existsSync(pathCurrentFile)) {
            // var file = fs.readFileSync(pathCurrentFile);
            fs.writeFileSync(pathRealFile, fs.readFileSync(pathCurrentFile));
            // fs.renameSync(pathCurrentFile, pathRealFile);
        }

        return true;
    } catch (error) {
        throw new Error(error.message);
    }
};

export const removeFile = async (path: string, filename: string = '') => {
    const pathReal = `${Config.get('BASE_PATH_FILE')}${path}/`;
    const fullPath = join(pathReal, filename);
    if (fs.existsSync(fullPath)) {
        fs.unlinkSync(fullPath);
    }

    return true;
}

export const failedUpload = async (filename: string) => {
    const appMode = Config.get('APP_MODE'),
        pathFolderTemp = appMode === 'dev' ? '../../uploads/' : '../uploads/',
        pathCurrentFile = join(__dirname, `${pathFolderTemp}${filename}`);

    if (fs.existsSync(pathCurrentFile)) {
        fs.unlinkSync(pathCurrentFile);
    }

    return true;
}
