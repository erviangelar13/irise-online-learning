import { EntityManager, getConnection, getManager, QueryRunner } from "typeorm";
import { UserValidation } from "../entities/user-validation.entity";
import { dateMoment } from './date.helper';
import { Otp } from "../entities/otp.entity";

export const generateCode = async (entity: any, prefix: string, maxLength: number = 10, queryRunner: QueryRunner = null) => {
    try {
        let manager: EntityManager;
        if (queryRunner) {
            manager = queryRunner.manager;
        } else {
            manager = getManager();
        }
        
        let result = prefix;
        const currentTotalData = await manager.createQueryBuilder(entity, 'a')
            .getCount();
        const lastId = currentTotalData + 1;
        for (let i = 0; i < (maxLength - prefix.length - lastId.toString().length); i++) {
            result = `${result}0`;
        }
        result = `${result}${lastId.toString()}`;
        return result;
    } catch (error) {
        throw new Error(error.message);
    }
}

export const generateRandomCode = async (entity: any, prefix: string, maxLength: number = 10, queryRunner: QueryRunner = null) => {
    try {
        let manager: EntityManager;
        if (queryRunner) {
            manager = queryRunner.manager;
        } else {
            manager = getManager();
        }
        
        let result = prefix;
        const currentTotalData = await manager.createQueryBuilder(entity, 'a')
            .getCount();
        const lastId = currentTotalData + 1;
        result = result + generateCharacter((maxLength - prefix.length - lastId.toString().length));
        result = `${result}${lastId.toString()}`;
        return result;
    } catch (error) {
        throw new Error(error.message);
    }
}

export const generateInvoice = async (entity: any, prefix: string, maxLength: number = 10, queryRunner: QueryRunner = null) => {
    try {
        let manager: EntityManager;
        if (queryRunner) {
            manager = queryRunner.manager;
        } else {
            manager = getManager();
        }
        console.log(dateMoment().format('MM'));
        const month = romanize(dateMoment().format('MM'));
        let result = `${prefix}/${dateMoment().format('yy')}/${month}/`;
        const currentTotalData = await manager.createQueryBuilder(entity, 'a')
            .where('date >= :after', { after: dateMoment().startOf('month').format('YYYY-MM-DD') })
            .andWhere('date < :before', { before:  dateMoment().endOf('month').format('YYYY-MM-DD') })
            .getCount();
        const lastId = currentTotalData + 1;
        for (let i = 0; i < (maxLength - prefix.length - lastId.toString().length); i++) {
            result = `${result}0`;
        }

        result = `${result}${lastId.toString()}`;
        return result;
    } catch (error) {
        throw new Error(error.message);
    }
}

export const generateOtp = async () => {
    try {
        const userValidationRepo = getConnection().manager.getRepository(UserValidation);
        const otpRepo = getConnection().manager.getRepository(Otp);

        const validationExpired = await userValidationRepo
            .createQueryBuilder()
            .where('validation_time < CURRENT_TIMESTAMP')
            .andWhere('active = False')
            .getMany();

        const otpCode: string[] = [];
        for (const item of validationExpired) {
            otpCode.push(item.otp);
        }

        if (otpCode.length) {
            await otpRepo.createQueryBuilder()
                .update()
                .set({
                    isActive: false,
                })
                .where('otp IN (:...otpCode)', { otpCode: otpCode })
                .execute();
        }

        let otp: string = '';
        let otpValid: boolean = false;
        do {
            otp = generateRandomNumber(6);
            const otpData = await otpRepo.createQueryBuilder()
                .where('otp = :otp AND is_active = True', { otp: otp })
                .getOne();
            otpValid = otpData ? false : true;
        }
        while (otpValid === false);

        return otp;
    } catch (error) {
        throw new Error(error.message);
    }
}

export const generateRandomNumber = (length: number) => {
    const genNumber = generateRandom(length, '123456789');
    return genNumber;
}

export const generateCharacter = (length: number) => {
    const genChar = generateRandom(length, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
    return genChar;
}

export const generateRandom = (length: number, characters: string) => {
    let genRandom = '';
    for (let i = 0; i < length; i++) {
        genRandom += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return genRandom;
}

export const generatePrefix = (name: string) => {
    var code = '';
    const words = name.split(' ');
    words.forEach(word => {
        code += word.slice(1, 1).toUpperCase();
    });
    return code;
}

function romanize (num) {
    if (isNaN(num))
        return NaN;
    var digits = String(+num).split(""),
        key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
               "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
               "","I","II","III","IV","V","VI","VII","VIII","IX"],
        roman = "",
        i = 3;
    while (i--)
        roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    return Array(+digits.join("") + 1).join("M") + roman;
}
