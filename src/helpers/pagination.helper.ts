import { InputType, ObjectType, Field } from "@nestjs/graphql";

@InputType()
export class PaginationInputType {
    @Field()
    page?: number;

    @Field()
    limit?: number;

    @Field({ nullable: true })
    search?: string;

    @Field({ nullable: true })
    order?: string;
}

@ObjectType()
export class PaginationPage {

    @Field()
    total: number;

    @Field()
    totalPage: number;
}

export const createPaginationOptions = (dto: PaginationInputType) => {
    let page = dto.page || 1;
    let limit = dto.limit || 10;
    let orderBy = '';
    let orderValue: 'ASC' | 'DESC' = 'ASC';
    const pagination = new PaginationOptions();

    if (page <= 0) { page = 1; }
    if (limit <= 0) { limit = 10; }

    if (dto.order && dto.order.indexOf(':') >= 0) {
        const order = dto.order.split(':');
        orderBy = order[0];//.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);
        orderValue = order[1].toUpperCase() as ('ASC' | 'DESC');
    }

    pagination.page = page;
    pagination.limit = limit;
    pagination.queryPage = (page - 1) * limit;
    pagination.search = dto.search.toLowerCase() || '';
    pagination.orderBy = orderBy;
    pagination.orderValue = orderValue;

    return pagination;
};

export class PaginationOptions {
    page: number;
    limit: number;
    queryPage: number;
    search: string;
    orderBy: string;
    orderValue: 'ASC' | 'DESC';
}
