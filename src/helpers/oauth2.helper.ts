import Axios from 'axios';
import { Config } from './config.helper';

export const getRequest = async (
    grantType: 'client_credentials' | 'refresh_token' | 'password',
    scopes: string[],
    body: { username?: string, password?: string, refreshToken?: string },
) => {
    try {
        const HttpClient = Axios.create();
        const rawBody = {
            grantType,
            clientId: Config.get('OAUTH2_CLIENT_ID'),
            clientSecret: Config.get('OAUTH2_CLIENT_SECRET'),
            scopes,
            ...body,
        };
        const req = await HttpClient.post(`${Config.get('BASE_URL')}oauth2/token`, rawBody);
        return req.data;
    } catch (error) {
        if (body.username && body.password) {
            throw new Error(`The user with username '${body.username}' and password '${body.password}' was not found`);
        } else {
            throw new Error('Token expired');
        }
    }
}
