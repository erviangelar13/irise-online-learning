export const getMime = (path: string) => {
    const mime = require('mime');
    return mime.getType(path);
}
