import { HttpException, HttpStatus } from '@nestjs/common';
import { ObjectType, Field } from '@nestjs/graphql';
import { Timestamp } from 'typeorm';

export class Response {
    code: HttpStatus;
    message: string;
    data: any;
    timestamp: string;

    constructor(message: string, data: any, code: HttpStatus = HttpStatus.OK, timestamp: string = Date().toLocaleString()) {
        this.message = message;
        this.data = data;
        this.code = code;
        this.timestamp = timestamp;
    }
}

export class ResponseDatatable {
    data: any[];
    total: number;

    constructor(data: any[], total: number) {
        this.data = data;
        this.total = total;
    }
}

export const response = (message: string, data: any = null) => new Response(message, data);

export const responseDatatable = (data: any[]) => new ResponseDatatable(data[0], data[1]);

// export const responsePage = (results: any[], total: number, paginationOptions: PaginationOptions) => {
//     return new Pagination(results, total, paginationOptions);
// };

export const responseError = (message: string) => {
    let errorCode: number = 400;
    let errorCodeIndex: number = 0;
    const usingErrorcode = message.includes('#EC');

    if (usingErrorcode) {
        errorCodeIndex = message.indexOf('#EC');
        errorCode = Number(message.substring(errorCodeIndex + 3));
    }
    const finalMessage = (!usingErrorcode) ? message : message.substring(0, errorCodeIndex).trim();
    return Promise.reject(new HttpException({ message: finalMessage }, errorCode));
};

@ObjectType()
export class ResponseMutation {
    @Field({ nullable: true })
    id: number;

    @Field()
    message: string;
}
