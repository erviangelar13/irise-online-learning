import * as Jimp from 'jimp';
import * as fs from 'fs';
import { join } from 'path';
import { Config } from './config.helper';

export class Certificate {
    course?: string;

    name?: string;

    date?: string;

    order?: string;
}

export const generateCertificate = async (dto: Certificate, path: string, filename: string, newname: string = "") => {
    try {
        const appMode = Config.get('APP_MODE'),
            pathFolderTemp = appMode === 'dev' ? '../../src/templates/certificate-templates/' : '../../uploads/',
            pathCurrentFile = join(__dirname, `${pathFolderTemp}${filename}`),
            pathReal = `${Config.get('BASE_PATH_FILE')}${path}/`,
            pathRealFile =  join(pathReal, `${newname}.png`);

        const rawPathLs = path.split('/');
        let rawPathURL = Config.get('BASE_PATH_FILE');
        for (const fd of rawPathLs) {
            rawPathURL += fd;
            fs.mkdirSync(rawPathURL, { recursive: true })
            rawPathURL += '/';
        }
        console.log(pathCurrentFile)
        console.log(pathRealFile)
        const mediumFont = await Jimp.loadFont(Jimp.FONT_SANS_64_BLACK);
        const bigFont = await Jimp.loadFont(Jimp.FONT_SANS_128_BLACK);
        const smallFont = await Jimp.loadFont(Jimp.FONT_SANS_32_BLACK);
        const image = await Jimp.read(pathCurrentFile);
        image.print(
            mediumFont,
            630,
            600,
            {
              text: dto.course,
              alignmentX: Jimp.HORIZONTAL_ALIGN_LEFT,
              alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE,
            },
            600,
            50
          );
        image.print(bigFont, 630, 820, {
            text: dto.name,
            alignmentX: Jimp.HORIZONTAL_ALIGN_LEFT,
            alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE,
          }, 1500, 50);
        image.print(smallFont, 1490, 1290, {
            text: dto.date,
            alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
            alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE,
        }, 250, 50);
        image.write(pathRealFile)

        return {
            path: pathReal,
            extension: image.getExtension()
        };
    } catch (error) {
        throw new Error(error.message);
    }
};