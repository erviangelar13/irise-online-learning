import * as moment from 'moment';

export const dateMoment = (dateRaw: string = null) => {
    return dateRaw ? moment(new Date(dateRaw)) : moment();
};
