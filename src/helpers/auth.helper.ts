import { getConnection } from "typeorm";
import { Users } from "../entities/users.entity";
import { Roles } from "../entities/roles.entity";

export async function getAuth(
    userId: number,
): Promise<Auth> {
    const roles: Roles[] = [];
    const userRepo = getConnection().manager.getRepository(Users);
    const userData = await userRepo.findOne(userId, { relations: ['instructor', 'userHasRoles', 'userHasRoles.role'] });
    let isAdmin: boolean = false;
    for (const item of userData.userHasRoles) {
        if (item.role.id === 1) {
            isAdmin = true;
        }
        roles.push(item.role);
    }

    const id = userData.id,
        name = userData.name,
        username = userData.username,
        email = userData.email,
        phone = userData.phone,
        instructorId = userData.instructor ? userData.instructor.id : null;

    return new Auth(
        id,
        name,
        username,
        email,
        phone,
        roles,
        instructorId,
        isAdmin,
    );
}

export class Auth {
    id: any;
    name: string;
    username: string;
    email: string;
    phone: string;
    roles: Roles[];
    instructorId: number;
    isAdmin: boolean;

    constructor(
        id: any,
        name: string,
        username: string,
        email: string,
        phone: string,
        roles: Roles[],
        instructorId: number,
        isAdmin: boolean,
    ) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.roles = roles;
        this.instructorId = instructorId;
        this.isAdmin = isAdmin;
    }
}
