import { SelectQueryBuilder } from "typeorm/query-builder/SelectQueryBuilder";
import { PaginationOptions } from "./pagination.helper";

//Declaration Merging Of Module.
declare module 'typeorm/query-builder/SelectQueryBuilder' {
    interface SelectQueryBuilder<Entity> {
        pagination(this: SelectQueryBuilder<Entity>, paginationOptions: PaginationOptions): Promise<{ data: Entity[], total: number, totalPage: number }>
    }
}

SelectQueryBuilder.prototype.pagination = async function <Entity>(this: SelectQueryBuilder<Entity>, paginationOptions: PaginationOptions): Promise<{ data: Entity[], total: number, totalPage: number }> {
    try {
        const [data, total] = await this.skip(paginationOptions.queryPage)
            .take(paginationOptions.limit)
            .orderBy(paginationOptions.orderBy, paginationOptions.orderValue)
            .getManyAndCount();

        return {
            data: data,
            total: total,
            totalPage: Math.ceil(total / paginationOptions.limit),
        };
    } catch (error) {
        throw new Error(error.message);
    }
}
