import { Auth } from "./auth.helper";

export const isRole = (auth: Auth, includes: string[]) => {
    let result: boolean = false;
    const roles = auth.roles;
    for (const role of includes) {
        const filterRole = roles.filter(fRole => {
            return fRole.name === role;
        });
        if (filterRole.length) {
            result = true;
            break;
        }
    }
    return result;
}
