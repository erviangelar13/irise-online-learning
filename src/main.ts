import { ApplicationContext } from './app/app.context';
import { TransformInterceptor } from './interceptors/transform.interceptor';
import { UndefinedInterceptor } from './interceptors/undefined.interceptor';
import { TimeoutInterceptor } from './interceptors/timeout.interceptor';
import { ValidationPipe } from './pipes/validation.pipe';
import { ConfigService } from './config/config.service';
import * as cookieParser from 'cookie-parser';
import { json } from 'body-parser';

async function bootstrap() {
    const app = await ApplicationContext();
    app.enableCors({
        origin: true,
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
        credentials: true,
    });
    app.use(cookieParser());
    app.use(json({ limit: '50mb' }));
    // app.use(bodyParser.urlencoded({limit: '5000mb', extended: true}));
    app.useGlobalPipes(new ValidationPipe());
    app.useGlobalInterceptors(new TransformInterceptor());
    app.useGlobalInterceptors(new UndefinedInterceptor());
    app.useGlobalInterceptors(new TimeoutInterceptor());
    app.listen(app.get(ConfigService).getInt('APP_PORT'), '0.0.0.0');
    // app.listen(app.get(ConfigService).getInt('APP_PORT'), '0.0.0.0', () => {
    //     console.log('Listening to port:  ' + app.get(ConfigService).getInt('APP_PORT'));
    // });
}
bootstrap();
