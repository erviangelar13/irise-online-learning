import { BasicStrategy as Strategy } from 'passport-http';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { getConnection } from "typeorm";
import { ClientEntity } from 'nestjs-oauth2-server';

@Injectable()
export class BasicStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super({
            passReqToCallback: true
        });
    }

    async validate(req: any, username: string, password: string): Promise<boolean> {
        const clientRepo = getConnection().manager.getRepository(ClientEntity);
        const access = await clientRepo.findOne({ clientId: username, clientSecret: password });
        if (!access) throw new UnauthorizedException();
        req.clientId = username;
        req.clientSecret = password;
        return true;
    }
}
