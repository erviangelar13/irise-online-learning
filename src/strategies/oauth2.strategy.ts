import { Strategy } from 'passport-http-bearer';
import { PassportStrategy } from '@nestjs/passport';
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { AccessTokenRepositoryInterface, Oauth2PayloadInterface, ClientPayload } from 'nestjs-oauth2-server';

@Injectable()
export class OAuth2Strategy extends PassportStrategy(Strategy, 'oauth2') {
    constructor(
        @Inject('AccessTokenRepositoryInterface')
        private readonly accessTokenRepository: AccessTokenRepositoryInterface,
    ) {
        super();
    }

    async validate(bearer: string): Promise<Oauth2PayloadInterface> {
        const accessToken = await this.accessTokenRepository.findByAccessToken(bearer);
        if (!accessToken) throw new UnauthorizedException();
        return new ClientPayload(
            accessToken,
            accessToken.client.id,
            accessToken.client.clientId,
            accessToken.client.name);
    }
}
