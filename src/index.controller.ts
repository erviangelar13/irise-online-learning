import { Controller, Get } from '@nestjs/common';

@Controller()
export class IndexController {
    @Get()
    async getIndex() {
        return { status: 'BeefSteak Server', uptime: process.uptime() };
    }
}
