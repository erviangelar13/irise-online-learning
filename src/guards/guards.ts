import { Injectable, ExecutionContext, Inject, UnauthorizedException, ForbiddenException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { AccessTokenRepositoryInterface, ClientRepositoryInterface, Oauth2GrantStrategyRegistry } from "nestjs-oauth2-server";
import { getAuth } from "../helpers/auth.helper";
import { Reflector } from "@nestjs/core";
import { getConnection } from "typeorm";
import { Users } from "../entities/users.entity";
import { GqlContextType, GqlExecutionContext } from "@nestjs/graphql";

@Injectable()
export class Oauth2Guard extends AuthGuard('oauth2') {

    constructor(
        @Inject('AccessTokenRepositoryInterface')
        private readonly accessTokenRepository: AccessTokenRepositoryInterface,
        @Inject('ClientRepositoryInterface')
        private readonly clientRepository: ClientRepositoryInterface,
        private readonly strategyRegistry: Oauth2GrantStrategyRegistry,
        private reflector: Reflector,
    ) {
        super();
    }

    async canActivate(context: ExecutionContext) {
        const scopes: string[] = this.reflector.get<string[]>('scopes', context.getHandler());
        const rolesGuard: string[] = this.reflector.get<string[]>('rolesGuard', context.getHandler());
        let activate = true;
        let request = null;
        let response = null;
        let headerToken: string = '';

        if (context.getType() === 'http') {
            // activate = (await super.canActivate(context)) as boolean;
            const contextHTTP = context.switchToHttp();
            request = contextHTTP.getRequest();
            response = contextHTTP.getResponse();

            headerToken = request.header('Authorization');
        } else if (context.getType<GqlContextType>() === 'graphql') {
            const ctx = GqlExecutionContext.create(context);
            request = ctx.getContext().req;
            response = ctx.getContext().res;

            headerToken = request.headers.authorization;
        }

        if (headerToken) {
            headerToken = headerToken.substr(7, headerToken.length);
        }

        const token = request.cookies['ACCESS-TOKEN-BEEFSTEAK'] || headerToken;
        const accessToken = await this.accessTokenRepository.findByAccessToken(token);

        const client = await this.clientRepository.findByClientId(accessToken.client.clientId);
        if (scopes && scopes.length) {
            const scopesClient: string[] = JSON.parse(client.scope);
            let canAccess: boolean = false;
            for (const scope of scopes) {
                const checkScope = scopesClient.filter(item => {
                    return item === scope || item === 'all';
                });
                // console.log(checkScope)
                if (checkScope.length) {
                    canAccess = true;
                    break;
                }
            }
            if (!canAccess) throw new ForbiddenException('You are not allowed to access the given resource');
        }

        const userRepo = getConnection().manager.getRepository(Users);
        const userId = Number(accessToken.userId);
        const userData = await userRepo.findOne(userId, { relations: ['userHasRoles', 'userHasRoles.role'] });

        const isAdmin = userData.userHasRoles.filter(item => {
            return item.role.name === 'admin';
        }).length ? true : false;

        if (!isAdmin && rolesGuard && rolesGuard.length) {
            let canAccess: boolean = false;
            for (const role of rolesGuard) {
                const checkRole = userData.userHasRoles.filter(item => {
                    return item.role.name === role;
                });
                if (checkRole.length) {
                    canAccess = true;
                    break;
                }
            }
            if (!canAccess) throw new ForbiddenException('You are not allowed to access the given resource');
        }

        if (accessToken.accessTokenExpiresAt < new Date(Date.now())) {
            const isRememberMe = request.cookies['REMEMBER-ME-BEEFSTEAK'] && request.cookies['REMEMBER-ME-BEEFSTEAK'] === 'yes' ? true : false;
            if (isRememberMe) {
                if (accessToken.refreshTokenExpiresAt < new Date(Date.now())) throw new UnauthorizedException();
                const refreshToken = request.cookies['REFRESH-TOKEN-BEEFSTEAK'] ? request.cookies['REFRESH-TOKEN-BEEFSTEAK'] : accessToken.refreshToken;
                const body = {
                    grantType: 'refresh_token',
                    clientId: accessToken.client.clientId,
                    clientSecret: accessToken.client.clientSecret,
                    refreshToken: refreshToken,
                };
                // console.log(client)
                if (!await this.strategyRegistry.validate(body, client)) {
                    console.log("Failed Refresh")
                    throw new ForbiddenException('You are not allowed to access the given resource');
                }

                const oauth2Response = await this.strategyRegistry.getOauth2Response(body, client);
                response.cookie('ACCESS-TOKEN-BEEFSTEAK', oauth2Response.accessToken);
                response.cookie('REFRESH-TOKEN-BEEFSTEAK', oauth2Response.refreshToken);
            } else {
                throw new UnauthorizedException();
            }
        }

        request.auth = await getAuth(Number(accessToken.userId));
        return activate;
    }
}
