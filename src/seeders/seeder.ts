import { Injectable } from "@nestjs/common";
import { SeedersService } from "./seeders.service";

@Injectable()
export class Seeder {
    constructor(
        private readonly seederService: SeedersService,
    ) { }

    async seed() {
        await this.seederService.createOauthClient().then((isCreated: boolean) => {
            if (isCreated) console.log('Some oauth client is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed oauth client with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        await this.seederService.createRoles().then((isCreated: boolean) => {
            if (isCreated) console.log('Some roles is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed roles with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        await this.seederService.createMasterReference().then((isCreated: boolean) => {
            if (isCreated) console.log('Some master reference is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed master reference with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        await this.seederService.createUsers().then((isCreated: boolean) => {
            if (isCreated) console.log('Some users is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed users with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        await this.seederService.createUserHasRoles().then((isCreated: boolean) => {
            if (isCreated) console.log('Some user has roles is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed user has roles with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        await this.seederService.createMasterCategory().then((isCreated: boolean) => {
            if (isCreated) console.log('Some master category is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed master category with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        await this.seederService.createMasterType().then((isCreated: boolean) => {
            if (isCreated) console.log('Some master type is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed master type with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        await this.seederService.createCourseOrganizer().then((isCreated: boolean) => {
            if (isCreated) console.log('Some course organizer is create');
        }).catch(error => {
            console.log('====================================');
            console.log('Failed seed course organizer with error: ');
            console.log(error.message);
            console.log('====================================');
        });

        return true;
    }
}
