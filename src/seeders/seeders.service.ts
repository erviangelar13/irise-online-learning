import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager } from 'typeorm';
import { Roles } from '../entities/roles.entity';
import {
    oauthClientData,
    rolesData,
    masterReferenceData,
    usersData,
    userHasRolesData,
    masterCategoryData,
    masterTypeData,
    courseOrganizerData,
} from './data.seeders';
import * as moment from 'moment';
import { MasterReference } from '../entities/master-reference.entity';
import { Users } from '../entities/users.entity';
import { UserHasRoles } from '../entities/user-has-roles.entity';
import { MasterCategory } from '../entities/master-category.entity';
import { MasterType } from '../entities/master-type.entity';
import { CourseOrganizer } from '../entities/course-organizer.entity';


@Injectable()
export class SeedersService {

    constructor(
        @InjectRepository(Roles) private readonly rolesRepo: Repository<Roles>,
        @InjectRepository(MasterReference) private readonly masterReferenceRepo: Repository<MasterReference>,
        @InjectRepository(Users) private readonly usersReferenceRepo: Repository<Users>,
        @InjectRepository(UserHasRoles) private readonly userHasRolesRepo: Repository<UserHasRoles>,
        @InjectRepository(MasterCategory) private readonly masterCategoryRepo: Repository<MasterCategory>,
        @InjectRepository(MasterType) private readonly masterTypeRepo: Repository<MasterType>,
        @InjectRepository(CourseOrganizer) private readonly courseOrganizerRepo: Repository<CourseOrganizer>,
    ) { }

    async createOauthClient() {
        try {
            const builder = getManager().createQueryBuilder();
            const entityManager = getManager();
            let isCreate: boolean = false;
            for (const item of oauthClientData) {
                const exist = await builder
                    .select('a.id')
                    .from('gb_oauth_client', 'a')
                    .where('a.id = :id', { id: item.id })
                    .getRawOne();

                if (!exist) {
                    await entityManager.query(
                        `INSERT INTO gb_oauth_client (
                            id,
                            name,
                            client_id,
                            client_secret,
                            grants,
                            scope,
                            access_token_lifetime,
                            refresh_token_lifetime,
                            "privateKey",
                            "publicKey",
                            cert,
                            cert_expires_at
                        ) VALUES (
                            '${item.id}',
                            '${item.name}',
                            '${item.clientId}',
                            '${item.clientSecret}',
                            '${item.grants}',
                            '${item.scope}',
                            ${item.accessTokenLifetime},
                            ${item.refreshTokenLifetime},
                            '${item.privateKey}',
                            '${item.publicKey}',
                            '${item.cert}',
                            '${moment(item.certExpiresAt).format('YYYY-MM-DD HH:mm')}'
                        )`
                    );
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createRoles() {
        try {
            let isCreate: boolean = false;
            for (const item of rolesData) {
                const exist = await this.rolesRepo.findOne({ id: item.id });
                if (!exist) {
                    await this.rolesRepo.save(item);
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createMasterReference() {
        try {
            let isCreate: boolean = false;
            for (const item of masterReferenceData) {
                const exist = await this.masterReferenceRepo.findOne({ group: item.group, idx: item.idx });
                if (!exist) {
                    await this.masterReferenceRepo.save(item);
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createUsers() {
        try {
            let isCreate: boolean = false;
            for (const item of usersData) {
                const exist = await this.usersReferenceRepo.findOne({ username: item.username });
                if (!exist) {
                    const users = new Users();
                    users.id = item.id;
                    users.code = item.code;
                    users.email = item.email;
                    users.isActive = item.isActive;
                    users.isEmailVerified = item.isEmailVerified;
                    users.isPhoneVerified = item.isPhoneVerified;
                    users.name = item.name;
                    users.phone = item.phone;
                    users.username = item.username;
                    users.password = item.password;
                    await this.usersReferenceRepo.save(users);
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createUserHasRoles() {
        try {
            const entityManager = getManager();
            let isCreate: boolean = false;
            for (const item of userHasRolesData) {
                const exist = await this.userHasRolesRepo.findOne({ id: item.id });
                if (!exist) {
                    await entityManager.query(
                        `INSERT INTO user_has_roles ( id, user_id, role_id )
                        VALUES (
                            ${item.id},
                            ${item.userId},
                            ${item.roleId}
                        )`
                    );
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createMasterCategory() {
        try {
            let isCreate: boolean = false;
            for (const item of masterCategoryData) {
                const exist = await this.masterCategoryRepo.findOne({ id: item.id });
                if (!exist) {
                    const masterCategory: MasterCategory = new MasterCategory();
                    masterCategory.id = item.id;
                    masterCategory.name = item.name;
                    masterCategory.description = item.description;
                    await this.masterCategoryRepo.save(masterCategory);
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createMasterType() {
        try {
            let isCreate: boolean = false;
            for (const item of masterTypeData) {
                const exist = await this.masterTypeRepo.findOne({ id: item.id });
                if (!exist) {
                    const masterType: MasterType = new MasterType();
                    masterType.id = item.id;
                    masterType.name = item.name;
                    masterType.description = item.description;
                    await this.masterTypeRepo.save(masterType);
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }

    async createCourseOrganizer() {
        try {
            let isCreate: boolean = false;
            for (const item of courseOrganizerData) {
                const exist = await this.courseOrganizerRepo.findOne({ id: item.id });
                if (!exist) {
                    const courseOrganizer: CourseOrganizer = new CourseOrganizer();
                    courseOrganizer.id = item.id;
                    courseOrganizer.name = item.name;
                    courseOrganizer.organizationTypeId = item.organizationTypeId;
                    courseOrganizer.description = item.description;
                    await this.courseOrganizerRepo.save(courseOrganizer);
                    isCreate = true;
                }
            }
            return isCreate;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
