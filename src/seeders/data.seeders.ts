import { ClientEntity } from "nestjs-oauth2-server";
import { Roles } from "../entities/roles.entity";
import { MasterReference } from "../entities/master-reference.entity";
import { MasterCategory } from "../entities/master-category.entity";
import { MasterType } from "../entities/master-type.entity";
import { CourseOrganizer } from "../entities/course-organizer.entity";

export const oauthClientData: ClientEntity[] = [
    {
        id: '6d1dbd71-fac9-4001-b102-abec374e3554',
        name: 'beefsteak-server',
        clientId: 'c8535817-1f01-4ffb-8c44-ab710d5621f2-beefsteak-server',
        clientSecret: 'Ihk9qccM7EKv7C5Fa+qAnye0nm64YWtOPTos2MbDpQU=',
        grants: ['client_credentials', 'refresh_token', 'password'],
        scope: '["create", "read", "update", "delete"]',
        accessTokenLifetime: 3600,
        refreshTokenLifetime: 7200,
        privateKey: 'privatekey',
        publicKey: 'publickey',
        cert: 'cert',
        certExpiresAt: new Date('2099-12-31 00:00:00'),
        createdAt: new Date(),
        deletedAt: null,
    }
];

export const rolesData: Roles[] = [
    { id: 1, name: 'admin', description: 'Administrator' },
    { id: 2, name: 'operator', description: 'Operator' },
    { id: 3, name: 'merchant', description: 'Merchant' },
    { id: 4, name: 'institution', description: 'Institution Admin' },
    { id: 5, name: 'user', description: 'User' },
]

export const masterReferenceData: MasterReference[] = [
    { group: 'gender', idx: 1, name: 'Male', description: '' },
    { group: 'gender', idx: 2, name: 'Female', description: '' },
    { group: 'access_type', idx: 1, name: 'Montly', description: '' },
    { group: 'access_type', idx: 2, name: 'Yearly', description: '' },
    { group: 'access_type', idx: 3, name: 'Unlimited', description: '' },
    { group: 'file_type', idx: 1, name: 'PDF', description: '' },
    { group: 'file_type', idx: 2, name: 'Video', description: '' },
    { group: 'file_type', idx: 3, name: 'Image', description: '' },
    { group: 'content_type', idx: 1, name: 'Information', description: '' },
    { group: 'content_type', idx: 2, name: 'News', description: '' },
    { group: 'promo_type', idx: 1, name: 'Percentage', description: '' },
    { group: 'promo_type', idx: 2, name: 'Value', description: '' },
    { group: 'promo_category', idx: 1, name: 'Promo Category 1', description: '' },
    { group: 'promo_category', idx: 2, name: 'Promo Category 1', description: '' },
    { group: 'promo_value_type', idx: 1, name: 'Percentage', description: '' },
    { group: 'promo_value_type', idx: 2, name: 'Value', description: '' },
    { group: 'organization_type', idx: 1, name: 'Group', description: '' },
    { group: 'organization_type', idx: 2, name: 'Partner', description: '' },
    { group: 'organization_type', idx: 3, name: 'Commercial', description: '' },
    { group: 'language', idx: 1, name: 'Indonesia', description: '' },
    { group: 'language', idx: 2, name: 'English', description: '' },
    { group: 'material_type', idx: 1, name: 'E-book', description: '' },
    { group: 'material_type', idx: 2, name: 'Video', description: '' },
    { group: 'material_type', idx: 3, name: 'Document', description: '' },
    { group: 'course_status', idx: 1, name: 'Draft', description: '' },
    { group: 'course_status', idx: 2, name: 'Publish', description: '' },
    { group: 'resource_type', idx: 1, name: 'Youtube', description: '' },
    { group: 'resource_type', idx: 2, name: 'Local Storage', description: '' },
    { group: 'resource_type', idx: 3, name: 'External Storage', description: '' },
    { group: 'order_type', idx: 1, name: 'Personal', description: '' },
    { group: 'order_type', idx: 2, name: 'Institution', description: '' },
    { group: 'order_status', idx: 1, name: 'Pending', description: '' },
    { group: 'order_status', idx: 2, name: 'Completed', description: '' },
    { group: 'order_status', idx: 3, name: 'Cancelled', description: '' },
]

interface IUsers {
    id: number;
    code: string;
    email: string;
    isActive: boolean;
    isEmailVerified: boolean;
    isPhoneVerified: boolean;
    name: string;
    phone: string;
    username: string;
    password: string;
}
export const usersData: IUsers[] = [
    {
        id: 1,
        code: 'USR0000100',
        email: 'beefsteak.adm@mailinator.com',
        isActive: true,
        isEmailVerified: true,
        isPhoneVerified: false,
        name: 'Administrator',
        phone: '081200000000',
        username: 'admin',
        password: 'admin'
    },
]

interface IUserHasRoles {
    id: number;
    userId: number;
    roleId: number;
}
export const userHasRolesData: IUserHasRoles[] = [
    { id: 1, userId: 1, roleId: 1 },
]

export const masterCategoryData: MasterCategory[] = [
    { id: 3, name: 'Kepribadian', description: '' },
    { id: 4, name: 'Sosial', description: '' },
    { id: 5, name: 'Profesional', description: '' },
    { id: 6, name: 'Pedagogy', description: '' },
]

export const masterTypeData: MasterType[] = [
    { id: 1, name: 'Blendid Learning', description: '' },
    { id: 2, name: 'Webminar', description: '' },
    { id: 3, name: 'Elektronik Book', description: '' },
]

export const courseOrganizerData: CourseOrganizer[] = [
    { id: 1, name: 'CAS-GTN', organizationTypeId: 1, description: '' },
    { id: 2, name: 'Gramedia', organizationTypeId: 2, description: '' },
    { id: 3, name: 'Brain Academy', organizationTypeId: 3, description: '' },
]
