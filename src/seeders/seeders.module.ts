import { Module } from '@nestjs/common';
import { SeedersService } from './seeders.service';
import { DatabaseModule } from '../config/database/database.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Roles } from '../entities/roles.entity';
import { Seeder } from './seeder';
import { MasterReference } from '../entities/master-reference.entity';
import { Users } from '../entities/users.entity';
import { UserHasRoles } from '../entities/user-has-roles.entity';
import { MasterCategory } from '../entities/master-category.entity';
import { MasterType } from '../entities/master-type.entity';
import { CourseOrganizer } from '../entities/course-organizer.entity';

@Module({
    imports: [
        DatabaseModule,
        TypeOrmModule.forFeature([
            Roles,
            MasterReference,
            Users,
            UserHasRoles,
            MasterCategory,
            MasterType,
            CourseOrganizer,
        ]),
    ],
    providers: [SeedersService, Seeder]
})
export class SeedersModule { }
