import { NestFactory } from "@nestjs/core";
import { SeedersModule } from "./seeders.module";
import { Seeder } from "./seeder";

async function bootstrap() {
    NestFactory.createApplicationContext(SeedersModule)
        .then(appContext => {
            const seeder = appContext.get(Seeder);

            console.log('===== Start Seeding =====');
            seeder
                .seed()
                .then(() => {
                    console.log('===== End Seeding =====');
                })
                .finally(() => appContext.close());
        })
        .catch(error => {
            throw error;
        });
}
bootstrap();
