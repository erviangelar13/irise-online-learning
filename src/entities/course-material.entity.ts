import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { CourseModul } from "./course-modul.entity";
import { CourseResource } from "./course-resource.entity";
import { UsersCourseMaterial } from "./users-course-material.entity";

@Entity('course_material')
@ObjectType()
export class CourseMaterial {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 512, default: '' })
    @Field()
    name: string;

    @ManyToOne(() => CourseModul, courseModul => courseModul.courseMaterial)
    @JoinColumn({ name: 'course_modul_id' })
    @Field(type => CourseModul, { nullable: true })
    courseModul: CourseModul;

    @Column('int', { name: 'learning_time' })
    @Field()
    learningTime: number;

    @Column('int4', { name: 'material_type_id' })
    @Field()
    materialTypeId: number;

    @Field({ nullable: true })
    materialType: string;

    @Column('text', { name: 'information', nullable: true })
    @Field({ nullable: true })
    information: string;

    @OneToMany(() => CourseResource, courseResource => courseResource.courseMaterial)
    @Field(type => [CourseResource], { nullable: true })
    courseResource: CourseResource[];

    @OneToMany(() => UsersCourseMaterial, usersCourseMaterial => usersCourseMaterial.courseMaterial)
    @Field(type => [UsersCourseMaterial], { nullable: true })
    usersCourseMaterial: UsersCourseMaterial[];

    @Field({ nullable: true })
    viewed: boolean;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
