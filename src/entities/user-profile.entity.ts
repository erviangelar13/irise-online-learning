import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Users } from "./users.entity";
// import { ObjectType, Field, registerEnumType } from "@nestjs/graphql";
import { ObjectType, Field } from "@nestjs/graphql";

// export enum Gender {
//     MALE = "male",
//     FEMALE = "female",
// }
//
// registerEnumType(Gender, {
//     name: 'Gender',
// });

@Entity('user_profile')
@ObjectType()
export class UserProfile {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    // @Column('enum', { name: 'gender', enum: Gender, default: Gender.MALE })
    // @Field(type => Gender)
    // gender: Gender;

    @Column('int4', { name: 'gender_id', nullable: true })
    @Field()
    genderId: number;

    @Field({ nullable: true })
    gender: string;

    @Column('date', { name: 'birth_date', nullable: true })
    @Field({ nullable: true })
    birthDate: string;

    @Column('varchar', { name: 'birth_place', nullable: true, length: 50 })
    @Field({ nullable: true })
    birthPlace: string;

    @OneToOne(() => Users, users => users.profile)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
