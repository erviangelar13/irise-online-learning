import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { Users } from "./users.entity";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('user_fcm')
@ObjectType()
export class UserFcm {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'fcm', length: 256 })
    @Field()
    fcm: string;

    @ManyToOne(() => Users, users => users.userForgotPassword)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;
}
