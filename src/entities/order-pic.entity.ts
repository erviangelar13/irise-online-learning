import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, JoinColumn, OneToOne } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";
import { Orders } from "./orders.entity";

@Entity('order_pic')
@ObjectType()
export class OrderPIC {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @OneToOne(() => Orders, orders => orders.orderPIC)
    @JoinColumn({ name: 'order_id' })
    @Field(type => Orders)
    orders: Orders;

    @Column('varchar', { name: 'name', length: 100, default: '' })
    @Field()
    name: string;

    @Column('varchar', { name: 'email', length: 120, default: '' })
    @Field()
    email?: string;

    @Column('varchar', { name: 'phone', nullable: true, length: 15 })
    @Field({ nullable: true })
    phone?: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;

}
