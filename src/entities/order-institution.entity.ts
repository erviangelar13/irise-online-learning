import { Entity, Column, CreateDateColumn, UpdateDateColumn, JoinColumn, ManyToOne, PrimaryGeneratedColumn, OneToOne } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";
import { Orders } from "./orders.entity";
import { Institution } from "./institution.entity";

@Entity('order_institution')
@ObjectType()
export class OrderInstitution {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @OneToOne(() => Orders, orders => orders.orderInstitution)
    @JoinColumn({ name: 'order_id' })
    @Field(type => Orders, { nullable: true })
    orders: Orders;

    @ManyToOne(() => Institution, institution => institution.orderInstitution)
    @JoinColumn({ name: 'institution_id' })
    @Field(type => Institution, { nullable: true })
    institution: Institution;

    @Column('varchar', { name: 'name', length: 100, default: '' })
    @Field()
    name: string;

    @Column('varchar', { name: 'email', length: 120, default: '' })
    @Field()
    email?: string;

    @Column('varchar', { name: 'phone', nullable: true, length: 15 })
    @Field({ nullable: true })
    phone?: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;

}
