import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany, OneToOne } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";
import { Users } from "./users.entity";
import { CourseSchedule } from "./course-schedule.entity";
import { UsersCourseMaterial } from "./users-course-material.entity";
import { UsersCertificate } from "./users-certificate.entity";
import { Institution } from "./institution.entity";

@Entity('users_institution')
@ObjectType()
export class UsersInstitution {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Users, users => users.usersCourse)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;

    @ManyToOne(() => Institution, course => course.userInstitution)
    @JoinColumn({ name: 'institution_id' })
    @Field(type => Institution, { nullable: true })
    institution: Institution;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
