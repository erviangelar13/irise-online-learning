import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { CourseEvaluation } from "./course-evaluation.entity";
import { QuestionOptions } from "./question-options.entity";
import { UserAnswer } from "./user-answer.entity";

@Entity('question')
@ObjectType()
export class Question {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => CourseEvaluation, courseEvaluation => courseEvaluation.question)
    @JoinColumn({ name: 'evaluation_id' })
    @Field(type => CourseEvaluation, { nullable: true })
    courseEvaluation: CourseEvaluation;

    @Column('varchar', { name: 'question', length: 512 })
    @Field()
    question: string;

    @Column('int4', { name: 'question_type_id' })
    @Field()
    questionTypeId: number;

    @Field({ nullable: true })
    questionType: string;

    @OneToMany(() => QuestionOptions, questionOptions => questionOptions.question)
    @Field(type => [QuestionOptions], { nullable: true })
    questionOptions: QuestionOptions[];

    @OneToMany(() => UserAnswer, userAnswer => userAnswer.question)
    @Field(type => [UserAnswer], { nullable: true })
    userAnswer: UserAnswer[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
