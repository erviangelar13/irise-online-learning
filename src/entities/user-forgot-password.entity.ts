import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { EncryptionTransformer } from "typeorm-encrypted";
import { Users } from "./users.entity";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('user_forgot_password')
@ObjectType()
export class UserForgotPassword {

    @PrimaryGeneratedColumn('uuid')
    @Field()
    id: string;

    @Column({
        name: 'password',
        type: 'varchar',
        length: 128,
        nullable: true,
        transformer: new EncryptionTransformer({
            key: 'e41c966f21f9e1577802463f8924e6a3fe3e9751f201304213b2f845d8841d61',
            algorithm: 'aes-256-cbc',
            ivLength: 16,
            iv: 'ff5ac19190424b1d88f9419ef949ae56'
        })
    })
    @Field()
    password: string;

    @Column('timestamp without time zone', { name: 'validation_time' })
    @Field()
    validationTime: Date;

    @ManyToOne(() => Users, users => users.userForgotPassword)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;
}
