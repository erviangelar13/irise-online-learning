import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('course_tags')
@ObjectType()
export class CourseTags {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('varchar', { name: 'description', length: 255, nullable: true, default: '' })
    @Field({ nullable: true })
    description: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
