import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";
import { Users } from "./users.entity";
import { UserAnswer } from "./user-answer.entity";

@Entity('course_evaluation_result')
@ObjectType()
export class CourseEvaluationResult {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Course, course => course.courseEvaluationResult)
    @JoinColumn({ name: 'course_id' })
    @Field(type => Course, { nullable: true })
    course: Course;

    @ManyToOne(() => Users, users => users.courseEvaluationResult)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    users: Users;

    @Column('int', { name: 'score' })
    @Field()
    score: number;

    @Column('date', { name: 'date' })
    @Field()
    date: string;

    @OneToMany(() => UserAnswer, userAnswer => userAnswer.courseEvaluationResult)
    @Field(type => [UserAnswer], { nullable: true })
    userAnswer: UserAnswer[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
