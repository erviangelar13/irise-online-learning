import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Users } from "./users.entity";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('user_billing_information')
@ObjectType()
export class UserBillingInformation {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100, default: '' })
    @Field()
    name: string;

    @Column('varchar', { name: 'email', length: 120, default: '' })
    @Field()
    email: string;

    @Column('varchar', { name: 'phone', nullable: true, length: 15 })
    @Field({ nullable: true })
    phone: string;

    @Column('varchar', { name: 'nationality', nullable: true, length: 100 })
    @Field({ nullable: true })
    nationality: string;

    @Column('text', { name: 'address', nullable: true })
    @Field({ nullable: true })
    address: string;

    @Column('text', { name: 'address2', nullable: true })
    @Field({ nullable: true })
    address2: string;

    @Column('varchar', { name: 'city', nullable: true, length: 100 })
    @Field({ nullable: true })
    city: string;

    @Column('varchar', { name: 'province', nullable: true, length: 100 })
    @Field({ nullable: true })
    province: string;

    @Column('varchar', { name: 'postal_code', nullable: true, length: 15 })
    @Field({ nullable: true })
    postalCode: string;

    @OneToOne(() => Users, users => users.billing)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
