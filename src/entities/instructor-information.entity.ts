import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
// import { Activities } from "./activities.entity";
import { Course } from "./course.entity";

@Entity('instructor_information')
@ObjectType()
export class InstructorInformation {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100, default: '' })
    @Field()
    name: string;

    @Column('varchar', { name: 'title', length: 100, default: '' })
    @Field()
    title: string;

    @Column('text', { name: 'personal_info', nullable: true })
    @Field({ nullable: true })
    personalInfo: string;

    @Column('text', { name: 'profesional_experience', nullable: true })
    @Field({ nullable: true })
    profesionalExperience: string;

    // @OneToMany(() => Activities, activities => activities.activityType)
    // @Field(type => [Activities], { nullable: true })
    // activities: Activities[];

    @OneToMany(() => Course, course => course.instructorInformation)
    @Field(type => [Course], { nullable: true })
    course: Course[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
