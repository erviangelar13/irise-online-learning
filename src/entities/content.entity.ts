import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { ContentMedia } from "./content-media.entity";

@Entity('content')
@ObjectType()
export class Content {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('int4', { name: 'content_type_id' })
    @Field()
    contentTypeId: number;

    @Field({ nullable: true })
    contentType: string;

    @Column('varchar', { name: 'title', length: 100 })
    @Field()
    title: string;

    @Column('text', { name: 'description' })
    @Field()
    description: string;

    @Column('varchar', { name: 'slug', length: 125 })
    @Field()
    slug: string;

    @Column('varchar', { name: 'banner_name', length: 100, nullable: true })
    @Field({ nullable: true })
    bannerName: string;

    @Column('varchar', { name: 'banner_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    bannerOriginalName: string;

    @Column('varchar', { name: 'banner_path', length: 100, nullable: true })
    @Field({ nullable: true })
    bannerPath: string;

    @Column('varchar', { name: 'banner_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    bannerExtension: string;

    @Field({ nullable: true })
    bannerURL: string;

    @Column('boolean', { name: 'is_publish', default: false })
    @Field()
    isPublish: boolean;

    @Column('timestamp', { name: 'publish_date', nullable: true, default: null })
    @Field({ nullable: true })
    publishDate: Date;

    @OneToMany(() => ContentMedia, contentMedia => contentMedia.content)
    @Field(type => [ContentMedia], { nullable: true })
    media: ContentMedia[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;

}
