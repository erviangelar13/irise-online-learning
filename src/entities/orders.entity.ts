import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";
import { Users } from "./users.entity";
import { Course } from "./course.entity";
import { OrderDetail } from "./order-detail.entity";
import { OrderInstitution } from "./order-institution.entity";
import { OrderPIC } from "./order-pic.entity";
import { Transactions } from "./transaction.entity";
import { TransactionHistory } from "./transaction-history.entity";
import { Voucher } from "./voucher.entity";

@Entity('orders')
@ObjectType()
export class Orders {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Users, users => users.orders)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;

    @ManyToOne(() => Course, course => course.orders)
    @JoinColumn({ name: 'course_id' })
    @Field(type => Course, { nullable: true })
    course: Course;

    @Column('timestamp', { name: 'order_date', nullable: true })
    @Field({ nullable: true })
    orderDate: Date;

    @Column('char', { name: 'order_no', length: 16 })
    @Field()
    orderNo: string;

    @Column('int4', { name: 'status', nullable: true })
    @Field({ nullable: true })
    statusId: number;

    @Field({ nullable: true })
    status: string;

    @Column('int4', { name: 'order_type', nullable: true })
    @Field({ nullable: true })
    orderTypeId: number;

    @Field({ nullable: true })
    orderType: string;

    @Column('decimal', { name: 'price_default', precision: 22, scale: 2, default: 0.00 })
    @Field()
    priceDefault: number;

    @Column('decimal', { name: 'discount', precision: 22, scale: 2, default: 0.00 })
    @Field()
    discount: number;

    @Column('decimal', { name: 'price', precision: 22, scale: 2, default: 0.00 })
    @Field()
    price: number;

    @Column('int', { name: 'qty' })
    @Field()
    qty: number;

    @Column('varchar', { name: 'promo_code', length: 15, nullable: true })
    @Field()
    promoCode: string;

    @OneToOne(() => OrderPIC, orderPIC => orderPIC.orders)
    @Field(type => OrderPIC, { nullable: true })
    orderPIC: OrderPIC;

    @OneToMany(() => OrderDetail, orderDetail => orderDetail.orders)
    @Field(type => [OrderDetail], { nullable: true })
    orderDetail: OrderDetail[];

    // @OneToMany(() => OrderInstitution, orderInstitution => orderInstitution.orders)
    // @Field(type => [OrderInstitution], { nullable: true })
    // orderInstitution: OrderInstitution[];

    @OneToOne(() => OrderInstitution, orderInstitution => orderInstitution.orders)
    @Field(type => OrderInstitution, { nullable: true })
    orderInstitution: OrderInstitution;

    @OneToMany(() => Transactions, transaction => transaction.orders)
    @Field(type => [Transactions], { nullable: true })
    transactions: Transactions[];

    @OneToMany(() => TransactionHistory, transactionHistory => transactionHistory.order)
    @Field(type => [TransactionHistory], { nullable: true })
    transactionsHistory: TransactionHistory[];

    @OneToMany(() => Voucher, voucher => voucher.orders)
    @Field(type => [Voucher], { nullable: true })
    vouchers: Voucher[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;

}
