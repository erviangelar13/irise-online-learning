import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('promo_code')
@ObjectType()
export class PromoCode {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'code', length: 15 })
    @Field()
    code: string;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('timestamp', { name: 'date_start', nullable: true })
    @Field({ nullable: true })
    dateStart: Date;

    @Column('timestamp', { name: 'date_end', nullable: true })
    @Field({ nullable: true })
    dateEnd: Date;

    @Column('int4', { name: 'promo_type', nullable: true })
    @Field({ nullable: true })
    promoTypeId: number;

    @Field({ nullable: true })
    promoType: string;

    @Column('int4', { name: 'promo_category', nullable: true })
    @Field({ nullable: true })
    promoCategoryId: number;

    @Field({ nullable: true })
    promoCategory: string;

    @Column('int4', { name: 'value_type', nullable: true })
    @Field({ nullable: true })
    valueTypeId: number;

    @Field({ nullable: true })
    valueType: string;

    @Column('int4', { name: 'value', nullable: true })
    @Field({ nullable: true })
    value: number;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
