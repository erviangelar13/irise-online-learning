import { Column, Entity, BaseEntity, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { EncryptionTransformer } from "typeorm-encrypted";
import { UserHasRoles } from "./user-has-roles.entity";
import { UserProfile } from "./user-profile.entity";
import { UserBillingInformation } from "./user-billing-information.entity";
import { InstructorInformation } from "./instructor-information.entity";
import { UserForgotPassword } from "./user-forgot-password.entity";
import { UserFcm } from "./user-fcm.entity";
import { CourseEvaluationResult } from "./course-evaluation-result.entity";
import { UsersCourse } from "./users-course.entity";
import { Orders } from "./orders.entity";
import { UsersInstitution } from "./users-institution.entity";


@Entity('users')
@ObjectType()
export class Users extends BaseEntity {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'code', length: 10 })
    @Field()
    code: string;

    @Column('varchar', { name: 'email', length: 255, nullable: true, default: '' })
    @Field({ nullable: true })
    email: string;

    @Column('boolean', { name: 'is_active', nullable: true, default: false })
    @Field({ nullable: true })
    isActive: boolean;

    @Column('boolean', { name: 'is_email_verified', nullable: true, default: false })
    @Field({ nullable: true })
    isEmailVerified: boolean;

    @Column('boolean', { name: 'is_phone_verified', nullable: true, default: false })
    @Field({ nullable: true })
    isPhoneVerified: boolean;

    @Column('varchar', { name: 'name', length: 255 })
    @Field()
    name: string;

    @Column('varchar', { name: 'phone', nullable: true, length: 15 })
    @Field({ nullable: true })
    phone: string;

    @Column('varchar', { name: 'username', nullable: true, length: 50 })
    @Field({ nullable: true })
    username: string;

    @Column({
        name: 'password',
        type: 'varchar',
        length: 128,
        nullable: true,
        default: '',
        transformer: new EncryptionTransformer({
            key: 'e41c966f21f9e1577802463f8924e6a3fe3e9751f201304213b2f845d8841d61',
            algorithm: 'aes-256-cbc',
            ivLength: 16,
            iv: 'ff5ac19190424b1d88f9419ef949ae56'
        })
    })
    @Field({ nullable: true })
    password: string;

    @OneToOne(() => InstructorInformation)
    @JoinColumn({ name: 'instructor_id' })
    @Field(type => InstructorInformation, { nullable: true })
    instructor?: InstructorInformation;

    @OneToMany(() => UserHasRoles, userHasRoles => userHasRoles.user)
    @Field(type => [UserHasRoles], { nullable: true })
    userHasRoles?: UserHasRoles[];

    @OneToOne(() => UserProfile, userProfile => userProfile.user)
    @Field(type => UserProfile, { nullable: true })
    profile?: UserProfile;

    @OneToOne(() => UserBillingInformation, userBillingInformation => userBillingInformation.user)
    @Field(type => UserBillingInformation, { nullable: true })
    billing?: UserBillingInformation;

    @OneToMany(() => UserForgotPassword, userForgotPassword => userForgotPassword.user)
    userForgotPassword?: UserForgotPassword[];

    @OneToMany(() => UserFcm, userFcm => userFcm.user)
    @Field(type => [UserFcm], { nullable: true })
    userFcm?: UserFcm[];

    @OneToMany(() => CourseEvaluationResult, courseEvaluationResult => courseEvaluationResult.users)
    @Field(type => [CourseEvaluationResult], { nullable: true })
    courseEvaluationResult: CourseEvaluationResult[];

    @OneToMany(() => UsersCourse, usersCourse => usersCourse.user)
    @Field(type => [UsersCourse], { nullable: true })
    usersCourse?: UsersCourse[];

    @OneToMany(() => Orders, orders => orders.user)
    @Field(type => [Orders], { nullable: true })
    orders?: Orders[];

    @OneToMany(() => UsersInstitution, usersInstitution => usersInstitution.user)
    @Field(type => [UsersInstitution], { nullable: true })
    usersInstitution?: UsersInstitution[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
