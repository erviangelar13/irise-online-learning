import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { CourseCategory } from "./course-category.entity";

@Entity('master_category')
@ObjectType()
export class MasterCategory {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('varchar', { name: 'description', length: 255, nullable: true })
    @Field({ nullable: true })
    description: string;

    @OneToMany(() => CourseCategory, courseCategory => courseCategory.category)
    @Field(type => [CourseCategory], { nullable: true })
    courseCategory?: CourseCategory[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
