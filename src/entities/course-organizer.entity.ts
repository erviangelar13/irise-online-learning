import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";

@Entity('course_organizer')
@ObjectType()
export class CourseOrganizer {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('varchar', { name: 'photo_name', length: 100, nullable: true })
    @Field({ nullable: true })
    photoName?: string;

    @Column('varchar', { name: 'photo_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    photoOriginalName?: string;

    @Column('varchar', { name: 'photo_path', length: 100, nullable: true })
    @Field({ nullable: true })
    photoPath?: string;

    @Column('varchar', { name: 'photo_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    photoExtension?: string;

    @Field({ nullable: true })
    photoURL?: string;

    @Column('int4', { name: 'organization_type_id' })
    @Field()
    organizationTypeId: number;

    @Field({ nullable: true })
    organizationType?: string;

    @Column('varchar', { name: 'description', length: 255, nullable: true })
    @Field({ nullable: true })
    description: string;

    @OneToMany(() => Course, course => course.courseOrganizer)
    @Field(type => [Course], { nullable: true })
    course?: Course[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
