import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { CourseMaterial } from "./course-material.entity";
import { UsersCourse } from "./users-course.entity";

@Entity('users_course_material')
@ObjectType()
export class UsersCourseMaterial {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => UsersCourse, usersCourse => usersCourse.usersCourseMaterial)
    @JoinColumn({ name: 'user_course_id' })
    @Field(type => UsersCourse, { nullable: true })
    usersCourse: UsersCourse;

    @ManyToOne(() => CourseMaterial, courseMaterial => courseMaterial.usersCourseMaterial)
    @JoinColumn({ name: 'course_material_id' })
    @Field(type => CourseMaterial, { nullable: true })
    courseMaterial: CourseMaterial;

    @Column('int4', { name: 'status_id' })
    @Field()
    statusId: number;

    @Field({ nullable: true })
    status: string;

    @Column('timestamp', { name: 'date' })
    @Field()
    date: Date;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
