import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { MasterCategory } from "./master-category.entity";
import { Course } from "./course.entity";

@Entity('course_category')
@ObjectType()
export class CourseCategory {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Course, course => course.courseCategory)
    @JoinColumn({ name: 'course_id' })
    @Field(type => Course, { nullable: true })
    course: Course;

    @ManyToOne(() => MasterCategory, masterCategory => masterCategory.courseCategory)
    @JoinColumn({ name: 'category_id' })
    @Field(type => MasterCategory, { nullable: true })
    category: MasterCategory;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
