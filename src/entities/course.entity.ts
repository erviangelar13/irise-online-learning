import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { MasterType } from "./master-type.entity";
import { InstructorInformation } from "./instructor-information.entity";
import { CourseOrganizer } from "./course-organizer.entity";
import { CourseCategory } from "./course-category.entity";
import { CourseSchedule } from "./course-schedule.entity";
import { CourseModul } from "./course-modul.entity";
import { CourseMaterial } from "./course-material.entity";
import { CourseEvaluation } from "./course-evaluation.entity";
import { CourseEvaluationResult } from "./course-evaluation-result.entity";
import { UsersCourse } from "./users-course.entity";
import { Orders } from "./orders.entity";

@Entity('course')
@ObjectType()
export class Course {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'code', length: 25, nullable: true })
    @Field({ nullable: true })
    code: string;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('text', { name: 'tags' })
    @Field()
    tags: string;

    @Column('int', { name: 'periode', nullable: true })
    @Field({ nullable: true })
    periode: number;

    @Column('int4', { name: 'language_id' })
    @Field()
    languageId: number;

    @Field({ nullable: true })
    language: string;

    @Column('text', { name: 'information' })
    @Field()
    information: string;

    @Column('int', { name: 'duration' })
    @Field()
    duration: number;

    @Column('int4', { name: 'access_type_id' })
    @Field()
    accessTypeId: number;

    @Field({ nullable: true })
    accessType: string;

    @Column('int', { name: 'quota' })
    @Field()
    quota: number;

    @Column('int', { name: 'current_quota', default: 0 })
    @Field()
    currentQuota: number;

    @Column('decimal', { name: 'price', precision: 22, scale: 2, default: 0.00 })
    @Field()
    price: number;

    @Column('boolean', { name: 'evaluation', nullable: true, default: false })
    @Field({ nullable: true })
    evaluation: boolean;

    @Column('boolean', { name: 'certificate', nullable: true, default: false })
    @Field({ nullable: true })
    certificate: boolean;

    @Column('int4', { name: 'course_status_id', default: 0 })
    @Field()
    courseStatusId: number;

    @Field({ nullable: true })
    courseStatus: string;

    @Column('varchar', { name: 'cover_name', length: 100, nullable: true })
    @Field({ nullable: true })
    coverName: string;

    @Column('varchar', { name: 'cover_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    coverOriginalName: string;

    @Column('varchar', { name: 'cover_path', length: 100, nullable: true })
    @Field({ nullable: true })
    coverPath: string;

    @Column('varchar', { name: 'cover_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    coverExtension: string;

    @Field({ nullable: true })
    coverURL: string;

    @ManyToOne(() => MasterType, masterType => masterType.course)
    @JoinColumn({ name: 'course_type' })
    @Field(type => MasterType, { nullable: true })
    courseType: MasterType;

    @ManyToOne(() => CourseOrganizer, courseOrganizer => courseOrganizer.course)
    @JoinColumn({ name: 'organizer_id' })
    @Field(type => CourseOrganizer, { nullable: true })
    courseOrganizer: CourseOrganizer;

    @ManyToOne(() => InstructorInformation, instructorInformation => instructorInformation.course)
    @JoinColumn({ name: 'instructor_id' })
    @Field(type => InstructorInformation, { nullable: true })
    instructorInformation: InstructorInformation;

    @OneToMany(() => CourseCategory, courseCategory => courseCategory.course)
    @Field(type => [CourseCategory], { nullable: true })
    courseCategory: CourseCategory[];

    @Field()
    courseCategoryGrid?: string;

    @OneToMany(() => CourseSchedule, courseSchedule => courseSchedule.course)
    @Field(type => [CourseSchedule], { nullable: true })
    courseSchedule: CourseSchedule[];

    @OneToMany(() => CourseModul, courseModul => courseModul.course)
    @Field(type => [CourseModul], { nullable: true })
    courseModul: CourseModul[];

    @OneToMany(() => CourseEvaluation, courseEvaluation => courseEvaluation.course)
    @Field(type => [CourseEvaluation], { nullable: true })
    courseEvaluation: CourseEvaluation[];

    @OneToMany(() => CourseEvaluationResult, courseEvaluationResult => courseEvaluationResult.course)
    @Field(type => [CourseEvaluationResult], { nullable: true })
    courseEvaluationResult: CourseEvaluationResult[];

    @OneToMany(() => UsersCourse, usersCourse => usersCourse.course)
    @Field(type => [UsersCourse], { nullable: true })
    usersCourse: UsersCourse[];

    @OneToMany(() => Orders, orders => orders.course)
    @Field(type => [Orders], { nullable: true })
    orders: Orders[];

    @Field({ nullable: true })
    activeLearn?: CourseMaterial;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
