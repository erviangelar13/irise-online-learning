import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";
import { UsersCourse } from "./users-course.entity";

@Entity('course_schedule')
@ObjectType()
export class CourseSchedule {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('date', { name: 'date' })
    @Field()
    date: string;

    @Column('char', { name: 'time_start', length: 5 })
    @Field()
    timeStart: string;

    @Column('char', { name: 'time_end', length: 5 })
    @Field()
    timeEnd: string;

    @ManyToOne(() => Course, course => course.courseSchedule)
    @JoinColumn({ name: 'course_id' })
    @Field(type => Course, { nullable: true })
    course: Course;

    @OneToMany(() => UsersCourse, usersCourse => usersCourse.courseSchedule)
    @Field(type => [UsersCourse], { nullable: true })
    usersCourse: UsersCourse[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
