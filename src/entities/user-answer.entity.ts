import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Question } from "./question.entity";
import { CourseEvaluationResult } from "./course-evaluation-result.entity";
import { QuestionOptions } from "./question-options.entity";

@Entity('user_answer')
@ObjectType()
export class UserAnswer {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => CourseEvaluationResult, courseEvaluationResult => courseEvaluationResult.userAnswer)
    @JoinColumn({ name: 'evaluation_result_id' })
    @Field(type => CourseEvaluationResult, { nullable: true })
    courseEvaluationResult: CourseEvaluationResult;

    @ManyToOne(() => Question, question => question.questionOptions)
    @JoinColumn({ name: 'question_id' })
    @Field(type => Question, { nullable: true })
    question: Question;

    @ManyToOne(() => QuestionOptions, questionOptions => questionOptions.userAnswer)
    @JoinColumn({ name: 'answer_id' })
    @Field(type => QuestionOptions, { nullable: true })
    answer: QuestionOptions;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
