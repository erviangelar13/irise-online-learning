import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('master_reference')
@ObjectType()
export class MasterReference {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id?: number;

    @Column('varchar', { name: 'group', length: 20 })
    @Field()
    group: string;

    @Column('int4', { name: 'idx' })
    @Field()
    idx: number;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('varchar', { name: 'description', length: 255, nullable: true })
    @Field()
    description: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
