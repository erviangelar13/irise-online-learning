import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";
import { CourseMaterial } from "./course-material.entity";

@Entity('course_modul')
@ObjectType()
export class CourseModul {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Course, course => course.courseModul)
    @JoinColumn({ name: 'course_id' })
    @Field(type => Course, { nullable: true })
    course: Course;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('int', { name: 'learning_time' })
    @Field()
    learningTime: number;

    @Column('int', { name: 'seq_no', default: 0 })
    @Field()
    seqNo: number;

    @OneToMany(() => CourseMaterial, courseMaterial => courseMaterial.courseModul)
    @Field(type => [CourseMaterial], { nullable: true })
    courseMaterial: CourseMaterial[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
