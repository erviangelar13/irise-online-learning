import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToOne } from "typeorm";
import { UserHasRoles } from "./user-has-roles.entity";
import { Orders } from "./orders.entity";
import { ObjectType, Field } from "@nestjs/graphql";
import { Users } from "./users.entity";
import { UsersCourse } from "./users-course.entity";

@Entity('voucher')
@ObjectType()
export class Voucher {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Orders, orders => orders.vouchers)
    @JoinColumn({ name: 'order_id' })
    @Field(type => Orders, { nullable: true })
    orders: Orders;

    @OneToOne(() => UsersCourse, userCourse => userCourse.voucher)
    @Field(type => UsersCourse, { nullable: true })
    userCourse: UsersCourse;

    @Column('varchar', { name: 'code', length: 25 })
    @Field()
    code: string;

    @Column('timestamp', { name: 'active_date', nullable: true })
    @Field()
    activeDate: Date;

    @Column('timestamp', { name: 'reedem_date', nullable: true })
    @Field({ nullable: true })
    redeemDate: Date;

    @Column('int4', { name: 'user_reedem', nullable: true })
    @Field({ nullable: true })
    userReedemId: number;

    @Field(type => Users, { nullable: true })
    user: Users;

    @Column('varchar', { name: 'invitation_code', length: 30 })
    @Field()
    invitationCode: string;

    @Field({ nullable: true })
    invoiceNo: string;

    @Field({ nullable: true })
    orderNo: string;

    @Field({ defaultValue: false })
    used: boolean;

    @Column('boolean', { name: 'active', default: true })
    @Field()
    active: boolean;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
