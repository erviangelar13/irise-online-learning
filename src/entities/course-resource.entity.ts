import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { CourseMaterial } from "./course-material.entity";

@Entity('course_resource')
@ObjectType()
export class CourseResource {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => CourseMaterial, courseMaterial => courseMaterial.courseResource)
    @JoinColumn({ name: 'course_material_id' })
    @Field(type => CourseMaterial, { nullable: true })
    courseMaterial: CourseMaterial;

    @Column('varchar', { name: 'file_name', length: 100, nullable: true })
    @Field({ nullable: true })
    fileName: string;

    @Column('varchar', { name: 'file_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    fileOriginalName: string;

    @Column('int4', { name: 'file_type_id', nullable: true })
    @Field({ nullable: true })
    fileTypeId: number;

    @Field({ nullable: true })
    fileType: string;

    @Column('varchar', { name: 'file_path', length: 100, nullable: true })
    @Field({ nullable: true })
    filePath: string;

    @Column('varchar', { name: 'file_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    fileExtension: string;

    @Field({ nullable: true })
    fileURL: string;

    @Column('text', { name: 'url_attachment', nullable: true })
    @Field({ nullable: true })
    urlAttachment: string;

    @Column('int4', { name: 'resource_type_id', nullable: true })
    @Field({ nullable: true })
    resourceTypeId: number;

    @Field({ nullable: true })
    resourceType: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
