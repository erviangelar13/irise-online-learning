import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { PaymentMethod } from "./payment-method.entity";

@Entity('payment_provider')
@ObjectType()
export class PaymentProvider {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('varchar', { name: 'logo_name', length: 100, nullable: true })
    @Field({ nullable: true })
    logoName: string;

    @Column('varchar', { name: 'logo_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    logoOriginalName: string;

    @Column('varchar', { name: 'logo_path', length: 100, nullable: true })
    @Field({ nullable: true })
    logoPath: string;

    @Column('varchar', { name: 'logo_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    logoExtension: string;

    @Field({ nullable: true })
    logoURL: string;

    @Column('text', { name: 'description', nullable: true, default: '' })
    @Field({ nullable: true })
    description: string;

    @Column('varchar', { name: 'url_payment', nullable: true, default: '', length: 255 })
    @Field({ nullable: true })
    urlPayment: string;

    @ManyToOne(() => PaymentMethod, paymentMethod => paymentMethod.paymentProvider)
    @JoinColumn({ name: 'payment_method_id' })
    @Field(type => PaymentMethod, { nullable: true })
    paymentMethod: PaymentMethod;

    @Column('boolean', { name: 'show_option', default: true })
    @Field()
    shwoOption: boolean;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
