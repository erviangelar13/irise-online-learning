import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { OrderInstitution } from "./order-institution.entity";
import { Voucher } from "./voucher.entity";
import { Course } from "./course.entity";
import { UsersInstitution } from "./users-institution.entity";

@Entity('institution')
@ObjectType()
export class Institution {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100, default: '' })
    @Field()
    name: string;

    @Column('text', { name: 'address', nullable: true })
    @Field({ nullable: true })
    address: string;

    @Column('varchar', { name: 'phone', length: 15, default: '' })
    @Field()
    phone: string;

    @Column('varchar', { name: 'pic', length: 50, default: '' })
    @Field()
    pic: string;

    @Column('varchar', { name: 'pic_phone', length: 15, default: '' })
    @Field()
    picPhone: string;

    @Column('varchar', { name: 'invitation_code', length: 10, default: '' })
    @Field()
    invitationCode: string;

    @OneToMany(() => OrderInstitution, orderInstitution => orderInstitution.institution)
    @Field(type => [OrderInstitution], { nullable: true })
    orderInstitution: OrderInstitution[];

    @Field(type => [Voucher], { nullable: true })
    vouchers: Voucher[];
    
    @OneToMany(() => UsersInstitution, userInstituion => userInstituion.institution)
    @Field(type => [UsersInstitution], { nullable: true })
    userInstitution: UsersInstitution[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;

}
