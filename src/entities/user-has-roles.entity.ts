import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, BaseEntity } from "typeorm";
import { Users } from "./users.entity";
import { Roles } from "./roles.entity";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('user_has_roles')
@ObjectType()
export class UserHasRoles extends BaseEntity {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Users, users => users.userHasRoles)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;

    @ManyToOne(() => Roles, roles => roles.userHasRoles)
    @JoinColumn({ name: 'role_id' })
    @Field(type => Roles, { nullable: true })
    role: Roles;
}
