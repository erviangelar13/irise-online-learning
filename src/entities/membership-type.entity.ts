import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field, registerEnumType } from "@nestjs/graphql";

export enum PeriodeUnit {
    DAYS = "days",
    MONTHS = "months",
    YEARS = "years",
}

registerEnumType(PeriodeUnit, {
    name: 'PeriodeUnit',
});

@Entity('membership_type')
@ObjectType()
export class MembershipType {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'code', length: 10 })
    @Field()
    code: string;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('int', { name: 'periode' })
    @Field()
    periode: number;

    @Column('enum', { name: 'periode_unit', enum: PeriodeUnit, default: PeriodeUnit.DAYS })
    @Field(type => PeriodeUnit)
    periodeUnit: PeriodeUnit;

    @Column('decimal', { name: 'price', precision: 22, scale: 2, default: 0.00 })
    @Field()
    price: number;

    @Column('text', { name: 'information', nullable: true, default: '' })
    @Field({ nullable: true })
    information: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
