import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";
import { Question } from "./question.entity";

@Entity('course_evaluation')
@ObjectType()
export class CourseEvaluation {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Course, course => course.courseEvaluation)
    @JoinColumn({ name: 'course_id' })
    @Field(type => Course, { nullable: true })
    course: Course;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('int', { name: 'evaluation_time' })
    @Field()
    evaluationTime: number;

    @Column('int', { name: 'passing_grade' })
    @Field()
    passingGrade: number;

    @OneToMany(() => Question, question => question.courseEvaluation)
    @Field(type => [Question], { nullable: true })
    question: Question[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
