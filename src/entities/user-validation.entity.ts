import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { Users } from "./users.entity";

@Entity('user_validation')
export class UserValidation {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('boolean', { name: 'active', default: true })
    active: boolean;

    @Column('boolean', { name: 'is_email_confirmation', default: false })
    isEmailConfirmation: boolean;

    @Column('boolean', { name: 'is_phone_confirmation', default: false })
    isPhoneConfirmation: boolean;

    @Column('char', { name: 'otp', length: 6 })
    otp: string;

    @Column('timestamp without time zone', { name: 'validation_time' })
    validationTime: Date;

    @OneToOne(() => Users)
    @JoinColumn({ name: 'user_id' })
    user: Users;

}
