import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany, OneToOne } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";
import { Users } from "./users.entity";
import { CourseSchedule } from "./course-schedule.entity";
import { UsersCourseMaterial } from "./users-course-material.entity";
import { UsersCertificate } from "./users-certificate.entity";
import { Voucher } from "./voucher.entity";

@Entity('users_course')
@ObjectType()
export class UsersCourse {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Users, users => users.usersCourse)
    @JoinColumn({ name: 'user_id' })
    @Field(type => Users, { nullable: true })
    user: Users;

    @ManyToOne(() => Course, course => course.usersCourse)
    @JoinColumn({ name: 'course_id' })
    @Field(type => Course, { nullable: true })
    course: Course;

    @ManyToOne(() => CourseSchedule, courseSchedule => courseSchedule.usersCourse)
    @JoinColumn({ name: 'course_schedule_id' })
    @Field(type => CourseSchedule, { nullable: true })
    courseSchedule: CourseSchedule;

    @Column('int4', { name: 'status_id' })
    @Field()
    statusId: number;

    @Field({ nullable: true })
    status: string;

    @Field({ nullable: true })
    progress: number;

    @Column('varchar', { name: 'course_code' })
    @Field({ nullable: true })
    courseCode: string;

    @Column('boolean', { name: 'is_visited', default : false })
    @Field()
    isVisited: boolean;

    @OneToMany(() => UsersCourseMaterial, usersCourseMaterial => usersCourseMaterial.usersCourse)
    @Field(type => [UsersCourseMaterial], { nullable: true })
    usersCourseMaterial: UsersCourseMaterial[];

    @OneToOne(() => UsersCertificate, userCertificate => userCertificate.usersCourse)
    @Field(type => UsersCertificate, { nullable: true })
    usersCertificate: UsersCertificate;

    @OneToOne(() => Voucher, voucher => voucher.userCourse)
    @JoinColumn({ name: 'voucher_id' })
    @Field(type => Voucher, { nullable: true })
    voucher: Voucher;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
