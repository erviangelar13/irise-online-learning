import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Question } from "./question.entity";
import { UserAnswer } from "./user-answer.entity";

@Entity('question_options')
@ObjectType()
export class QuestionOptions {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Question, question => question.questionOptions)
    @JoinColumn({ name: 'question_id' })
    @Field(type => Question, { nullable: true })
    question: Question;

    @Column('varchar', { name: 'question', length: 512 })
    @Field()
    answer: string;

    // @Column('int4', { name: 'question_type_id' })
    // @Field()
    // questionTypeId: number;

    // @Field({ nullable: true })
    // questionType: string;

    @Column('boolean', { name: 'correct_answer' })
    @Field()
    correctAnswer: boolean;

    @OneToMany(() => UserAnswer, userAnswer => userAnswer.answer)
    @Field(type => [UserAnswer], { nullable: true })
    userAnswer: UserAnswer[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
