import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Content } from "./content.entity";

@Entity('content_media')
@ObjectType()
export class ContentMedia {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'media_name', length: 100, nullable: true })
    @Field({ nullable: true })
    mediaName: string;

    @Column('varchar', { name: 'media_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    mediaOriginalName: string;

    @Column('varchar', { name: 'media_path', length: 100, nullable: true })
    @Field({ nullable: true })
    mediaPath: string;

    @Column('varchar', { name: 'media_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    mediaExtension: string;

    @Field({ nullable: true })
    mediaURL: string;

    @Column('int4', { name: 'media_type_id', nullable: true })
    @Field({ nullable: true })
    mediaTypeId: number;

    @Field({ nullable: true })
    mediaType: string;

    @ManyToOne(() => Content, content => content.media)
    @JoinColumn({ name: 'content_id' })
    @Field(type => Content, { nullable: true })
    content: Content;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;

}
