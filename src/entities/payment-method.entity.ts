import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { PaymentProvider } from "./payment-provider.entity";

@Entity('payment_method')
@ObjectType()
export class PaymentMethod {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('varchar', { name: 'logo_name', length: 100, nullable: true })
    @Field({ nullable: true })
    logoName: string;

    @Column('varchar', { name: 'logo_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    logoOriginalName: string;

    @Column('varchar', { name: 'logo_path', length: 100, nullable: true })
    @Field({ nullable: true })
    logoPath: string;

    @Column('varchar', { name: 'logo_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    logoExtension: string;

    @Field({ nullable: true })
    logoURL: string;

    @Column('text', { name: 'description', nullable: true, default: '' })
    @Field({ nullable: true })
    description: string;

    @Column('text', { name: 'term_and_condition', nullable: true, default: '' })
    @Field({ nullable: true })
    termAndCondition: string;

    @OneToMany(() => PaymentProvider, paymentProvider => paymentProvider.paymentMethod)
    @Field(type => [PaymentProvider], { nullable: true })
    paymentProvider: PaymentProvider[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
