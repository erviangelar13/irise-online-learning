import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Course } from "./course.entity";

@Entity('master_type')
@ObjectType()
export class MasterType {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 100 })
    @Field()
    name: string;

    @Column('varchar', { name: 'description', length: 255, nullable: true })
    @Field()
    description: string;

    @OneToMany(() => Course, course => course.courseType)
    @Field(type => [Course], { nullable: true })
    course?: Course[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
