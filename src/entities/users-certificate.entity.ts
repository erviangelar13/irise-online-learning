import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, OneToOne } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { UsersCourse } from "./users-course.entity";

@Entity('users_certificate')
@ObjectType()
export class UsersCertificate {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @OneToOne(() => UsersCourse, usersCourse => usersCourse.usersCertificate)
    @JoinColumn({ name: 'user_course_id' })
    @Field(type => UsersCourse, { nullable: true })
    usersCourse: UsersCourse;

    @Column('timestamp', { name: 'date' })
    @Field()
    date: Date;

    @Column('varchar', { name: 'file_name', length: 100, nullable: true })
    @Field({ nullable: true })
    fileName: string;

    @Column('varchar', { name: 'file_original_name', length: 100, nullable: true })
    @Field({ nullable: true })
    fileOriginalName: string;

    @Column('varchar', { name: 'file_path', length: 100, nullable: true })
    @Field({ nullable: true })
    filePath: string;

    @Column('varchar', { name: 'file_extension', length: 10, nullable: true })
    @Field({ nullable: true })
    fileExtension: string;

    @Field({ nullable: true })
    fileURL: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
