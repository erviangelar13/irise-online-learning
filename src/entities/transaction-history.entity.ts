import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Orders } from "./orders.entity";
import { Transactions } from "./transaction.entity";

@Entity('transaction_history')
@ObjectType()
export class TransactionHistory {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Transactions, transactions => transactions.transactionsHistory)
    @JoinColumn({ name: 'transaction_id' })
    @Field(type => Transactions, { nullable: true })
    transaction: Transactions;

    @ManyToOne(() => Orders, orders => orders.transactionsHistory)
    @JoinColumn({ name: 'order_id' })
    @Field(type => Orders, { nullable: true })
    order: Orders;

    @Column('timestamp', { name: 'date', nullable: true })
    @Field({ nullable: true })
    date: Date;

    @Column('int4', { name: 'payment_status_id', nullable: true })
    @Field({ nullable: true })
    paymentStatusId: number;

    @Field({ nullable: true })
    paymentStatus: string;

    @Field({ nullable: true })
    info: string;

    @Field({ nullable: true })
    amount: number;

    @Field({ nullable: true })
    transactionCode: string;

    @Field({ nullable: true })
    paymentProvider: string;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
