import { Entity, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ObjectType, Field } from "@nestjs/graphql";
import { Orders } from "./orders.entity";
import { Voucher } from "./voucher.entity";
import { TransactionHistory } from "./transaction-history.entity";

@Entity('transaction')
@ObjectType()
export class Transactions {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @ManyToOne(() => Orders, orders => orders.transactions)
    @JoinColumn({ name: 'order_id' })
    @Field(type => Orders, { nullable: true })
    orders: Orders;

    @Column('timestamp', { name: 'date', nullable: true })
    @Field({ nullable: true })
    date: Date;

    @Column('decimal', { name: 'amount', precision: 22, scale: 2, default: 0.00 })
    @Field()
    amount: number;

    @Field({ nullable: true })
    notes: string;

    @Column('int4', { name: 'payment_provider_id', nullable: true })
    @Field({ nullable: true })
    paymentProviderId: number;

    @Field({ nullable: true })
    paymentProvider: string;

    @Column('varchar', { name: 'transaction_code', length: 25, nullable: true })
    @Field({ nullable: true })
    transactionCode: string;

    @Column('varchar', { name: 'invoice_no', length: 25, nullable: true })
    @Field({ nullable: true })
    invoiceNo: string;

    @Column('int4', { name: 'payment_type_id', nullable: true })
    @Field({ nullable: true })
    paymentTypeId: number;

    @Field({ nullable: true })
    paymentType: string;

    @Column('int4', { name: 'payment_status_id', nullable: true })
    @Field({ nullable: true })
    paymentStatusId: number;

    @Field({ nullable: true })
    paymentStatus: string;

    @OneToMany(() => TransactionHistory, transactionHistory => transactionHistory.transaction)
    @Field(type => [TransactionHistory], { nullable: true })
    transactionsHistory: TransactionHistory[];
    
    @Field(type => [Voucher], { nullable: true })
    vouchers: Voucher[];

    @Field({ defaultValue: false})
    voucherIssued: boolean;

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
