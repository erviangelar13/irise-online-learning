import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('otp')
export class Otp {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('char', { name: 'otp', length: 6 })
    otp: string;

    @Column('boolean', { name: 'is_active', default: false })
    isActive: boolean;

}
