import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { UserHasRoles } from "./user-has-roles.entity";
import { ObjectType, Field } from "@nestjs/graphql";

@Entity('roles')
@ObjectType()
export class Roles {

    @PrimaryGeneratedColumn('increment')
    @Field()
    id: number;

    @Column('varchar', { name: 'name', length: 25 })
    @Field()
    name: string;

    @Column('varchar', { name: 'description', length: 255, nullable: true })
    @Field()
    description: string;

    @OneToMany(() => UserHasRoles, userHasRoles => userHasRoles.role)
    @Field(type => [UserHasRoles], { nullable: true })
    userHasRoles?: UserHasRoles[];

    @Field()
    @CreateDateColumn({ name: 'created_at' })
    createdAt?: Date;

    @Field()
    @UpdateDateColumn({ name: 'updated_at' })
    updatedAt?: Date;
}
