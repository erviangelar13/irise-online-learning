import { SetMetadata } from '@nestjs/common';

export const RolesGuard = (...rolesGuard: string[]) => SetMetadata('rolesGuard', rolesGuard);
