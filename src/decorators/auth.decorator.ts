import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Auth } from '../helpers/auth.helper';

export const AuthUser = createParamDecorator((data, context: ExecutionContext): Auth => {
    const ctx = GqlExecutionContext.create(context);
    const auth: Auth = ctx.getContext().req.auth;
    return auth;
});
