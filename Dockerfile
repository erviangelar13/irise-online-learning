FROM node:14.18-alpine

WORKDIR /app

COPY package*.json /app/

RUN npm install glob rimraf

RUN npm install

RUN npm audit fix

COPY . /app

RUN npm run build

CMD ["node", "dist/main"]

EXPOSE 3001
